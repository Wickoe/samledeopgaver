package opgaver.Measurable;

/**
 * Models a chilie with a name and a scoville strength.
 *
 * @author Martin
 *
 */
public class Chillie implements Measurable {
	private String navn;
	private double scoville;

	/**
	 * Initialize a chillie with a name and a scoville streangth.
	 *
	 * @Pre: The scoville strength is between 1 and 2.500.000
	 * @param etNavn
	 *            the name of the chillie
	 * @param enScovilleStyrke
	 *            the strength of the chillie in scoville
	 */
	public Chillie(String etNavn, double enScovilleStyrke) {
		navn = etNavn;
		scoville = enScovilleStyrke;
	}

	/**
	 * Returns the name of the chillie.
	 *
	 * @return the name of the chillie
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Returns the measure of strength in scovilles.
	 *
	 * @return the measure of strength in scoville of the chillie
	 */
	@Override
	public double getMeasure() {
		return scoville;
	}

}

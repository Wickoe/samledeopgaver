package opgaver.Measurable;

/**
 * Models data.
 *
 * @author Martin
 *
 */
public class Data {
	/**
	 * Returns the biggest measure.
	 *
	 * @param objects
	 *            a list of measurable objects
	 * @return the largest measure measured from the list of objects
	 */
	public static double max(Measurable[] objects) {
		double biggestMeasure = objects[0].getMeasure();

		for (Measurable m : objects) {
			if (m.getMeasure() > biggestMeasure) {
				biggestMeasure = m.getMeasure();
			}
		}

		return biggestMeasure;
	}

	public static double avg(Measurable[] objects) {
		double sum = 0;
		int size = 0;

		for (Measurable m : objects) {
			sum += m.getMeasure();
			size++;
		}
		return sum / size;
	}

}

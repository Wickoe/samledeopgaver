package opgaver.Measurable;

/**
 * Interface defining measure and how to return it
 *
 * @author Matin
 *
 */
public interface Measurable {
	/**
	 * Returns the measure of the object.
	 *
	 * @return the measure of the object
	 */
	public double getMeasure();
}

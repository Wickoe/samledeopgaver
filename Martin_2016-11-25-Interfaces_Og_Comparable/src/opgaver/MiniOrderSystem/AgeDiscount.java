package opgaver.MiniOrderSystem;

/**
 * Models an age discount that gives an age discount
 *
 * @author Martin
 *
 */
public class AgeDiscount implements Discount {
	private int age;

	/**
	 * Initialize an age discount with a specific age as discount.
	 *
	 * @param anAge
	 *            the specific age discount
	 *
	 */
	public AgeDiscount(int anAge) {
		age = anAge;
	}

	@Override
	public double getDiscountedPrice(double originalPrice) {
		return originalPrice * (1 - (100 - age) / 100);
	}
}

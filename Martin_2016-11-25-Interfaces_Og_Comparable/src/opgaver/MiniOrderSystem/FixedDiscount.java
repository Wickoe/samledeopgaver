package opgaver.MiniOrderSystem;

public class FixedDiscount implements Discount {
	private double fixedDiscount, discountLimit;

	public FixedDiscount(double aDiscountLimit, double aFixedDiscount) {
		fixedDiscount = aFixedDiscount;
		discountLimit = aDiscountLimit;
	}

	@Override
	public double getDiscountedPrice(double originalPrice) {
		double price = originalPrice;

		if (price >= discountLimit) {
			price -= fixedDiscount;
		}

		return price;
	}

}

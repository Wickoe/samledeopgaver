package opgaver.MiniOrderSystem;

/**
 * Models a discount for prices.
 *
 * @author Martin
 *
 */
public interface Discount {
	/**
	 * Calculate the discounted price based on the original price.
	 *
	 * @param originalPrice
	 *            the original price of the order
	 * @return the discounted price of the order
	 */
	public double getDiscountedPrice(double originalPrice);
}

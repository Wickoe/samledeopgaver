package opgaver.Customer;

/**
 * Models a customer with a first- and last name and an age. Customer is
 * comparable to other customers.
 *
 * @author Martin
 *
 */
public class Customer implements Comparable<Customer> {
	private String fornavn, efternavn;
	private int alder;

	/**
	 * Initialize a customer with a first and last name and an age.
	 *
	 * @param etFornavn
	 *            the first name of the customer
	 * @param etEfternavn
	 *            the last name of the customer
	 * @param enAlder
	 *            the age of the customer
	 */
	public Customer(String etFornavn, String etEfternavn, int enAlder) {
		fornavn = etFornavn;
		efternavn = etEfternavn;
		alder = enAlder;
	}

	/**
	 * Returns the forst name of the customer.
	 *
	 * @return the first name of the customer
	 */
	public String getFornavn() {
		return fornavn;
	}

	/**
	 * Returns the last name of the customer.
	 *
	 * @return the last name of the customer
	 */
	public String getEfternavn() {
		return efternavn;
	}

	/**
	 * Returns the age of the customer.
	 *
	 * @return the age of the customer
	 */
	public int getAlder() {
		return alder;
	}

	@Override
	public int compareTo(Customer other) {
		int compareStatus = efternavn.compareToIgnoreCase(other.getEfternavn());

		if (compareStatus == 0) {
			compareStatus = fornavn.compareToIgnoreCase(other.getFornavn());
		}

		if (compareStatus == 0) {
			compareStatus = alder - other.getAlder();
		}
		return compareStatus;
	}

	/**
	 * Compares and returns the last customer from a given list.
	 *
	 * @param customers
	 *            the list of customers to be compared
	 * @return the last customer from the given list of customers
	 */
	public static Customer lastCustomer(Customer[] customers) {
		Customer last = customers[0];

		for (Customer c : customers) {
			if (c.compareTo(last) > 0) {
				last = c;
			}
		}
		return last;
	}

	/**
	 * Returns a list of customers that in a compare comes after the specific
	 * customer.
	 *
	 * @param customers
	 *            the list of customers to sort through
	 * @param customer
	 *            the customer to compare against
	 * @return the list of customers that comes after the given customer
	 */
	public static Customer[] afterCustomer(Customer[] customers, Customer customer) {
		Customer[] afterCustomers = new Customer[customers.length];
		int tempIndex = 0;

		for (Customer c : customers) {
			if (c.compareTo(customer) > 0) {
				afterCustomers[tempIndex] = c;
				tempIndex++;
			}
		}
		return afterCustomers;
	}

}

package opgave1_ImplementereStakPåArray;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayStack implements Stack {
    private Object[] stack;
    private final int INIT_SIZE = 10;
    private int currentSize;
    
    public ArrayStack() {
        this.stack = new Object[INIT_SIZE];
        currentSize = 0;
    }
    
    @Override
    public void push(Object element) {
        growIfNecessary();
        stack[currentSize] = element;
        currentSize++;
    }
    
    @Override
    public Object pop() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        currentSize--;
        Object removed = stack[currentSize];
        //stack[currentSize] = null;
        return removed;
    }
    
    @Override
    public Object peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Object look = stack[currentSize - 1];
        return look;
    }
    
    @Override
    public boolean isEmpty() {
        return currentSize == 0;
    }
    
    @Override
    public int size() {
        return currentSize;
    }
    
    private void growIfNecessary() {
        if (currentSize == stack.length) {
            Object[] newStack = new Object[2 * stack.length];
            for (int i = 0; i < stack.length; i++) {
                newStack[i] = stack[i];
            }
            stack = newStack;
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(stack);
    }
    
}

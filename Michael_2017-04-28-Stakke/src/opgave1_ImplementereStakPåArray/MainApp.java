package opgave1_ImplementereStakPåArray;

public class MainApp {

    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack();

        stack.push(10);
        stack.push(10);
        stack.push(10);
        stack.push(10);
        stack.push(10);
        stack.push(10);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();

        System.out.println(stack.toString());
    }

}

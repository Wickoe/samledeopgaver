package opgave2_CheckParenteses;

public class MainApp {
    
    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack();
        
        String ord = "((([[[]]]))){}{}{}[[]]{[()]}";

        System.out.println(stack.checkParenteses(ord));
        System.out.println(stack.toString());
    }
    
}

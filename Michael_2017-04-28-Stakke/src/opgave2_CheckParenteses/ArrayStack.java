package opgave2_CheckParenteses;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayStack implements Stack {
    private Object[] stack;
    private final int INIT_SIZE = 10;
    private int currentSize;
    
    public ArrayStack() {
        this.stack = new Object[INIT_SIZE];
        currentSize = 0;
    }
    
    @Override
    public void push(Object element) {
        growIfNecessary();
        stack[currentSize] = element;
        currentSize++;
    }
    
    @Override
    public Object pop() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        currentSize--;
        Object removed = stack[currentSize];
        //stack[currentSize] = null;
        return removed;
    }
    
    @Override
    public Object peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Object look = stack[currentSize - 1];
        return look;
    }
    
    @Override
    public boolean isEmpty() {
        return currentSize == 0;
    }
    
    @Override
    public int size() {
        return currentSize;
    }
    
    private void growIfNecessary() {
        if (currentSize == stack.length) {
            Object[] newStack = new Object[2 * stack.length];
            for (int i = 0; i < stack.length; i++) {
                newStack[i] = stack[i];
            }
            stack = newStack;
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(stack);
    }
    
    public boolean checkParenteses(String s) {
        boolean fine = false;
        // første = ()
        int første = 0;
        // anden = {}
        int anden = 0;
        // tredje = []
        int tredje = 0;
        char bogstav = ' ';
        
        for (int i = 0; i < s.length(); i++) {
            bogstav = s.charAt(i);
            int ascii = bogstav;
            if (ascii == 40) {
                første++;
            }
            else if (ascii == 41) {
                første--;
            }
            else if (ascii == 123) {
                anden++;
            }
            else if (ascii == 125) {
                anden--;
            }
            else if (ascii == 91) {
                tredje++;
            }
            else if (ascii == 93) {
                tredje--;
            }
            
            if (første == 0 && anden == 0 && tredje == 0) {
                fine = true;
            }
        }

        return fine;
    }
}

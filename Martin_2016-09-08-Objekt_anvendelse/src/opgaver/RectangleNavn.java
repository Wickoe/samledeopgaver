package opgaver;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class RectangleNavn extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Figurer");
		Pane pane = new Pane();
		initContent(pane);
		
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(Pane pane) {
		Rectangle r = new Rectangle(10, 10, 100, 20);
		r.setStroke(Color.BLACK);
		r.setFill(Color.BLUE);
		pane.getChildren().add(r);
		
		Text navn = new Text(20, 25, "Martin");
		navn.setFill(Color.ORANGE);
		pane.getChildren().add(navn);
	}
}

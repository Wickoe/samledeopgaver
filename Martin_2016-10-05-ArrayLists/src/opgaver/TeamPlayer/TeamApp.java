package opgaver.TeamPlayer;

public class TeamApp {
	public static void main(String[] args) {
		Team team = new Team("Team GG");

		Player p1 = new Player("Martin", 21);
		Player p2 = new Player("Leo", 22);
		Player p3 = new Player("Michael", 23);
		Player p4 = new Player("Nicolai", 24);

		team.addPlayer(p1);
		team.addPlayer(p2);
		team.addPlayer(p3);
		team.addPlayer(p4);

		p1.addScore(5);
		p2.addScore(6);
		p3.addScore(7);
		p4.addScore(8);

		System.out.println(team.bestPlayer());
		System.out.println(team.calcAverageAge());
		System.out.println(team.calcOldPlayerScore(23));
		System.out.println(team.calcTotalScore());
	}
}

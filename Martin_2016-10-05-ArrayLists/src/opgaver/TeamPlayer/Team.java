package opgaver.TeamPlayer;

import java.util.ArrayList;

public class Team {
	private String name;
	private ArrayList<Player> players;

	public Team(String aName) {
		name = aName;
		players = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Player> getPlayers() {
		return new ArrayList<>(players);
	}

	public void addPlayer(Player player) {
		players.add(player);
	}

	public void print() {
		for (Player p : players) {
			System.out.println(p);
		}
	}

	public double calcAverageAge() {
		double ageTotal = 0;

		for (Player p : players) {
			ageTotal += p.getAge();
		}

		return ageTotal / players.size();
	}

	public int calcTotalScore() {
		int score = 0;

		for (Player p : players) {
			score += p.getScore();
		}

		return score;
	}

	public int calcOldPlayerScore(int ageLimit) {
		int score = 0;

		for (Player p : players) {
			if (p.getAge() >= ageLimit) {
				score += p.getScore();
			}
		}

		return score;
	}

	public int maxScore() {
		int maxScore = 0;

		for (Player p : players) {
			int tempScore = p.getScore();

			if (tempScore >= maxScore) {
				maxScore = tempScore;
			}
		}

		return maxScore;
	}

	public String bestPlayer() {
		Player bestPlayer = players.get(0);

		for (int i = 1; i < players.size(); i++) {
			Player tempPlayer = players.get(i);

			if (tempPlayer.getScore() > bestPlayer.getScore()) {
				bestPlayer = tempPlayer;
			}
		}

		return bestPlayer.getName();
	}
}

package opgaver.TeamPlayer;

public class Player {
	private String name;
	private int age;
	private int score;
	private Team team;

	public Player(String aName, int anAge) {
		name = aName;
		age = anAge;
		score = 0;
	}

	public Player(String aName, int anAge, Team ateam) {
		name = aName;
		age = anAge;
		team = ateam;
	}

	public String getName() {
		return name;
	}

	public void setName(String aName) {
		name = aName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getScore() {
		return score;
	}

	public void addScore(int score) {
		this.score += score;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "Name: " + name + ", age: " + age + ", score: " + score;
	}
}

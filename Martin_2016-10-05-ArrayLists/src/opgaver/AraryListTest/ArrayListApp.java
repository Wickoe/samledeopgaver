package opgaver.AraryListTest;

import java.util.ArrayList;

public class ArrayListApp {
	public static void main(String[] args) {
		ArrayList<String> navne = new ArrayList<>();

		navne.add("Hans");
		navne.add("Viggo");
		navne.add("Jens");
		navne.add("Børge");
		navne.add("Bent");

		System.out.println(navne.size());
		System.out.println();

		navne.add(2, "Jane");

		System.out.println(navne.toString());
		System.out.println();

		navne.remove(1);

		navne.add(0, "Pia");
		navne.add("Ib");

		System.out.println(navne.size());
		System.out.println();

		navne.set(2, "Hansi");
		System.out.println(navne.size());
		System.out.println(navne.toString());
		System.out.println();

		for (String navn : navne) {
			System.out.println(navn.length());
		}

		System.out.println();

		for (int i = 0; i < navne.size(); i++) {
			System.out.println(navne.get(i).length());
		}
	}
}

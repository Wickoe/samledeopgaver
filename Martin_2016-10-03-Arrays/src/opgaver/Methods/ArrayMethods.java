package opgaver.Methods;

public class ArrayMethods {
	public int sum(int[] array) {
		int sum = 0;

		for (int i : array) {
			sum += i;
		}
		return sum;
	}

	public int[] createSum(int[] a, int[] b) {
		int[] array = new int[a.length + b.length];

		for (int i = 0; i < array.length; i++) {
			int sum = 0;

			if (i < a.length) {
				sum += a[i];
			}
			if (i < b.length) {
				sum += b[i];
			}

			array[i] = sum;
		}
		return array;
	}

	public boolean hasUneven(int[] t) {
		return t.length % 2 == 1;
	}
}

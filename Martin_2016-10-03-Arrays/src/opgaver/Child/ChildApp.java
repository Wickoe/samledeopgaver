package opgaver.Child;

public class ChildApp {

	public static void main(String[] args) {
		Child child = new Child(2, false);
		System.out.println(String.format("Alder: %s", child.getAge()));
		String inst = child.institution();
		System.out.println(String.format("Institution: %s", inst));

		double[] weight = { 4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3 };

		Child childTwo = new Child(10, true, weight);

		System.out.println(childTwo.getWeight(9));
		System.out.println(childTwo.getBiggestWeightGain());

		Institution i = new Institution("Roenbaek", "Ådalsvej", 10);
		Child c = new Child(4, true);
		i.addCahild(child);
		i.addCahild(childTwo);
		i.addCahild(c);

		System.out.println(i.boysCount());
		System.out.println(i.girlsCount());
	}

}

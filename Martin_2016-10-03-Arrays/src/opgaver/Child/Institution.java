package opgaver.Child;

public class Institution {
	private String navn, adresse;
	private Child[] indskrevendeBørn;
	private int indskrevende;

	public Institution(String etNavn, String enAdresse, int capasitet) {
		navn = etNavn;
		adresse = enAdresse;
		indskrevendeBørn = new Child[capasitet];
		indskrevende = 0;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public void addCahild(Child child) {
		if (indskrevende < indskrevendeBørn.length) {
			indskrevendeBørn[indskrevende] = child;
			indskrevende++;
		}
	}

	public double gennemsnitsalder() {
		double alder = 0;
		for (Child c : indskrevendeBørn) {
			alder += c.getAge();
		}

		return alder / indskrevende;
	}

	public int girlsCount() {
		return indskrevende - boysCount();
	}

	public int boysCount() {
		int boysCount = 0;

		for (int i = 0; i < indskrevende; i++) {
			if (indskrevendeBørn[i].isBoy()) {
				boysCount++;
			}
		}

		return boysCount;
	}
}

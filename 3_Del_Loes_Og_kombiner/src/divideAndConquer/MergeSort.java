package divideAndConquer;

import java.util.ArrayList;
import java.util.List;

public class MergeSort<E extends Comparable<E>> {
	private void mergesort(List<E> list, int l, int h) {
		if (l < h) {
			int middle = (l + h) / 2;
			mergesort(list, l, middle);
			mergesort(list, middle + 1, h);
			merge(list, l, middle, h);
		}
	}

	private void merge(List<E> list, int low, int middle, int high) {
		int tempMiddle = middle + 1;
		int tempLeft = low;

		List<E> tempList = new ArrayList<>();

		while (tempLeft <= middle && tempMiddle <= high) {
			if (list.get(tempLeft).compareTo(list.get(tempMiddle)) < 0) {
				tempList.add(list.get(tempLeft));
				tempLeft++;
			} else if (list.get(tempLeft).compareTo(list.get(tempMiddle)) > 0) {
				tempList.add(list.get(tempMiddle));
				tempMiddle++;
			} else {
				tempList.add(list.get(tempLeft));
				tempLeft++;

				tempList.add(list.get(tempMiddle));
				tempMiddle++;
			}
		}

		while (tempLeft <= middle) {
			tempList.add(list.get(tempLeft));
			tempLeft++;
		}

		while (tempMiddle <= high) {
			tempList.add(list.get(tempMiddle));
			tempMiddle++;
		}

		for (int i = 0; i < tempList.size(); i++) {
			list.set(low + i, tempList.get(i));
		}
	}

	public void mergesort(List<E> list) {
		mergesort(list, 0, list.size() - 1);
	}
}
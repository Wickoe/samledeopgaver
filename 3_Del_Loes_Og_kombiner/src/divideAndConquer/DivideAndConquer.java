package divideAndConquer;

import java.util.List;

public class DivideAndConquer {
	public int sum(List<Integer> list) {
		return sumOf(list, 0, list.size() - 1);
	}

	private int sumOf(List<Integer> list, int start, int end) {
		int sum = 0;
		if (start == end) {
			sum = list.get(start);
		} else {
			int middle = (start + end) / 2;

			sum = sumOf(list, start, middle) + sumOf(list, middle + 1, end);
		}

		return sum;
	}

	public int countZeros(List<Integer> list) {
		return countZeros(list, 0, list.size() - 1);
	}

	private int countZeros(List<Integer> list, int start, int end) {
		int zeros = 0;

		if (start == end && list.get(start) == 0) {
			zeros++;
		} else if (start < end) {
			int middle = (start + end) / 2;

			zeros = countZeros(list, start, middle) + countZeros(list, middle + 1, end);
		}

		return zeros;
	}
}

package divideAndConquer;

import java.util.List;

public class Quicksort<E extends Comparable<E>> {
	public void quicksort(List<E> list) {
		quickSort(list, 0, list.size() - 1);
	}

	private void quickSort(List<E> list, int low, int high) {
		if (low < high) {
			int p = partition(list, low, high);
			quickSort(list, low, p - 1);
			quickSort(list, p + 1, high);
		}
	}

	/**
	 * Krav: low <= high Returnerer p hvor der i list[low...high] er lavet en
	 * ombytning så list[low...p-1] <= list[p] < list[p+1...high]
	 */
	private int partition(List<E> list, int low, int high) {
		E e = list.get(low);
		int i = low + 1;
		int j = high;
		while (i <= j) {
			if (list.get(i).compareTo(e) <= 0) {
				i++;
			} else if (list.get(j).compareTo(e) > 0) {
				j--;
			} else {
				swap(list, i, j);
				i++;
				j--;
			}
		}

		swap(list, low, j);

		return j;
	}

	private void swap(List<E> list, int k, int l) {
		E e = list.get(k);
		list.set(k, list.get(l));
		list.set(l, e);
	}
}

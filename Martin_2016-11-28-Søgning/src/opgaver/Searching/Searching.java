package opgaver.Searching;

import java.util.ArrayList;

import searching_exercises.Player;

/**
 * Models a collections of search methods
 *
 * @author Martin
 *
 */
public class Searching {
	/**
	 * Returns a boolean wether or not a given tabel contains an uneven number.
	 *
	 * @param tabel
	 *            the tabel to search thorugh
	 * @return true if the tabel contains an uneven number else false
	 */
	public static boolean findUneven(int[] tabel) {
		boolean found = false;
		int indexAt = 0;

		while (!found && indexAt < tabel.length) {
			found = tabel[indexAt] % 2 == 1;

			indexAt++;
		}

		return found;
	}

	/**
	 * Search for a number from a table in a specific interval.
	 *
	 * @param startInterval
	 *            the start of the interval
	 * @param endInterval
	 *            the end of the interval
	 * @param table
	 *            the table to search through
	 * @return the first found number between the interval start and end, else
	 *         -1
	 */
	public static int numberBwtweenInterval(int startInterval, int endInterval, int[] table) {
		int number = -1;
		boolean found = false;
		int indexAt = 0;

		while (!found && indexAt < table.length) {
			int tempNumber = table[indexAt];

			if (startInterval <= tempNumber && tempNumber <= endInterval) {
				found = true;
				number = tempNumber;
			} else {
				indexAt++;
			}
		}

		return number;
	}

	/**
	 * Returns whether or not if two side by side numbers in a table are the
	 * same number.
	 *
	 * @param table
	 *            the table to search through
	 * @return true if the exists two same numbers side by side in the table,
	 *         else false
	 */
	public static boolean sideBySideSameNumber(int[] table) {
		boolean found = false;
		int indexAt = 1;

		while (!found && indexAt < table.length) {
			found = table[indexAt] == table[indexAt - 1];
			indexAt++;
		}

		return found;
	}

	/**
	 * Finds and returns a given player with a specific goal score.
	 *
	 * @param players
	 *            the list of players to search through
	 * @param score
	 *            the score the players must have scored
	 * @return the player which have scored equal to or more than the given
	 *         score
	 */
	public Player findScoreLinear(ArrayList<Player> players, int score) {
		Player player = null;
		boolean found = false;
		int indexAt = 0;

		while (!found && indexAt < players.size()) {
			Player tempPlayer = players.get(indexAt);
			found = tempPlayer.getScore() >= score;

			if (found) {
				player = tempPlayer;
			} else {
				indexAt++;
			}
		}

		return player;
	}

	/**
	 * Finds and returns a given player with a specific goal score.
	 *
	 * @param players
	 *            the list of players to search through
	 * @param score
	 *            the score the players must have scored
	 * @return the player which have scored equal to or more than the given
	 *         score
	 */
	public Player findScoreBinary(ArrayList<Player> players, int score) {
		boolean found = false;
		int left = 0;
		int right = players.size() - 1;
		int middle;
		Player player = null;

		while (!found && left <= right) {
			middle = (right + left) / 2;

			Player tempPlayer = players.get(middle);
			found = tempPlayer.getScore() == score;

			if (found) {
				player = tempPlayer;
			} else if (score < tempPlayer.getScore()) {
				left = middle + 1;
			} else {
				right = middle - 1;
			}
		}

		return player;
	}

	/**
	 * Finds and returns the player shorter than 170 cm and have scored more
	 * than 50 goals
	 *
	 * @param players
	 *            the players to sort through
	 * @return the name of the player who is shorter than 170 and score more
	 *         than 50 goals
	 */
	public String shortTopPlayer(ArrayList<Player> players) {
		boolean found = false;
		int indexAt = 0;
		Player player = null;

		while (!found && indexAt < players.size()) {
			Player tempPlayer = players.get(indexAt);

			found = tempPlayer.getHeight() < 170 && tempPlayer.getScore() > 50;

			if (found) {
				player = tempPlayer;
			} else {
				indexAt++;
			}
		}

		String name = "";

		if (found) {
			name = player.getName();
		}

		return name;
	}

	/**
	 * Finds and returns the first integer square root of a given integer n
	 * using linear search.
	 *
	 * @param n
	 *            the integer to find the square root of
	 * @return the integer square root of n
	 */
	public static int linearIntegerSquareRootOf(int n) {
		boolean found = false;
		int integerAt = 1;

		while (!found && integerAt < n / 2.0) {
			found = integerAt * integerAt > n;

			if (!found) {
				integerAt++;
			}
		}

		return integerAt--;
	}

	/**
	 * Finds and returns the first integer square root of a given integer n
	 * using binary search.
	 *
	 * @param n
	 *            the integer to find the square root of
	 * @return the integer square root of n
	 */
	public static int binaryIntegerSquareRootOf(int n) {
		// TODO
		return -1;
	}

	/**
	 * Returnér positionen på n efter det evt. er flyttet én position til
	 * venstre. Hvis n ikke findes, returneres -1.
	 */
	public static int find(ArrayList<Integer> list, int n) {
		boolean found = false;
		int indexAt = 0;

		while (!found && indexAt < list.size()) {
			found = list.get(indexAt) == n;

			if (!found) {
				indexAt++;
			}
		}

		if (found && indexAt > 0) {
			int temp = list.get(indexAt - 1);
			list.set(indexAt - 1, list.get(indexAt));
			list.set(indexAt, temp);
			indexAt--;
		} else if (!found) {
			indexAt = -1;
		}

		return indexAt;
	}
}

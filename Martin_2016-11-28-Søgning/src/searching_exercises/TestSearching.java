package searching_exercises;

import java.util.Arrays;

import opgaver.Searching.Searching;

public class TestSearching {
	
	public static void main(String[] args) {

	    // Test for exercise 1
        int[] array1 = {2, 4, 8, 2};
        System.out.println("Array: " + Arrays.toString(array1));
        System.out.println("Are there uneven numbers in the array? " + Searching.findUneven(array1));
        array1[2] = 15;
        System.out.println("Array: " + Arrays.toString(array1));
        System.out.println("Are there uneven numbers in the array? " + Searching.findUneven(array1));
        System.out.println();
        
		// TODO: test exercise 2-7
	}

}

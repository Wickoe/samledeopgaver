package aflevering;

import java.util.ArrayList;

import dictionaryelev.Dictionary;

public class DictionaryArrayList<K, V> implements Dictionary<K, V> {
	private ArrayList<Node>[] tabel;
	private static int N;

	private class Node {
		private K key;
		private V value;

		@Override
		public boolean equals(Object o) {
			if (o == null || !(o instanceof DictionaryArrayList.Node)) {
				return false;
			}

			@SuppressWarnings("unchecked")
			DictionaryArrayList<K, V>.Node node = (DictionaryArrayList<K, V>.Node) o;

			return key.equals(node.key);
		}
	}

	public DictionaryArrayList() {
		this(10);
	}

	@SuppressWarnings("unchecked")
	public DictionaryArrayList(int nSize) {
		tabel = new ArrayList[nSize];
		N = nSize;

		for (int i = 0; i < N; i++) {
			tabel[i] = new ArrayList<>();
		}
	}

	@Override
	public V get(K key) {
		Node node = new Node();

		if (key != null) {
			int hashValue = key.hashCode() % N;

			ArrayList<Node> alist = tabel[hashValue];

			node.key = key;

			int indexAt = tabel[hashValue].indexOf(node);

			if (indexAt >= 0) {
				node = alist.get(indexAt);
			}
		}

		return node.value;
	}

	@Override
	public boolean isEmpty() {
		boolean isEmpty = true;
		int indexAt = 0;

		while (isEmpty && indexAt < N) {
			isEmpty = tabel[indexAt].isEmpty();
			indexAt++;
		}

		return isEmpty;
	}

	@Override
	public V put(K key, V value) {
		if (key != null && value != null) {
			Node node = new Node();
			node.key = key;

			int hashValue = key.hashCode() % N;

			ArrayList<Node> aList = tabel[hashValue];

			int indexAt = aList.indexOf(node);

			if (indexAt >= 0) {
				node = aList.remove(indexAt);
			}

			V removedValue = node.value;
			node.value = value;
			aList.add(node);

			return removedValue;
		}

		return null;
	}

	@Override
	public V remove(K key) {
		if (key != null) {
			Node node = new Node();
			node.key = key;

			int hashValue = key.hashCode() % N;

			ArrayList<Node> aList = tabel[hashValue];

			int indexAt = aList.indexOf(node);

			if (indexAt >= 0) {
				node = aList.remove(indexAt);
			}

			return node.value;
		}

		return null;
	}

	@Override
	public int size() {
		int size = 0;

		for (int i = 0; i < N; i++) {
			size += tabel[i].size();
		}

		return size;
	}
}

package aflevering;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class DictionaryArrayListTest {
	private DictionaryArrayList<Integer, String> testMap;

	@Before
	public void setUp() throws Exception {
		testMap = new DictionaryArrayList<>();
	}

	@Test
	public void testPut() {
		assertTrue(testMap.size() == 0);
		assertTrue(testMap.isEmpty());

		testMap.put(1, "Martin");
		assertTrue(testMap.size() == 1);
		assertFalse(testMap.isEmpty());

		testMap.put(2, "Martin");
		testMap.put(3, "Martin");
		
		assertTrue(testMap.size() == 3);
		assertFalse(testMap.isEmpty());
	}
	
	@Test
	public void testGet() {
		testMap.put(1, "Martin");
		testMap.put(2, "Kristian");
		testMap.put(3, "Leo");
		
		assertTrue(testMap.size() == 3);
		assertFalse(testMap.isEmpty());
		
		assertEquals("Martin", testMap.get(1));
		assertTrue(testMap.size() == 3);
		assertFalse(testMap.isEmpty());
		
		assertEquals("Kristian", testMap.get(2));
		assertTrue(testMap.size() == 3);
		assertFalse(testMap.isEmpty());
	}

	@Test
	public void testRemove() {
		testMap.put(1, "Martin");
		testMap.put(2, "Kristian");
		testMap.put(3, "Leo");
		
		assertTrue(testMap.size() == 3);
		assertFalse(testMap.isEmpty());
		
		assertEquals("Martin", testMap.remove(1));
		assertTrue(testMap.size() == 2);
		assertFalse(testMap.isEmpty());
		assertNull(testMap.get(1));
		
		assertEquals("Kristian", testMap.remove(2));
		assertTrue(testMap.size() == 1);
		assertFalse(testMap.isEmpty());
		assertNull(testMap.get(2));

		assertEquals("Leo", testMap.remove(3));
		assertTrue(testMap.size() == 0);
		assertTrue(testMap.isEmpty());
		assertNull(testMap.get(3));
	}

	@Test
	public void testSize() {
		assertTrue(testMap.size() == 0);
		assertTrue(testMap.isEmpty());

		testMap.put(1, "Martin");
		testMap.put(2, "Kristian");
		
		assertTrue(testMap.size() == 2);
		assertFalse(testMap.isEmpty());
		
		testMap.put(3, "Leo");
		
		assertTrue(testMap.size() == 3);
		assertFalse(testMap.isEmpty());
		
		testMap.put(1, "Leo");
		
		assertTrue(testMap.size() == 3);
		assertFalse(testMap.isEmpty());
	}
	
	@Test
	public void testIsEmpty() {
		assertTrue(testMap.isEmpty());
		
		testMap.put(1, "Martin");
		
		assertFalse(testMap.isEmpty());
		
		testMap.put(1, "Leo");
		
		assertFalse(testMap.isEmpty());
		
		testMap.get(1);
		
		assertFalse(testMap.isEmpty());
		
		testMap.remove(1);
		
		assertTrue(testMap.isEmpty());
		
		testMap.put(1, "Leo");
		testMap.put(2, "Martin");
		
		assertFalse(testMap.isEmpty());
		
		testMap.remove(2);
		
		assertFalse(testMap.isEmpty());
		
		testMap.remove(1);
		
		assertTrue(testMap.isEmpty());
	}
}
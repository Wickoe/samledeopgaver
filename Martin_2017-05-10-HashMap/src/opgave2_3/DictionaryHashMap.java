package opgave2_3;

import dictionaryelev.Dictionary;

public class DictionaryHashMap<K, V> implements Dictionary<K, V> {
	private java.util.Map<K, V>[] tabel;
	private static int N = 10;

	/**
	 * HashingMap constructor comment.
	 */
	@SuppressWarnings("unchecked")
	public DictionaryHashMap() {
		tabel = new java.util.HashMap[N];
		for (int i = 0; i < N; i++) {
			tabel[i] = new java.util.HashMap<>();
		}
	}

	@Override
	public V get(K key) {
		int i = key.hashCode() % N;
		java.util.Map<K, V> m = tabel[i];
		return m.get(key);
	}

	@Override
	public boolean isEmpty() {
		boolean empty = true;
		int i = 0;
		while (empty && i < N) {
			empty = tabel[i].isEmpty();
			i++;
		}
		return empty;
	}

	@Override
	public V put(K key, V value) {
		// TODO
		return null;
	}

	@Override
	public V remove(K key) {
		// TODO
		return null;
	}

	@Override
	public int size() {
		// TODO
		return 0;
	}

}

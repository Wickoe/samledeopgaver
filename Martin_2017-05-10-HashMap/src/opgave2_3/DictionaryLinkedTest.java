package opgave2_3;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import dictionaryelev.Dictionary;

public class DictionaryLinkedTest {
	private Dictionary<Integer, String> dictionary;
	
	@Before
	public void setUp() throws Exception {
		dictionary = new DictionaryLinked<>();
	}

	@Test
	public void testIsEmpty() {
		assertTrue(dictionary.isEmpty());
		assertTrue(dictionary.size() == 0);

		dictionary.put(3, "Viggo");
		dictionary.put(8, "Hans");

		assertFalse(dictionary.isEmpty());
		assertTrue(dictionary.size() == 2);

		dictionary.put(7, "Bent");
		
		assertFalse(dictionary.isEmpty());
		assertTrue(dictionary.size() == 3);

		dictionary.remove(7);
		dictionary.remove(8);
		dictionary.remove(3);

		assertTrue(dictionary.isEmpty());
		assertTrue(dictionary.size() == 0);
	}
}

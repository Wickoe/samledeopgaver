package opgave2_3;

import dictionaryelev.Dictionary;

public class DictionaryLinked<K, V> implements Dictionary<K, V> {
	private Node start;
	private int size;

	/**
	 * HashingMap constructor comment.
	 */
	public DictionaryLinked() {
		// Sentinel (tomt listehoved - der ikke indeholder data)
		start = new Node();
		size = 0;
	}

	@Override
	public V get(K key) {
		Node nodeAt = start.next;
		boolean found = false;
		
		while (!found && nodeAt != null) {
			found = key.equals(nodeAt.key);
			if (!found) {
				nodeAt = nodeAt.next;
			}
		}
		
		V data = null;
		
		if (found) {
			data = nodeAt.value;
		}

		return data;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public V put(K key, V value) {
		Node nodeAt = start.next;
		Node previous = start;
		boolean found = false;

		while (!found && nodeAt != null) {
			found = key.equals(nodeAt.key);

			if (!found) {
				previous = nodeAt;
				nodeAt = nodeAt.next;
			}
		}
		
		if (!found) {
			nodeAt = new Node();
			nodeAt.key = key;
			previous.next = nodeAt;
			size++;
		}

		nodeAt.value = value;
		
		return nodeAt.value;
	}

	@Override
	public V remove(K key) {
		Node nodeAt = start.next;
		Node previous = start;
		boolean found = false;

		while (!found && nodeAt != null) {
			found = key.equals(nodeAt.key);

			if (!found) {
				previous = nodeAt;
				nodeAt = nodeAt.next;
			}
		}
		
		V data = null;
		
		if (found) {
			data = nodeAt.value;
			previous.next = nodeAt.next;
			size--;
		}

		return data;
	}

	@Override
	public int size() {
		return size;
	}

	private class Node {
		Node next;
		K key;
		V value;
	}
}

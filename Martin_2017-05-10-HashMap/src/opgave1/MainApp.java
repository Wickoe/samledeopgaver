package opgave1;

import java.util.HashSet;

import javafx.scene.paint.Color;

public class MainApp {
	public static void main(String[] args) {
		HashSet<Bil> biler = new HashSet<>();
		
		Bil b1 = new Bil("xxxxxxx", "xx", "yy", Color.BLACK);
		Bil b2 = new Bil("yyyyyyy", "yy", "xx", Color.WHITE);
		Bil b3 = new Bil("xyxyxyx", "xy", "xy", Color.GRAY);
		
		biler.add(b1);
		biler.add(b2);
		biler.add(b3);

		System.out.println(biler);
		
		Bil b4 = new Bil("xxxxxxx", "xx", "yy", Color.BLACK);
		Bil b5 = new Bil("yyyyyyy", "yy", "xx", Color.WHITE);
		Bil b6 = new Bil("xyxyxyx", "xy", "xy", Color.GRAY);

		biler.add(b4);
		biler.add(b5);
		biler.add(b6);
		
		System.out.println(biler);
	}
}

package opgave1;

import javafx.scene.paint.Color;

public class Bil {
	private String registreringsnummer;
	private String mærke;
	private String model;
	private Color farve;

	public Bil(String etRegistreringsnummer, String etMærke, String enModel, Color enFarve) {
		this.registreringsnummer = etRegistreringsnummer;
		this.mærke = etMærke;
		this.model = enModel;
		this.farve = enFarve;
	}

	public String getRegistreringsnummer() {
		return registreringsnummer;
	}

	public void setRegistreringsnummer(String registreringsnummer) {
		this.registreringsnummer = registreringsnummer;
	}

	public String getMærke() {
		return mærke;
	}

	public void setMærke(String mærke) {
		this.mærke = mærke;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Color getFarve() {
		return farve;
	}

	public void setFarve(Color farve) {
		this.farve = farve;
	}

	@Override
	public String toString() {
		return registreringsnummer + " " + mærke + " " + model + " " + farve.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Bil)) {
			return false;
		}

		Bil bilO = (Bil) o;

		return registreringsnummer.equalsIgnoreCase(bilO.getRegistreringsnummer())
			&& mærke.equalsIgnoreCase(bilO.getMærke());
	}

	@Override
	public int hashCode() {
		int hashCode = 31 * (registreringsnummer.hashCode() + mærke.hashCode() + model.hashCode())
			+ farve.hashCode() * 7;
		return hashCode;
	}
}

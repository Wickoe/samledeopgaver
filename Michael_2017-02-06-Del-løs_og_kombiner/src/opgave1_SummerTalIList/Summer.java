package opgave1_SummerTalIList;

import java.util.ArrayList;

public class Summer {

    public static void main(String[] args) {
        ArrayList<Integer> liste = new ArrayList<>();
        liste.add(10);
        liste.add(1);
        liste.add(1);
        liste.add(10);

        System.out.println(sum(liste));
    }
    
    public static int sum(ArrayList<Integer> list) {
        return summer(list, 0, list.size() - 1);
    }

    private static int summer(ArrayList<Integer> list, int low, int high) {
        int tal = 0;
        
        if (low == high) {
            tal = list.get(high);
        }
        else {
            int middle = (low + high) / 2;
            int sum1 = summer(list, low, middle);
            int sum2 = summer(list, middle + 1, high);
            tal = sum1 + sum2;
        }
        return tal;
    }
}

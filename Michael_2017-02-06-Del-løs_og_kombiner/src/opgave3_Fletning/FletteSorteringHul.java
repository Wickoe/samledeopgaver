package opgave3_Fletning;

import java.util.ArrayList;

public class FletteSorteringHul {
    
    // den metode der saetter fletningen i gang
    public void fletteSort(ArrayList<Integer> list) {
        mergesort(list, 0, list.size() - 1);
    }
    
    // den rekursive metode der implementere del-loes og kombiner skabelonen
    private void mergesort(ArrayList<Integer> list, int l, int h) {
        if (l < h) {
            int m = (l + h) / 2;
            mergesort(list, l, m);
            mergesort(list, m + 1, h);
            merge(list, l, m, h);
        }
    }
    
    // kombiner er realiseret ved fletteskabelonen
    private void merge(ArrayList<Integer> list, int low, int middle, int high) {
        ArrayList<Integer> temp = new ArrayList<>();
        
        int i = low;
        int j = middle + 1;
        
        while (i <= middle && j <= high) {
            if (list.get(i) < list.get(j)) {
                temp.add(list.get(i));
                i++;
            }
            else {
                temp.add(list.get(j));
                j++;
            }
        }
        
        while (i <= middle) {
            temp.add(list.get(i));
            i++;
        }
        
        while (j <= high) {
            temp.add(list.get(j));
            j++;
        }
        for (i = 0; i < temp.size(); i++) {
            list.set(low + i, temp.get(i));
        }
    }
}

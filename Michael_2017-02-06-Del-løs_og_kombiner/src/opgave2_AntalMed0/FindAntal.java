package opgave2_AntalMed0;

import java.util.ArrayList;

public class FindAntal {
    
    public static void main(String[] args) {
        ArrayList<Integer> liste = new ArrayList<>();
        liste.add(1);
        liste.add(0);
        liste.add(1);
        liste.add(0);

        System.out.println(findAntal(liste));
    }
    
    public static int findAntal(ArrayList<Integer> liste) {
        return findAntalIListe(liste, 0, liste.size() - 1);
    }
    
    private static int findAntalIListe(ArrayList<Integer> liste, int low, int high) {
        if (low == high) {
            if (liste.get(low) == 0) {
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            int middle = (low + high) / 2;
            int zero1 = findAntalIListe(liste, low, middle);
            int zero2 = findAntalIListe(liste, middle + 1, high);

            return zero1 + zero2;
        }
    }
}

package opgaver.Mekaniker;

/**
 * Models a car mechanic with year of final examen, an hourly wage and extends
 * Person.
 *
 * @author Martin
 *
 */
public class Mekaniker extends Person {
	private int svendeprøve;
	private double timeLøn;
	private final int weeklyWorkHours;

	/**
	 * Initialize a car mechanic as a specialization of person with a final
	 * examen year and an hourly wage.
	 *
	 * @param etNavn
	 *            the name of the car mechanic
	 * @param enAdresse
	 *            the address of the car mechanic
	 * @param etSvendeprøveÅr
	 *            the year of the final examen
	 * @param enTimeLøn
	 *            the hourly wage
	 */
	public Mekaniker(String etNavn, String enAdresse, int etSvendeprøveÅr, double enTimeLøn, int weeklyWorkHours) {
		super(etNavn, enAdresse);

		this.svendeprøve = etSvendeprøveÅr;
		this.timeLøn = enTimeLøn;
		this.weeklyWorkHours = weeklyWorkHours;
	}

	/**
	 * Returns the year of the final examen.
	 *
	 * @return finalExamen the year of the final examen
	 */
	public int getSvendeprøve() {
		return svendeprøve;
	}

	/**
	 * Returns the hourly wage of the car mechanic.
	 *
	 * @return hourlyWage the hourly wage of the car mechanic
	 */
	public double getTimeløn() {
		return timeLøn;
	}

	/**
	 * Calculate the weekly earns for the mechanic.
	 *
	 * @return weeklyEarn the weekly earn by the mechanic
	 */
	public double beregnUgeLøn() {
		return weeklyWorkHours * timeLøn;
	}
}

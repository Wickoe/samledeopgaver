package opgaver.Mekaniker;

/**
 * Models an car mechanic inspector
 *
 * @author Martin
 *
 */
public class Synsmand extends Mekaniker {
	private int syn;

	/**
	 * Initialize a car mechanic inspector with a name, an address, a year of
	 * final examen taken, an hourly wage and the weekly work hours.
	 *
	 * @param etNavn
	 *            the name of the car mechanic inspector
	 * @param enAdresse
	 *            the address of the car mechanic inspector
	 * @param etSvendeprøveÅr
	 *            the year of the final examen taken
	 * @param enTimeLøn
	 *            the hourly wage
	 * @param weeklyWorkHours
	 *            the weekly work hours
	 */
	public Synsmand(String etNavn, String enAdresse, int etSvendeprøveÅr, double enTimeLøn, int weeklyWorkHours) {
		super(etNavn, enAdresse, etSvendeprøveÅr, enTimeLøn, weeklyWorkHours);

		syn = 0;
	}

	/**
	 * Perform a car inspection.
	 */
	public void udførSyn() {
		syn++;
	}

	@Override
	public double beregnUgeLøn() {
		return super.beregnUgeLøn() + (250 * syn);
	}
}

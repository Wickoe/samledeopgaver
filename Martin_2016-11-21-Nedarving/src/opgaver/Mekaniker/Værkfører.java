package opgaver.Mekaniker;

/**
 * Models a mechanic foreman that extends mechanic
 *
 * @author Martin
 *
 */
public class Værkfører extends Mekaniker {
	private int værkfører;
	private double ugeTillæg;

	/**
	 * Initialize a foreman with a name, an address, a year for final examen
	 * taken, an hourly wage, the year of promotion and a weekly bonus.
	 *
	 * @param etNavn
	 *            the name of the foreman
	 * @param enAdresse
	 *            the address of the foreman
	 * @param etSvendeprøveÅr
	 *            the year of the final exam taken
	 * @param enTimeLøn
	 *            the hourly wage
	 * @param værkføreUdnævenelse
	 *            the year of promotion
	 * @param lønTillæg
	 *            the weekly bonus
	 */
	public Værkfører(String etNavn, String enAdresse, int etSvendeprøveÅr, double enTimeLøn, int weeklyWorkHours,
			int værkføreUdnævenelse, double lønTillæg) {
		super(etNavn, enAdresse, etSvendeprøveÅr, enTimeLøn, weeklyWorkHours);

		værkfører = værkføreUdnævenelse;
		ugeTillæg = lønTillæg;
	}

	/**
	 * Returns the year of promotion.
	 *
	 * @return værkføre the year of promotion
	 */
	public int getVærkføreÅr() {
		return værkfører;
	}

	/**
	 * Returns the weekly bonus.
	 *
	 * @return ugeTillæg the weekly bonus
	 */
	public double getUgeTillæg() {
		return ugeTillæg;
	}

	@Override
	public double beregnUgeLøn() {
		return super.beregnUgeLøn() + ugeTillæg;
	}
}

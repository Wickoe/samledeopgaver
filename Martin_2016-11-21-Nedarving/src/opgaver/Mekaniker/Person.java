package opgaver.Mekaniker;

/**
 * Models a person with a name and an address
 *
 * @author Martin
 *
 */
public class Person {
	private String navn, adresse;

	/**
	 * Initialize a person with a name and an address.
	 *
	 * @param etNavn
	 *            the name of the person
	 * @param enAdresse
	 *            the address of the person
	 */
	public Person(String etNavn, String enAdresse) {
		navn = etNavn;
		adresse = enAdresse;
	}

	/**
	 * Returns the address of the person
	 *
	 * @return address the address of the person
	 */
	public String getAddress() {
		return adresse;
	}

	/**
	 * Returns the name of the person.
	 *
	 * @return name the name of the person
	 */
	public String getNavn() {
		return navn;
	}
}

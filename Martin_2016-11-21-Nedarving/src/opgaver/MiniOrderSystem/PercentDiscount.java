package opgaver.MiniOrderSystem;

/**
 * Models a percent wise discount for prices.
 *
 * @author Martin
 *
 */
public class PercentDiscount extends Discount {
	private int percent;

	/**
	 * Initialize a percent discount with a percent rate.
	 * 
	 * @param percentRate
	 *            the percent rate discount given by the discount
	 */
	public PercentDiscount(int percentRate) {
		percent = percentRate;
	}

	@Override
	public double getDiscountedPrice(double originalPrice) {
		return originalPrice - (originalPrice * (1 - (100 - percent) / 100));
	}
}

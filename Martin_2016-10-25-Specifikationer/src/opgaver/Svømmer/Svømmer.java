package opgaver.Svømmer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Modellerer en svømmer
 */
public class Svømmer {
	private String navn;
	private String klub;
	private LocalDate fødselsdag;
	private ArrayList<Double> tider;

	/**
	 * Initialiserer en ny svømmer med navn, fødselsdag, klub og tider.
	 */
	public Svømmer(String etNavn, String enKlub, LocalDate enFødselsdag, List<Double> tider) {
		navn = etNavn;
		klub = enKlub;
		fødselsdag = enFødselsdag;
		this.tider = new ArrayList<>(tider);
	}

	/**
	 * Returnerer svømmerens navn.
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Returnerer svømmerens årgang.
	 */
	public int getÅrgang() {
		return fødselsdag.getYear();
	}

	/**
	 * Returnerer svømmerens klub.
	 */
	public String getKlub() {
		return klub;
	}

	/**
	 * Registrerer svømmerens klub
	 *
	 * @param klub
	 */
	public void setKlub(String klub) {
		this.klub = klub;
	}

	/**
	 * Returnerer den hurtigste tid svømmeren har opnået
	 */
	public double bedsteTid() {
		double bedsteTid = tider.get(0);
		for (int i = 1; i < tider.size(); i++) {
			double tempTid = tider.get(i);

			if (tempTid < bedsteTid) {
				bedsteTid = tempTid;
			}
		}

		return bedsteTid;
	}

	/**
	 * Returnerer gennemsnittet af de tider svømmeren har opnået
	 */
	public double gennemsnitAfTid() {
		double sumOfTider = 0;

		for (double tid : tider) {
			sumOfTider += tid;
		}
		return sumOfTider / tider.size();
	}

	/**
	 * Returnerer gennemsnittet af de tider svømmeren har opnået idet den
	 * langsomste tid ikke er medregnet
	 */
	public double snitUdenDårligste() {
		double sum = 0;
		double langsomstTid = tider.get(0);

		for (double tid : tider) {
			if (tid > langsomstTid) {
				sum += langsomstTid;
				langsomstTid = tid;
			} else {
				sum += tid;
			}
		}
		return sum / tider.size();
	}
}

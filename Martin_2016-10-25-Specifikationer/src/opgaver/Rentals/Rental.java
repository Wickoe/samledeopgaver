package opgaver.Rentals;

import java.time.LocalDate;

/**
 * Models a rental with a given start day, a price, number of days of rental and
 * specific id number
 *
 * @author Martin
 *
 */
public class Rental {
	private int number;
	private int days;
	private LocalDate startDate;
	private double price;

	/**
	 * Initialize a rental with a given number of days, a given price per day, a
	 * given start date and a specific id number
	 *
	 * @Pre: Days cannot be negative
	 * @Pre: Price cannot be negative
	 * @Pre: number is unique
	 *
	 * @param number
	 *            the number of the rental
	 * @param days
	 *            the number of days of the rental
	 * @param price
	 *            the price for the rental per day
	 * @param startDate
	 *            the start date of the rental
	 */
	public Rental(int number, int days, double price, LocalDate startDate) {
		this.number = number;
		this.days = days;
		this.price = price;
		this.startDate = startDate;
	}

	/**
	 * Returns the price per day for the rental.
	 *
	 * @return price the price per day for the rental
	 */
	public double getPricePerDay() {
		return price;
	}

	/**
	 * Sets the span of days for the rental.
	 *
	 * @param days
	 *            the amount of days the rental spans over
	 */
	public void setDays(int days) {
		this.days = days;
	}

	/**
	 * Returns the span of days of the rental
	 *
	 * @return days the span of days of the rental
	 */
	public int getDays() {
		return days;
	}

	/**
	 * Returns the startDate of the rental.
	 *
	 * @return startDate the start date of the rental
	 */
	public LocalDate getStartDate() {
		return startDate;
	}

	/**
	 * Calculate the end date of the rental and returns it.
	 *
	 * @return endDate the end date of the rental
	 */
	public LocalDate getEndDate() {
		return startDate.plusDays(days);
	}

	/**
	 * Calculate the total price for the complete rental and returns it.
	 *
	 * @return totalPrice the total price of the rental
	 */
	public double getTotalPrice() {
		return price * days;
	}

	/**
	 * Returns the unique number of the rental.
	 *
	 * @return number the unique number of the rental
	 */
	public int getNumber() {
		return number;
	}
}

package opgaver.Rentals;

import java.time.LocalDate;

public class RentalTest {
	public static void main(String[] args) {
		Rental rentalOne = new Rental(1, 5, 75, LocalDate.now().plusMonths(1));
		Rental rentalTwo = new Rental(2, 10, 750, LocalDate.now().plusMonths(10));

		System.out.println("Rental one:");
		System.out.println("\tRental number: " + rentalOne.getNumber());
		System.out.println("\tRental start date: " + rentalOne.getStartDate());
		System.out.println("\tRental end date: " + rentalOne.getEndDate());
		System.out.println("\tRental days: " + rentalOne.getDays());
		System.out.println("\tRental price per day: " + rentalOne.getPricePerDay());
		System.out.println("\tRental total price: " + rentalOne.getTotalPrice());

		System.out.println();
		System.out.println("Rental two:");
		System.out.println("\tRental number: " + rentalTwo.getNumber());
		System.out.println("\tRental start date: " + rentalTwo.getStartDate());
		System.out.println("\tRental end date: " + rentalTwo.getEndDate());
		System.out.println("\tRental days: " + rentalTwo.getDays());
		System.out.println("\tRental price per day: " + rentalTwo.getPricePerDay());
		System.out.println("\tRental total price: " + rentalTwo.getTotalPrice());
	}
}

package opgaver.Overvejelser;

/**
 * Models an Bank account with a balance.
 *
 * @author Martin (ECLIPSE, I'M MAD!!!! Apparently I'm not allowed to be called
 *         'Martin' since I'm "wrongly spelled word"-worning spelling my OWN
 *         name)
 *
 */
public class BankAccount {
	private static int IDs = 0;
	private int id;
	private double balance;

	/**
	 * Instantiate an object of type BankAcoount with given initial balance.
	 *
	 * @param initialBalance
	 *            the initial balance of the bank account
	 * @Pre: initialBalance cannot be below 0
	 */
	public BankAccount(double initialBalance) {
		id = IDs;
		BankAccount.IDs++;
		balance = initialBalance;
	}

	/**
	 * Deposit a given amount into the bank account.
	 *
	 * @Pre: amount cannot be below 0
	 * @param amount
	 *            the amount to be deposited
	 */
	public void deposit(double amount) {
		balance += amount;
	}

	/**
	 * Withdraw a given amount from the bank account.
	 *
	 * @Pre: amount cannot be below 0.
	 * @param amount
	 *            the amount to be withdrawn
	 */
	public void withdraw(double amount) {
		balance -= amount;
	}

	/**
	 * Returns the current balance of the bank account.
	 *
	 * @return balance the current balance of the bank account
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * Returns the id of the bank account.
	 *
	 * @return id the id of the bank account.
	 */
	public int getId() {
		return id;
	}
}
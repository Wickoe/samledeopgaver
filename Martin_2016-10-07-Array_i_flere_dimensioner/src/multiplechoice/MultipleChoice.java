package multiplechoice;

public class MultipleChoice {
	private char[] correctAnswer = { 'B', 'C', 'A', 'A', 'B', 'A', 'C', 'D', 'A', 'C' };
	private char[][] studentAnswers;

	public MultipleChoice(int numberOfStudents) {
		studentAnswers = new char[numberOfStudents][10];
		for (int i = 0; i < studentAnswers.length; i++) {
			for (int j = 0; j < studentAnswers[i].length; j++) {
				int tal = (int) (Math.random() * 4); // tilfældigt tal fra 0-3
				char c = (char) (tal + 'A'); // laver tallet om til en char fra
												// A-C
				studentAnswers[i][j] = c;
			}
		}
	}

	public void printCorrectAnwsers() {
		System.out.print("Correct anwsers: ");
		for (int i = 0; i < correctAnswer.length; i++) {
			if (i != 0) {
				System.out.print(", ");
			}

			System.out.print(correctAnswer[i]);
		}
	}

	public void printStudentAnswers() {
		for (int i = 0; i < studentAnswers.length; i++) {
			System.out.print("Student " + (i + 1) + " resultat: ");
			for (int j = 0; j < studentAnswers[i].length; j++) {
				if (j != 0) {
					System.out.print(", ");
				}
				System.out.print(studentAnswers[i][j]);
			}
			System.out.println();
		}
	}

	/**
	 * Udskriver for hver studerende antallet af rigtige svar
	 */
	public void printCorrectAnswersPrStudent() {
		for (int i = 0; i < studentAnswers.length; i++) {
			int correctAnwsers = 0;

			for (int j = 0; j < studentAnswers[i].length; j++) {
				if (studentAnswers[i][j] == correctAnswer[j]) {
					correctAnwsers++;
				}
			}

			System.out.println("Student " + (i + 1) + " has " + correctAnwsers + " anwsers");
		}
	}

	/**
	 * Udskriver for hvert spørgsmål antallet af rigtige svar
	 */
	public void printCorrectAnswerPrQuestion() {
		for (int i = 0; i < correctAnswer.length; i++) {
			int studentsAnwseringCorrect = 0;

			for (int j = 0; j < studentAnswers.length; j++) {
				if (correctAnswer[i] == studentAnswers[j][i]) {
					studentsAnwseringCorrect++;
				}
			}

			System.out.println("Question " + (i + 1) + " correct anwsers: " + studentsAnwseringCorrect);
		}
	}

}

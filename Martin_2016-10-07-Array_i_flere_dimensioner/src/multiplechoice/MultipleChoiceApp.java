package multiplechoice;

public class MultipleChoiceApp {
	public static void main(String[] args) {
		MultipleChoice mc = new MultipleChoice(4);

		mc.printCorrectAnwsers();
		System.out.println();
		System.out.println();
		mc.printStudentAnswers();
		System.out.println();
		mc.printCorrectAnswersPrStudent();
		System.out.println();
		mc.printCorrectAnswerPrQuestion();
	}

}

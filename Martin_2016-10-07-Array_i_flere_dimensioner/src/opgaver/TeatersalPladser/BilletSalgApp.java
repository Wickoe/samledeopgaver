package opgaver.TeatersalPladser;

public class BilletSalgApp {
	public static void main(String[] args) {
		int[][] pladser = { { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }, { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 },
				{ 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, }, { 10, 10, 20, 20, 20, 20, 20, 20, 10, 10 },
				{ 10, 10, 20, 20, 20, 20, 20, 20, 10, 10 }, { 10, 10, 20, 20, 20, 20, 20, 20, 10, 10 },
				{ 20, 20, 30, 30, 40, 40, 30, 30, 20, 20 }, { 20, 20, 30, 30, 40, 40, 30, 30, 20, 20 },
				{ 20, 30, 30, 40, 50, 50, 40, 30, 30, 20 }, { 30, 40, 50, 50, 50, 50, 50, 50, 40, 30 } };

		Billetsalg bs = new Billetsalg(pladser);

		bs.print();

		System.out.println();
		bs.købBillet(20);
		bs.print();

		System.out.println();
		bs.købBillet(4, 9);
		bs.print();
	}
}

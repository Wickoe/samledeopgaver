package opgaver.TeatersalPladser;

public class Billetsalg {
	private int[][] pladser;

	public Billetsalg(int[][] pladser) {
		this.pladser = pladser;
	}

	public int[][] getPladser() {
		int[][] tempPladser = new int[pladser.length][pladser[0].length];

		for (int i = 0; i < pladser.length; i++) {
			for (int j = 0; j < pladser[i].length; j++) {
				tempPladser[i][j] = pladser[i][j];
			}
		}

		return tempPladser;
	}

	public void købBillet(int pris) {
		boolean found = false;
		int collum = 0;
		int row = 0;

		while (!found && row < pladser.length && collum < pladser[row].length) {
			if (pladser[row][collum] == pris) {
				found = true;
				pladser[row][collum] = 0;
			} else {
				collum++;

				if (collum >= pladser[row].length) {
					collum = 0;
					row++;
				}
			}
		}

		if (found) {
			pladser[row][collum] = 0;
		}
	}

	public void købBillet(int række, int kollonne) {
		if (pladser[kollonne][række] != 0) {
			pladser[kollonne][række] = 0;
		}
	}

	public void print() {
		for (int i = 0; i < pladser.length; i++) {
			for (int j = 0; j < pladser[i].length; j++) {
				System.out.print(pladser[i][j] + " ");
			}
			System.out.println();
		}
	}
}

package opgaver.BarChart.Opgave5;

import java.util.ArrayList;
import java.util.Scanner;

public class BarChart {
	private ArrayList<Integer> list = new ArrayList<>();
	private ArrayList<String> listOverskrifter = new ArrayList<>();

	public void readValues() {
		System.out.println("Indtast nogle par af overskrifter og tal.");

		Scanner inOverSkrift = new Scanner(System.in);
		Scanner inTal = new Scanner(System.in);

		try {
			System.out.println("Indtast en overskrift.");
			String overskrift = inOverSkrift.next();
			System.out.println("Indtast et tal. " + "Angiv et negativt tal for at afslutte.");
			int n = inTal.nextInt();

			while (n >= 0) {
				listOverskrifter.add(overskrift);
				list.add(n);
				System.out.println("Indtast en overskrift.");
				overskrift = inOverSkrift.next();
				System.out.println("Indtast et tal. " + "Angiv et negativt tal for at afslutte.");
				n = inTal.nextInt();
			}
		} catch (Exception e) {

		}
		inTal.close();
		inOverSkrift.close();
	}

	/**
	 * Finds and returns the max value in the list.
	 *
	 * @param list
	 *            the list with values.
	 * @return the max value found.
	 */
	public int findMax(ArrayList<Integer> list) {
		int max = list.get(0);

		for (int i : list) {
			if (i >= max) {
				max = i;
			}
		}

		return max;

	}

	/**
	 * Prints out a BarChart of the values using the System.out.println method.
	 * The max value will be printed with a width of 40 stars (*).
	 */
	public void printBarChart() {
		int max = findMax(this.list);

		for (int i = 0; i < list.size(); i++) {
			System.out.print(listOverskrifter.get(i) + " \t");
			int temp = list.get(i);
			for (int j = 0; j < (temp / (double) max) * 40; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

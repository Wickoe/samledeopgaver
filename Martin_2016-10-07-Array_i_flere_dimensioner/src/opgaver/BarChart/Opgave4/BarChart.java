package opgaver.BarChart.Opgave4;

import java.util.ArrayList;
import java.util.Scanner;

public class BarChart {
	private ArrayList<Integer> list = new ArrayList<Integer>();

	public ArrayList<Integer> readValues() {
		System.out.println("Indtast nogle positive tal.  " + "Indtast et negativt tal for at afslutte: ");

		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		while (n >= 0) {
			list.add(n);
			n = in.nextInt();
		}
		in.close();

		return list;
	}

	/**
	 * Finds and returns the max value in the list.
	 *
	 * @param list
	 *            the list with values.
	 * @return the max value found.
	 */
	public int findMax(ArrayList<Integer> list) {
		int max = list.get(0);

		for (int i : list) {
			if (i >= max) {
				max = i;
			}
		}

		return max;

	}

	/**
	 * Prints out a BarChart of the values using the System.out.println method.
	 * The max value will be printed with a width of 40 stars (*).
	 */
	public void printBarChart() {
		int max = findMax(this.list);

		for (int i = 0; i < list.size(); i++) {
			int temp = list.get(i);
			for (int j = 0; j < (temp / (double) max) * 40; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

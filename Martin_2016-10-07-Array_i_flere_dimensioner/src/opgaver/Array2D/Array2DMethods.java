package opgaver.Array2D;

public class Array2DMethods {
	public void print2DArray(int[][] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j] + " | ");
			}
			System.out.println();
		}
	}

	public void replaceAllValueWithFive(int[][] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = 5;
			}
		}
	}

	public void replaceAllWithOnesAndZeros(int[][] array) {
		int counter = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = counter % 2;
				counter++;
			}
			counter++;
			counter = counter % 2;
		}
	}

	public int sumOf(int[][] array) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				sum += array[i][j];
			}
		}

		return sum;
	}
}

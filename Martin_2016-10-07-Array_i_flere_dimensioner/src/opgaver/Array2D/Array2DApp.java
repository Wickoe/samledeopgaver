package opgaver.Array2D;

public class Array2DApp {
	public static void main(String[] args) {
		int[][] table = new int[8][8];
		Array2DMethods methods = new Array2DMethods();

		methods.print2DArray(table);
		System.out.println("Sum of table: " + methods.sumOf(table));

		methods.replaceAllValueWithFive(table);

		System.out.println();
		methods.print2DArray(table);
		System.out.println("Sum of table: " + methods.sumOf(table));

		methods.replaceAllWithOnesAndZeros(table);

		System.out.println();
		methods.print2DArray(table);
		System.out.println("Sum of table: " + methods.sumOf(table));
	}
}
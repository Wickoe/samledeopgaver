package martin.Absence.Aflevering;

public class TestAbsenceSystem {

	public static void main(String[] args) {
		int[][] absence16t = { { 2, 0, 0, 0, 3, 1, 0, 2, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 2, 0, 0, 0, 3, 1, 0, 2, 0, 0, 0, 0 }, { 1, 2, 1, 2, 1, 2, 0, 2, 0, 0, 4, 0 },
				{ 5, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0 } };

		AbsenceSystem absenceSystem = new AbsenceSystem();
		absenceSystem.printAbsence(absence16t);

		System.out.println();
		System.out.println("Total absence count for student with student nr. 4");
		System.out.println(absenceSystem.totalAbsence(absence16t, 4));
		System.out.println("Average absence count for student with student nr. 4");
		System.out.println(absenceSystem.averageMonth(absence16t, 4));
		System.out.println(
				"Student with zero absence is student " + absenceSystem.studentWithoutAbsenceCount(absence16t));
		System.out.println("Student with the most absence is student " + absenceSystem.mostAbsentStudent(absence16t));
		System.out.println("Student with most absence gets a reset");
		absenceSystem.reset(absence16t, 5);
		absenceSystem.printAbsence(absence16t);
	}
}

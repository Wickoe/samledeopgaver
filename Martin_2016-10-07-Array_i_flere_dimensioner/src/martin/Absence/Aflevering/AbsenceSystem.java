package martin.Absence.Aflevering;

public class AbsenceSystem {
	/**
	 * Print the absence table on the screen
	 */
	public void printAbsence(int[][] absence) {
		for (int i = 0; i < absence.length; i++) {
			for (int j = 0; j < absence[i].length; j++) {
				System.out.print(absence[i][j] + " | ");
			}
			System.out.println();
		}
	}

	/**
	 * Returns the total number of absent days for the given student during the
	 * last 12 months.
	 */
	public int totalAbsence(int[][] absence, int studentNumber) {
		int absenceCount = 0;

		for (int i = 0; i < absence[studentNumber - 1].length; i++) {
			absenceCount += absence[studentNumber][i];
		}
		return absenceCount;
	}

	/**
	 * Returns the average monthly number of absent days for the given student.
	 */
	public double averageMonth(int[][] absence, int studentNumber) {
		return totalAbsence(absence, studentNumber) / (double) absence[studentNumber].length;
	}

	/**
	 * Returns the number of students without any absence during the last 12
	 * months.
	 *
	 */
	public int studentWithoutAbsenceCount(int[][] absence) {
		int student = -1;
		int absenceCount = Integer.MAX_VALUE;

		for (int i = 0; i < absence.length; i++) {
			int tempAbsenceCount = 0;
			for (int j = 0; j < absence[i].length; j++) {
				tempAbsenceCount += absence[i][j];
			}

			if (tempAbsenceCount <= absenceCount) {
				student = i + 1;
				absenceCount = tempAbsenceCount;
			}
		}

		return student;
	}

	/**
	 * Returns the student with the most absence during the last 12 months. If
	 * more than one student has the most absence, return any one of them.
	 */
	public int mostAbsentStudent(int[][] absence) {
		int student = -1;
		int absenceCount = Integer.MIN_VALUE;

		for (int i = 0; i < absence.length; i++) {
			int tempAbsenceCount = 0;
			for (int j = 0; j < absence[i].length; j++) {
				tempAbsenceCount += absence[i][j];
			}

			if (tempAbsenceCount >= absenceCount) {
				student = i + 1;
				absenceCount = tempAbsenceCount;
			}
		}

		return student;
	}

	/**
	 * Resets the absence to 0 for the given student during the last 12 months.
	 */
	public void reset(int[][] absence, int studentNumber) {
		for (int i = 0; i < absence[studentNumber - 1].length; i++) {
			absence[studentNumber - 1][i] = 0;
		}
	}
}

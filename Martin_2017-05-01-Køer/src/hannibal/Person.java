package hannibal;

public class Person {
	private String navn;
	private String sound;

	public Person(String navn, String sound) {
		this.navn = navn;
		this.sound = sound;
	}

	@Override
	public String toString() {
		return navn + "\"" + sound + "\"";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Person)) {
			return false;
		}

		boolean equals = false;
		@SuppressWarnings("unused")
		Person p = (Person) o;

		return equals;
	}
}

package opgaver;

public class ArrayQueue implements Queue {
	private Object[] queue;
	private final int head;
	private int tailSize;
	
	public ArrayQueue() {
		queue = new Object[10];
		head = 0;
		tailSize = 0;
	}
	
	@Override
	public void enqueue(Object element) {
		queue[tailSize] = element;
		tailSize++;
		if (tailSize == queue.length) {
			Object[] newQueue = new Object[queue.length * 2];
			for (int i = 0; i < queue.length; i++) {
				newQueue[i] = queue[i];
			}
			queue = newQueue;
		}
	}
	
	@Override
	public Object dequeue() {
		Object dequeued = queue[head];

		for (int i = 1; i < tailSize; i++) {
			queue[i - 1] = queue[i];
		}

		tailSize--;

		return dequeued;
	}
	
	@Override
	public Object getFront() {
		return queue[head];
	}
	
	@Override
	public boolean isEmpty() {
		return head == tailSize;
	}
	
	@Override
	public int size() {
		return tailSize;
	}
	
}

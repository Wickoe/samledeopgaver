package opgaver;

import java.util.NoSuchElementException;

public class DeckArrayCircle implements Deque {
	private Object[] queue;
	private int size;
	private int head;
	private int tail;
	
	public DeckArrayCircle() {
		queue = new Object[10];
		size = 0;
		head = 0;
		tail = 0;
	}

	@Override
	public void addFirst(Object e) {
		growIfNecessary();
		head = head - 1;

		if (head < 0) {
			head = queue.length - head;
		}
		
		queue[head] = e;
		size++;
	}

	@Override
	public void addLast(Object e) {
		growIfNecessary();
		
		queue[tail] = e;
		tail = (tail + 1) % queue.length;

		size++;
	}

	private void growIfNecessary() {
		if ((tail + 1) % queue.length == head) {
			Object[] newQueue = new Object[queue.length * 2];

			for (int i = 0; i < queue.length; i++) {
				newQueue[i] = queue[(head + i) % queue.length];
			}

			tail = queue.length;
			queue = newQueue;
			head = 0;
		}
	}

	@Override
	public Object removeFirst() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		Object removed = queue[head];
		head = (head + 1) % queue.length;
		size--;
		return removed;
	}

	@Override
	public Object removeLast() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		tail = tail - 1;
		
		if (tail < 0) {
			tail = queue.length - tail;
		}

		size--;

		return queue[tail];
	}

	@Override
	public Object getFirst() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		return queue[head];
	}

	@Override
	public Object getLast() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		return queue[tail - 1];
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

}

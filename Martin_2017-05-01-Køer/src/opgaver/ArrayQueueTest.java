package opgaver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

public class ArrayQueueTest {
	private Queue queue;
	
	@Before
	public void setUp() throws Exception {
		queue = new ArrayQueue();
	}
	
	@Test
	public void testEnqueue() {
		assertNull(queue.getFront());
		assertEquals(0, queue.size());

		Object object = new Object();

		queue.enqueue(object);

		assertEquals(object, queue.getFront());
		assertEquals(1, queue.size());
	}
	
}

package opgaver;

import java.util.NoSuchElementException;

public class DeckLinked implements Deque {
	private DoubleNode first, last;
	private int size;

	private class DoubleNode {
		private DoubleNode previous, next;
		private Object data;
	}

	public DeckLinked() {
		size = 0;
	}

	@Override
	public void addFirst(Object e) {
		DoubleNode newNode = new DoubleNode();
		newNode.data = e;
		
		if (isEmpty()) {
			last = newNode;
		}
		else {
			newNode.next = first;
			first.previous = newNode;
		}
		
		first = newNode;
		size++;
	}

	@Override
	public void addLast(Object e) {
		DoubleNode newNode = new DoubleNode();
		newNode.data = e;
		
		if (isEmpty()) {
			first = newNode;
		}
		else {
			last.next = newNode;
			newNode.previous = last;
		}
		
		last = newNode;
		size++;
	}

	@Override
	public Object removeFirst() {
		if (size == 0) {
			throw new NoSuchElementException();
		}
		
		DoubleNode removed = first;
		first = first.next;
		first.previous = null;
		size--;
		return removed.data;
	}

	@Override
	public Object removeLast() {
		if (size == 0) {
			throw new NoSuchElementException();
		}
		
		DoubleNode removed = last;
		last = last.previous;
		last.next = null;
		size--;
		return removed.data;
	}

	@Override
	public Object getFirst() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		
		return first.data;
	}

	@Override
	public Object getLast() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		
		return last.data;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}
}

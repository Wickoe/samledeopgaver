package opgaver;

import java.util.NoSuchElementException;

public class LinkedQueue implements Queue {
	private Node head, tail;
	private int size;

	private class Node {
		private Node next;
		private Object data;
	}
	
	public LinkedQueue() {
		head = null;
		tail = null;
		size = 0;
	}
	
	@Override
	public void enqueue(Object element) {
		Node newNode = new Node();
		newNode.data = element;
		
		if (isEmpty()) {
			head = newNode;
		}
		else {
			tail.next = newNode;
		}

		tail = newNode;
		size++;
	}
	
	@Override
	public Object dequeue() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}

		Object dequeued = head.data;
		head = head.next;
		size--;
		return dequeued;
	}
	
	@Override
	public Object getFront() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}

		return head.data;
	}
	
	@Override
	public boolean isEmpty() {
		return size == 0;
	}
	
	@Override
	public int size() {
		return size;
	}
}

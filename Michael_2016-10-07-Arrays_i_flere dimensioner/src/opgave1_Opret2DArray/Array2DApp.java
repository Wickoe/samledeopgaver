package opgave1_Opret2DArray;

public class Array2DApp {

    public static void main(String[] args) {
        int[][] liste = new int[8][8];
        Array2DMethods ny = new Array2DMethods();
        
        ny.print(liste);
        System.out.println();

        ny.setArray(liste);
        ny.print(liste);
        System.out.println("\n" + ny.summer(liste));

        ny.change(liste);
        ny.print(liste);
        System.out.println();
        
        System.out.println(ny.summer(liste));
    }

}

package opgave1_Opret2DArray;

public class Array2DMethods {
    public void print(int[][] array) {
        final int x = array.length;
        final int y = array[0].length;
        
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    public void setArray(int[][] array) {
        final int x = array.length;
        final int y = array[0].length;

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                array[i][j] = 5;
            }
        }
    }
    
    public void change(int[][] array) {
        final int x = array.length;
        final int y = array[0].length;

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (i % 2 == 0) {
                    if (j % 2 == 0) {
                        array[i][j] = 0;
                    }
                    else {
                        array[i][j] = 1;
                    }
                }
                else {
                    if (j % 2 == 1) {
                        array[i][j] = 0;
                    }
                    else {
                        array[i][j] = 1;
                    }
                }
            }
        }
    }

    public int summer(int[][] array) {
        int sum = 0;
        final int x = array.length;
        final int y = array[0].length;

        for (int i = 0; i < x; i++) {
            //sum += array[i][0];
            for (int j = 0; j < y; j++) {
                sum += array[i][j];
            }
        }
        return sum;
    }
}

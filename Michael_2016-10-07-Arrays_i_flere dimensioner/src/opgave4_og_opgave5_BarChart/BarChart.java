package opgave4_og_opgave5_BarChart;

import java.util.ArrayList;
import java.util.Scanner;

public class BarChart {
    private ArrayList<Integer> list = new ArrayList<>();
    private ArrayList<String> overskrift = new ArrayList<>();
    
    public void readValues() {
        System.out.println(
            "Skriv først en overskrift til tallet, derefter indtast et positive tal." +
                " Bliv ved end du du ikke ønsker flere tal. "
                + "\nIndtast et negativt tal for at afslutte: ");
        Scanner inTal = new Scanner(System.in);
        Scanner inString = new Scanner(System.in);
        String s = inString.next();
        int n = inTal.nextInt();
        while (n >= 0) {
            overskrift.add(s);
            list.add(n);
            s = inString.next();
            n = inTal.nextInt();
        }
        inTal.close();
        inString.close();
    }
    
    /**
     * Finds and returns the max value in the list.
     * @param list the list with values.
     * @return the max value found.
     */
    public int findMax(ArrayList<Integer> list) {
        int max = list.get(0);
        for (int tal : list) {
            if (tal > max) {
                max = tal;
            }
        }
        return max;
    }
    
    /**
     * Prints out a BarChart of the values using the System.out.println method.
     * The max value will be printed with a width of 40 stars (*).
     */
    public void printBarChart() {
        int max = findMax(this.list);
        
        for (int i = 0; i < list.size(); i++) {
            System.out.print(overskrift.get(i) + " \t");
            int temp = list.get(i);
            for (int j = 0; j < (temp / (double) max) * 40; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
        
    }
}

package opgave3_Teatersal;

import java.util.Scanner;

public class TeatersalApp {
    
    public static void main(String[] args) {
        int[][] plads =
            {
                { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 },
                { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 },
                { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 },
                { 10, 10, 20, 20, 20, 20, 20, 20, 10, 10 },
                { 10, 10, 20, 20, 20, 20, 20, 20, 10, 10 },
                { 10, 10, 20, 20, 20, 20, 20, 20, 10, 10 },
                { 10, 10, 20, 20, 20, 20, 20, 20, 10, 10 },
                { 20, 20, 30, 30, 40, 40, 30, 30, 20, 20 },
                { 20, 30, 30, 40, 50, 50, 40, 30, 30, 20 },
                { 30, 40, 50, 50, 50, 50, 50, 50, 40, 30 }
            };
        
        Teatersal t1 = new Teatersal(plads);
        t1.print();
        System.out.println();
        
        System.out.println("Vælg med tal om du vil ");
        System.out.println("1: Vælg en pris og få plads");
        System.out.println("2: Vælg en plads");
        Scanner scan = new Scanner(System.in);
        int tal = scan.nextInt();
        if (tal == 1) {
            System.out.println("Skriv en pris du ønsker");
            tal = scan.nextInt();
            t1.købBilletPris(tal);
            t1.print();
        }
        else {
            System.out.println("Skriv en plads du ønsker EX: (x, y)");
            tal = scan.nextInt();
            int x = tal;
            tal = scan.nextInt();
            int y = tal;
            t1.købBilletPlads(x, y);
            t1.print();
        }
        scan.close();
    }
}

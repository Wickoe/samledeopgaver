package opgave3_Teatersal;

public class Teatersal {
    int[][] plads;

    public Teatersal(int[][] plads) {
        this.plads = plads;
    }
    
    public void købBilletPris(int pris) {
        boolean found = false;
        int x = 0;
        int y = 0;
        
        while (!found) {
            if (plads[y][x] == pris) {
                found = true;
            }
            else {
                x++;
                if (x == plads.length - 1) {
                    x = 0;
                    y++;
                }
            }
        }
        if (found) {
            plads[y][x] = 0;
        }
    }

    public void købBilletPlads(int x, int y) {
        if (plads[y][x] != 0) {
            plads[y][x] = 0;
        }
        else {
            System.out.println("Plads ikke fri. Vælg en anden");
        }
    }

    public void print() {
        for (int y = 0; y < plads.length; y++) {
            for (int x = 0; x < plads[0].length; x++) {
                System.out.print(plads[y][x] + " ");
            }
            System.out.println();
        }
    }
}

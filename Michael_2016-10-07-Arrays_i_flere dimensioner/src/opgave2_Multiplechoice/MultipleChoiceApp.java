package opgave2_Multiplechoice;

public class MultipleChoiceApp {

    public static void main(String[] args) {
        MultipleChoice mc = new MultipleChoice(4);
        mc.printStudentAnswers();
        System.out.println();

        mc.printCorrectAnswersPrStudent();
        System.out.println();

        mc.printCorrectAnswerPrQuestion();
    }

}

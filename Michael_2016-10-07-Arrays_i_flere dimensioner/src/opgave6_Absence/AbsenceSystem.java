package opgave6_Absence;

public class AbsenceSystem {
    /**
     * Print the absence table on the screen
     */
    public void printAbsence(int[][] absence) {
        for (int row = 0; row < absence.length; row++) {
            System.out.print("Studerende " + (row + 1) + ": ");
            for (int column = 0; column < absence[0].length; column++) {
                System.out.print(absence[row][column] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Returns the total number of absent days for the given student during the last 12 months.
     */
    public int totalAbsence(int[][] absence, int studentNumber) {
        int frævær = 0;
        studentNumber--;
        for (int column = 0; column < absence[studentNumber].length; column++) {
            frævær += absence[studentNumber][column];
        }
        return frævær;

    }

    /**
     * Returns the average monthly number of absent days for the given student.
     */
    public double averageMonth(int[][] absence, int studentNumber) {
        return totalAbsence(absence, studentNumber) / (double) absence[studentNumber - 1].length;
    }

    /**
     * Returns the number of students without any absence during the last 12 months.
     *
     */
    public int studentWithoutAbsenceCount(int[][] absence) {
        int frævær = 0;
        int antal = 0;
        for (int row = 0; row < absence.length; row++) {
            frævær = 0;
            for (int column = 0; column < absence[0].length; column++) {
                frævær += absence[row][column];
            }
            if (frævær == 0) {
                antal++;
            }
        }
        return antal;
    }

    /**
     * Returns the student with the most absence during the last 12 months.
     * If more than one student has the most absence, return any one of them.
     */
    public int mostAbsentStudent(int[][] absence) {
        int frævær = 0;
        int student = 0;
        int max = 0;
        for (int row = 0; row < absence.length; row++) {
            frævær = 0;
            for (int column = 0; column < absence[0].length; column++) {
                frævær += absence[row][column];
            }
            if (frævær > max) {
                max = frævær;
                student = row + 1;
            }
        }
        return student;
    }

    /**
     * Resets the absence to 0 for the given student during the last 12 months.
     */
    public void reset(int[][] absence, int studentNumber) {
        for (int column = 0; column < absence[studentNumber - 1].length; column++) {
            absence[studentNumber - 1][column] = 0;
        }
    }
}

package opgave6_Absence;

public class TestAbsenceSystem {
    
    public static void main(String[] args) {
        int[][] absence16t =
            { { 2, 0, 0, 0, 3, 1, 0, 2, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 2, 0, 0, 0, 3, 1, 0, 2, 0, 0, 0, 0 },
                { 1, 2, 1, 2, 1, 2, 0, 2, 0, 0, 4, 0 },
                { 5, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0 } };
        
        AbsenceSystem absenceSystem = new AbsenceSystem();
        absenceSystem.printAbsence(absence16t);

        System.out.println(absenceSystem.totalAbsence(absence16t, 5));
        System.out.println(absenceSystem.averageMonth(absence16t, 5));

        System.out.println();
        System.out.println(absenceSystem.studentWithoutAbsenceCount(absence16t));
        
        System.out.println();
        System.out.println(absenceSystem.mostAbsentStudent(absence16t));

        absenceSystem.reset(absence16t, 5);
        absenceSystem.printAbsence(absence16t);

    }
}

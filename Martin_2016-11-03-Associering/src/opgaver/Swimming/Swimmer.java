package opgaver.Swimming;

import java.util.ArrayList;

/**
 * Modeling a Swimmer
 */
public class Swimmer {
	private String name;
	private String club;
	private int yearGroup;
	private ArrayList<Double> lapTimes;
	private TrainingPlan plan;

	/**
	 * Initialize a new swimmer with name, club, yearGroup, and lap times.
	 */
	public Swimmer(String name, int yearGroup, ArrayList<Double> lapTimes, String club) {
		this.name = name;
		this.yearGroup = yearGroup;
		this.lapTimes = lapTimes;
		this.club = club;
	}

	/**
	 * Return the name of the swimmer
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Return the yearGroup of the swimmer
	 */
	public int getYearGroup() {
		return this.yearGroup;
	}

	/**
	 * Return the club of the swimmer
	 */
	public String getClub() {
		return this.club;
	}

	/**
	 * Register the club of the swimmer
	 *
	 * @param club
	 */
	public void setClub(String club) {
		this.club = club;
	}

	/**
	 * Return the fastest lap time
	 */
	public double bestLapTime() {
		double bestTime = Double.MAX_VALUE;

		for (double d : lapTimes) {
			if (d < bestTime) {
				bestTime = d;
			}
		}
		return bestTime;
	}

	/**
	 * Set the training plan for the swimmer.
	 *
	 * @param aPlan
	 *            the plan the swimmer has to follow
	 */
	public void setTraininPlan(TrainingPlan aPlan) {
		TrainingPlan oldPlan = plan;
		this.plan = aPlan;
		if (oldPlan != null) {
			oldPlan.removeSwimmer(this);
		}
		if (plan != null && plan.getSwimmers().contains(this)) {
			plan.addSwimmer(this);
		}
	}

	/**
	 * Returns the training plan that the swimmer follows.
	 *
	 * @return plan the training plan that the swimmer follows
	 */
	public TrainingPlan getTrainingPlan() {
		return plan;
	}

	/**
	 * Returns how many hours the swimmer has each week.
	 *
	 * @return weeklyTrainingHours
	 */
	public int allTrainingHours() {
		return plan.getWeeklyStrengthHours() + plan.getWeeklyWaterHours();
	}
}

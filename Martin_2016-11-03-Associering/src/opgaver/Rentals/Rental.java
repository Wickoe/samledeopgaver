package opgaver.Rentals;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a rental leash.
 *
 * @author Martin
 *
 */
public class Rental {
	private int number;
	private int days;
	private String date;
	private List<Car> cars;

	/**
	 * Initialize a rental with a specific number, number of days, a start date
	 * and list of acutal cars.
	 *
	 * @param number
	 *            the number of the rental leash
	 * @param date
	 *            the start date of the rental
	 * @param days
	 *            the number of days for the rental
	 */
	public Rental(int number, String date, int days) {
		this.number = number;
		this.date = date;
		this.days = days;
		cars = new ArrayList<Car>();
	}

	/**
	 * Returns the price of the rental
	 *
	 * @return totalRentalPrice the total price of the rental
	 */
	public double getPrice() {
		double totalRentalPrice = 0;

		for (Car c : cars) {
			totalRentalPrice += c.getDayPrice() * days;
		}
		return totalRentalPrice;
	}

	/**
	 * Sets the days of the rental leash.
	 *
	 * @param days
	 *            the days the rental are active
	 */
	public void setDays(int days) {
		this.days = days;
	}

	/**
	 * Returns the days the rental are active.
	 *
	 * @return days the days the rental are active
	 */
	public int getDays() {
		return days;
	}

	/**
	 * Add a car the the rental
	 *
	 * @param car
	 *            the car to be added
	 */
	public void addCar(Car car) {
		cars.add(car);
	}

	/**
	 * Remove a car from the rental.
	 *
	 * @param car
	 *            the car to be removed
	 */
	public void removeCar(Car car) {
		cars.remove(car);
	}

	/**
	 * Returns a list of cars associated with the rental.
	 *
	 * @return cars the cars associated with the rental
	 */
	public List<Car> getCars() {
		return new ArrayList<>(cars);
	}

	@Override
	public String toString() {
		return number + " " + days + " " + date;
	}
}

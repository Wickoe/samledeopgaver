package opgaver.Rentals;

/**
 * Models a car with a linces registration, a price per day and the year of
 * purchase.
 *
 * @author Martin
 *
 */
public class Car {
	private String license;
	private double pricePrDay;
	private int purchaseYear;

	/**
	 * Initialize a car with a given license and year brought
	 *
	 * @param license
	 *            the license of the car
	 * @param year
	 *            the year the car was brought
	 */
	public Car(String license, int year) {
		this.license = license;
		purchaseYear = year;
	}

	/**
	 * Sets the price for this car per day.
	 *
	 * @param price
	 *            the price of the car per day
	 */
	public void setDayPrice(double price) {
		pricePrDay = price;
	}

	/**
	 * Returns the daily price of the car.
	 *
	 * @return pricePrDay the daily price of renting the car
	 */
	public double getDayPrice() {
		return pricePrDay;
	}

	/**
	 * Returns the license of the car.
	 *
	 * @return license the license of the car
	 */
	public String getLicense() {
		return license;
	}

	/**
	 * Returns the year the car was brought.
	 *
	 * @return purchaseYear the year the car was brought
	 */
	public int getPurchaseYear() {
		return purchaseYear;
	}
}

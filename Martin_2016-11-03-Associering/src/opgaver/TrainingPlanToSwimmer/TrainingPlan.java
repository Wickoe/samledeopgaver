package opgaver.TrainingPlanToSwimmer;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a training plan for a Swimmer
 */
public class TrainingPlan {
	private char level;
	private int weeklyWaterHours;
	private int weeklyStrengthHours;
	private List<Swimmer> swimmers;

	/**
	 * Initialize a TrainingPlan with a level, an weekly water- and strength-
	 * hour workout
	 *
	 * @param level
	 *            the level of the training plan
	 * @param weeklyWaterHours
	 *            the weekly water hours
	 * @param weeklyStrengthHours
	 *            the weekly strength hours
	 */
	public TrainingPlan(char level, int weeklyWaterHours, int weeklyStrengthHours) {
		this.level = level;
		this.weeklyWaterHours = weeklyWaterHours;
		this.weeklyStrengthHours = weeklyStrengthHours;
		swimmers = new ArrayList<>();
	}

	/**
	 * Returns the level of the training plan.
	 *
	 * @return level the level of the training plan
	 */
	public char getLevel() {
		return level;
	}

	/**
	 * Set the level of the training plan.
	 *
	 * @param niveau
	 *            the level of the training plan
	 */
	public void setLevel(char niveau) {
		this.level = niveau;
	}

	/**
	 * Returns the weekly strength hours work-out.
	 *
	 * @return weeklyStrengthHours the weekly strength hours
	 */
	public int getWeeklyStrengthHours() {
		return weeklyStrengthHours;
	}

	/**
	 * Set the weekly strength hours.
	 *
	 * @Pre: weeklyStrengthHours cannot be negative
	 * @param weeklyStrengthHours
	 *            the weekly strength hours
	 */
	public void setWeeklyStrengthHours(int weeklyStrengthHours) {
		this.weeklyStrengthHours = weeklyStrengthHours;
	}

	/**
	 * Returns the weekly water hours.
	 *
	 * @return weeklyWaterHours the weekly water hours.
	 */
	public int getWeeklyWaterHours() {
		return weeklyWaterHours;
	}

	/**
	 * Set the weekly water hours.
	 *
	 * @Pre: weeklyWaterHours cannot be negative
	 * @param weeklyWaterHours
	 *            the weekly water hours
	 */
	public void setWeeklyWaterHours(int weeklyWaterHours) {
		this.weeklyWaterHours = weeklyWaterHours;
	}

	/**
	 * Adds a swimmer to the training plan.
	 *
	 * @param swimmer
	 *            the swimmer to be added to the training plan
	 */
	public void addSwimmer(Swimmer swimmer) {
		swimmers.add(swimmer);
	}

	/**
	 * Removes a swimmer from the training plan.
	 *
	 * @param swimmer
	 *            the swimmer to be removed from the training plan
	 */
	public void removeSwimmer(Swimmer swimmer) {
		swimmers.remove(swimmer);
	}

	/**
	 * Returns the list of swimmers associated with the training plan.
	 *
	 * @return swimmers all the swimmers that are associated with the training
	 *         plan
	 */
	public List<Swimmer> getSwimmers() {
		return new ArrayList<>(swimmers);
	}

	@Override
	public String toString() {
		return level + "";
	}
}

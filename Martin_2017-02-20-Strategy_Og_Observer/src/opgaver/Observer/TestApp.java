package opgaver.Observer;

public class TestApp {
	public static void main(String[] args) {
		BogTitel denSejeLeoBog = new BogTitel("Den seje Leo", 7);
		BogTitel dinosaurenNicolai = new BogTitel("Dinosauren Nicolai", 2);
		BogTitel michaelSpinners = new BogTitel("Michael spinners", 6);

		Kunde michael = new Kunde("Michael");
		Kunde nicolai = new Kunde("Nicolai");

		Observer leo = new Indkoeber("Leo");
		Observer martin = new Saelger("Martin");

		denSejeLeoBog.addObserver(leo);
		denSejeLeoBog.addObserver(martin);

		dinosaurenNicolai.addObserver(leo);
		dinosaurenNicolai.addObserver(martin);

		michaelSpinners.addObserver(leo);
		michaelSpinners.addObserver(martin);

		denSejeLeoBog.etKoeb(michael);
		System.out.println();
		dinosaurenNicolai.etKoeb(nicolai);
		System.out.println();
		denSejeLeoBog.etKoeb(nicolai);
	}
}

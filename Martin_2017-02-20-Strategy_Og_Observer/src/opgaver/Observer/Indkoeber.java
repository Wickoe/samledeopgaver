package opgaver.Observer;

public class Indkoeber implements Observer {
	private String navn;

	public Indkoeber(String navn) {
		this.navn = navn;
	}

	@Override
	public void update(Subject s) {
		BogTitel bt = (BogTitel) s;

		if (bt.getAntal() < 6) {
			bt.indkoebTilLager(10);
			System.out.println("Indkøbt 10 af " + bt.getTitel());
		}
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}
}

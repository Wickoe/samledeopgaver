package opgaver.Observer;

import java.util.ArrayList;
import java.util.List;

public class Kunde {
	private String navn;
	private List<BogTitel> bøger;

	public Kunde(String navn) {
		this.navn = navn;

		bøger = new ArrayList<>();
	}

	public void addBogTitel(BogTitel bt) {
		bøger.add(bt);
	}

	@Override
	public String toString() {
		return navn;
	}

	public List<BogTitel> getBoeger() {
		return new ArrayList<>(bøger);
	}
}

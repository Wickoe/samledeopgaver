package opgaver.Observer;

import java.util.ArrayList;
import java.util.List;

public class BogTitel implements Subject {
	private String titel;
	private int antal;
	private List<Observer> observers;
	private List<Kunde> kunder;

	public BogTitel(String titel, int antal) {
		this.titel = titel;
		this.antal = antal;
		observers = new ArrayList<>();
		kunder = new ArrayList<>();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public int getAntal() {
		return antal;
	}

	public void setAntal(int antal) {
		this.antal = antal;
	}

	private void fire() {
		for (Observer o : observers) {
			o.update(this);
		}
	}

	public void indkoebTilLager(int antal) {
		this.antal += antal;
	}

	public void etKoeb(Kunde k) {
		k.addBogTitel(this);

		if (!kunder.contains(k)) {
			kunder.add(k);
		}
		antal--;

		fire();
	}

	public List<Kunde> getKunder() {
		return new ArrayList<>(kunder);
	}

	@Override
	public String toString() {
		return titel;
	}
}

package opgaver.Observer;

public interface Observer {
	public void update(Subject s);
}

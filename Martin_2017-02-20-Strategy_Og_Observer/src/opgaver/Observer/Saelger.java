package opgaver.Observer;

import java.util.HashSet;
import java.util.Set;

public class Saelger implements Observer {
	private String navn;

	public Saelger(String navn) {
		this.navn = navn;
	}

	@Override
	public void update(Subject s) {
		BogTitel bt = (BogTitel) s;

		Set<BogTitel> bter = new HashSet<>();
		for (Kunde k : bt.getKunder()) {
			bter.addAll(k.getBoeger());
		}

		bter.remove(bt);

		System.out.println("Bøger som andre kunder også har købt");
		System.out.println(bter);
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}
}

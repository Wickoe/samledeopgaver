package opgaver.Strategy;

import java.util.Comparator;

public class ComparatorKundeAdresse implements Comparator<Kunde> {
	@Override
	public int compare(Kunde o1, Kunde o2) {
		return o1.getAdresse().compareTo(o2.getAdresse());
	}

}

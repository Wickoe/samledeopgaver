package opgaver.Strategy;

import java.util.Comparator;

/**
 * Modelerer en seperat algoritme for at sortere ved hjælp af Comparator<Kunde>
 * 
 * @author Martin
 *
 */
public class ComparatorKundeNummer implements Comparator<Kunde> {
	@Override
	public int compare(Kunde o1, Kunde o2) {
		return o1.getNummer() - o2.getNummer();
	}
}

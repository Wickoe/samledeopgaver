package opgaver.Strategy;

/**
 * Modellerer en kunde som er Comparable<Kunde>, der fungerer på klassen
 *
 * @author Martin
 *
 */
public class Kunde implements Comparable<Kunde> {
	private String navn;
	private int nummer;
	private String adresse;

	public Kunde(String etNavn, int etNummer, String enAdresse) {
		navn = etNavn;
		nummer = etNummer;
		adresse = enAdresse;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String etNavn) {
		navn = etNavn;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int etNummer) {
		nummer = etNummer;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String enAdresse) {
		adresse = enAdresse;
	}

	@Override
	public String toString() {
		return navn + " " + nummer + " " + adresse;
	}

	@Override
	public int compareTo(Kunde o) {
		return navn.compareTo(o.getNavn());
	}
}

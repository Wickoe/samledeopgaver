package opgaver.Strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainTestApp {
	public static void main(String[] args) {
		Kunde leo = new Kunde("Leo", 2, "Sønderhøj 30 x.X.x.");
		Kunde michael = new Kunde("Michael", 1, "Randersvej");

		ComparatorKundeNummer comparatorKunde = new ComparatorKundeNummer();

		List<Kunde> kunder = new ArrayList<>();

		kunder.add(michael);
		kunder.add(leo);

		System.out.println(kunder.toString());
		System.out.println();

		Collections.sort(kunder);

		System.out.println(kunder.toString());
		System.out.println();

		Collections.sort(kunder, comparatorKunde);

		System.out.println(kunder.toString());

		Collections.sort(kunder, new ComparatorKundeAdresse());

		System.out.println(kunder.toString());
	}
}

package opgave1_QueueWithArray;

public class MainApp {

    public static void main(String[] args) {
        ArrayQueue queue = new ArrayQueue();
        
        queue.enqueue("Lars");
        queue.enqueue("Lol");
        queue.enqueue("Din mor");
        queue.enqueue("Haha");
        
        System.out.println(queue);
        System.out.println(queue.getFront());
        System.out.println(queue.isEmpty());
        System.out.println(queue.size());
        
        queue.dequeue();
        
        System.out.println(queue);
        
    }

}

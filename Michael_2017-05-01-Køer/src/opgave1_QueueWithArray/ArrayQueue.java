package opgave1_QueueWithArray;

import java.util.Arrays;

public class ArrayQueue implements Queue {
    private Object[] queue;
    private final int INIT_SIZE = 10;
    private int currentSize;
    
    public ArrayQueue() {
        this.queue = new Object[INIT_SIZE];
    }
    
    @Override
    public void enqueue(Object element) {
        growIfNecessary();
        queue[currentSize] = element;
        currentSize++;
    }
    
    @Override
    public Object dequeue() {
        Object removed = queue[0];
        currentSize--;
        for (int i = 0; i < currentSize; i++) {
            queue[i] = queue[i + 1];
        }
        return removed;
    }
    
    @Override
    public Object getFront() {
        return queue[0];
    }
    
    @Override
    public boolean isEmpty() {
        return currentSize == 0;
    }
    
    @Override
    public int size() {
        return currentSize;
    }
    
    private void growIfNecessary() {
        if (currentSize == queue.length) {
            Object[] newStack = new Object[2 * queue.length];
            for (int i = 0; i < queue.length; i++) {
                newStack[i] = queue[i];
            }
            queue = newStack;
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(queue);
    }
    
}

package opgave1_QueueWithNode;

public class LinkedQueue<E> implements Queue {
    private class Node<E> {
        E data;
        Node next;
        
        public Node(E data) {
            this.data = data;
        }
    }

    private Node head;
    private Node tail;
    private int currentSize = 0;
    
    @Override
    public void enqueue(Object element) {
        Node node = new Node(element);
        
        if (head == null) {
            head = node;
        }
        else {
            tail.next = node;
        }
        tail = node;
        currentSize++;
    }
    
    @Override
    public Object dequeue() {
        Object removed = head.data;
        head = head.next;
        currentSize--;
        return removed;
    }
    
    @Override
    public Object getFront() {
        Object front = head.data;
        return front;
    }
    
    @Override
    public boolean isEmpty() {
        return head == null;
    }
    
    @Override
    public int size() {
        return currentSize;
    }
    
    @Override
    public String toString() {
        Node node = head;
        String result = "";
        while (node != null) {
            result += node.data + " ";
            node = node.next;
        }
        return result.trim();
    }

}
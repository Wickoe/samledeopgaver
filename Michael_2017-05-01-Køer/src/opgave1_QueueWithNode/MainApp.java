package opgave1_QueueWithNode;

public class MainApp {
    
    public static void main(String[] args) {
        LinkedQueue<String> queue = new LinkedQueue();
        
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("4");
        System.out.println("Alt: " + queue);

        queue.dequeue();
        System.out.println("Første værdi fjernet: " + queue);

        System.out.println("Første i kø: " + queue.getFront());

        System.out.println("Hvor meget er i kø: " + queue.size());

        System.out.println(queue.isEmpty());
        
    }
    
}

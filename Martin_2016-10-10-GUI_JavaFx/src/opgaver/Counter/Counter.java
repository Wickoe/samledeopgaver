package opgaver.Counter;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Counter extends Application {
	private TextField txfCounter;
	private int counter;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Counter");
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		txfCounter = new TextField(counter + "");
		pane.add(txfCounter, 0, 0, 2, 1);

		Button btnUp = new Button("Up");
		btnUp.setOnAction(event -> countUpAction());
		pane.add(btnUp, 0, 1);

		Button btnDown = new Button("Down");
		btnDown.setOnAction(event -> countDownAction());
		pane.add(btnDown, 1, 1);
	}

	private void countDownAction() {
		counter--;
		txfCounter.setText(counter + "");
	}

	private void countUpAction() {
		counter++;
		txfCounter.setText(counter + "");
	}
}

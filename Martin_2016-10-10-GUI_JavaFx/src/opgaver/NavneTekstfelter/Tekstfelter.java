package opgaver.NavneTekstfelter;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Tekstfelter extends Application {
	private TextField txfFirstName;
	private TextField txfLastName;
	private TextField txfName;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Combine names");
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	public void initContent(GridPane pane) {
		pane.setVgap(10);
		pane.setHgap(10);
		pane.setPadding(new Insets(20));

		txfFirstName = new TextField();
		pane.add(txfFirstName, 0, 0);
		txfLastName = new TextField();
		pane.add(txfLastName, 1, 0);
		txfName = new TextField();
		pane.add(txfName, 0, 1, 2, 1);

		Button btnCombine = new Button("Combine");
		btnCombine.setOnAction(event -> combineNamesAction());
		pane.add(btnCombine, 0, 2);
	}

	private void combineNamesAction() {
		txfName.setText(txfFirstName.getText() + " " + txfLastName.getText());
		txfFirstName.setText("");
		txfLastName.setText("");
	}
}

package opgaver.CelciusFahrenheit;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class CelciusFahrenheit extends Application {
	private TextField txfTemperatur;
	private Button btnTemperature;
	private boolean isCelcius;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Tempratur");
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		isCelcius = true;

		txfTemperatur = new TextField();
		pane.add(txfTemperatur, 0, 0);

		btnTemperature = new Button("To fahrenheit");
		btnTemperature.setOnAction(event -> swapMeassurementAction());
		pane.add(btnTemperature, 0, 1);
	}

	private void swapMeassurementAction() {
		if (isCelcius) {
			btnTemperature.setText("To celcious");
			double temperature = Integer.valueOf(txfTemperatur.getText());

			txfTemperatur.setText(((9 / 5) * temperature + 32) + "");
			isCelcius = false;
		} else {
			btnTemperature.setText("To fahrenheit");

			txfTemperatur.setText((Integer.valueOf(txfTemperatur.getText()) - 32) * 5 / 9 + "");

			isCelcius = true;
		}
	}
}

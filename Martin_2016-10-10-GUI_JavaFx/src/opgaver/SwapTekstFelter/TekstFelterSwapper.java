package opgaver.SwapTekstFelter;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TekstFelterSwapper extends Application {
	private TextField txfFirst;
	private TextField txfSecond;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Swap");
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setVgap(10);
		pane.setHgap(10);

		txfFirst = new TextField();
		pane.add(txfFirst, 0, 0);

		txfSecond = new TextField();
		pane.add(txfSecond, 0, 1);

		Button btnSwap = new Button("Swap");
		btnSwap.setOnAction(event -> swapAction());
		pane.add(btnSwap, 0, 2);
	}

	private void swapAction() {
		String temp = txfFirst.getText();
		txfFirst.setText(txfSecond.getText());
		txfSecond.setText(temp);
	}
}

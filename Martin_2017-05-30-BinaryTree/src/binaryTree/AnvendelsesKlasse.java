package binaryTree;

public class AnvendelsesKlasse {
	public static void main(String[] args) {
		BinaryTree<String> binaryTwo = new BinaryTree<>("2");
		BinaryTree<String> binaryFour = new BinaryTree<>("4");
		BinaryTree<String> binaryTwoPlusFour = new BinaryTree<>("+", binaryTwo, binaryFour);

		BinaryTree<String> binarySeven = new BinaryTree<>("7");
		BinaryTree<String> binaryPlusTimesSeven = new BinaryTree<>("*", binaryTwoPlusFour, binarySeven);

		BinaryTree<String> binaryThree = new BinaryTree<>("3");
		BinaryTree<String> binaryEight = new BinaryTree<>("8");
		BinaryTree<String> binaryThreePlusEight = new BinaryTree<>("+", binaryThree, binaryEight);

		BinaryTree<String> binaryTimesPlusPlus = new BinaryTree<>("+", binaryPlusTimesSeven, binaryThreePlusEight);

		binaryTimesPlusPlus.printInOrder();
		System.out.println();
		System.out.println();
		System.out.println(binaryTimesPlusPlus.value());
		System.out.println();
		System.out.println(binaryTimesPlusPlus.countElements("+"));
	}
}

package opgave6_Talfølge;

public class Talfølge {
    
    public static void main(String[] args) {
        System.out.println(talN(3));
        System.out.println(talN(4));
        System.out.println(talN(5));
        System.out.println(talN(6));
        
        System.out.println();

        System.out.println(talFølge2(3));
        System.out.println(talFølge2(4));
        System.out.println(talFølge2(5));
        System.out.println(talFølge2(6));
    }
    
    public static int talN(int n) {
        int result = 0;
        
        if (n == 0) {
            result = 2;
        }
        else if (n == 1) {
            result = 1;
        }
        else if (n == 2) {
            result = 5;
        }
        else if (n > 2 && n % 2 == 0) {
            result = 2 * talN(n - 3) - talN(n - 1);
        }
        else if (n > 2 && n % 2 == 1) {
            result = talN(n - 1) + talN(n - 2) + 3 * talN(n - 3);
        }
        
        return result;
    }

    public static int talFølge2(int n) {
        int[] array = new int[n + 1];
        
        array[0] = 2;
        array[1] = 1;
        array[2] = 5;
        
        for (int i = 3; i <= n; i++) {
            if (i % 2 == 1) {
                array[i] = array[i - 1] + array[i - 2] + 3 * array[i - 3];
            }
            else {
                array[i] = 2 * array[i - 3] - array[i - 1];
            }
        }
        return array[n];
    }
    
}

package opgave1_Palindrom;

public class Palindrom {
    
    public static void main(String[] args) {
        String tekst = "radarl";
        
        System.out.println(palindrom(tekst));
    }
    
    public static boolean palindrom(String tekst) {
        if (tekst.length() == 1) {
            return true;
        }
        else {
            return palindromHelper(tekst, 0, tekst.length() - 1);
        }
    }
    
    public static boolean palindromHelper(String tekst, int startIndex, int slutIndex) {
        boolean result = false;
        if (tekst.charAt(startIndex) == tekst.charAt(slutIndex)) {
            result = true;
        }
        else if (startIndex < slutIndex) {
            result = false;
        }
        else {
            result = palindromHelper(tekst, startIndex + 1, slutIndex - 1);
        }
        return result;
    }
    
}

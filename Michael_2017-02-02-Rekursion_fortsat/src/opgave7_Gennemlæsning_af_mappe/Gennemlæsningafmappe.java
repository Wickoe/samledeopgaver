package opgave7_Gennemlæsning_af_mappe;

import java.io.File;

public class Gennemlæsningafmappe {
    
    public static void main(String[] args) {
        String path =
            "C:/Users/Michael-PC/Desktop/Jule musik";
        ScanDir(path);
        System.out.println();

        int n = ScanDirCount(path);
        System.out.println("Antal mapper: " + n);
    }
    
    public static void ScanDir(String path) {
        System.out.println("[DIR] " + path);
        // skab et File-objekt svarende til mappen path
        File file = new File(path);
        // få listen over alle filer og undermapper
        String[] names = file.list();
        for (String name : names) {
            File file2 = new File(path + "/" + name);
            if (file2.isDirectory()) {
                ScanDir(path + "/" + name);
            }
        }
    }
    
    public static int ScanDirCount(String path) {
        int tal = 0;
        File file = new File(path);
        String[] names = file.list();
        for (String name : names) {
            File file2 = new File(path + "/" + name);
            if (file2.isDirectory()) {
                tal++;
            }
        }
        return tal;
    }
    
}

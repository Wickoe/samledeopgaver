package opgave5_Towers_of_Hanoi;

public class TowersofHanoi {
    
    public static void main(String[] args) {
        System.out.println(flyt(10, 1, 3));
    }

    public static int flyt(int n, int fra, int til) {
        int moveresult = 0;
        
        if (n == 1) {
            moveresult = 1;
        }
        else {
            int temp = 6 - fra - til;
            moveresult += flyt(n - 1, fra, temp);
            moveresult += flyt(n - 1, temp, til) + 1;
        }
        return moveresult;
    }

}

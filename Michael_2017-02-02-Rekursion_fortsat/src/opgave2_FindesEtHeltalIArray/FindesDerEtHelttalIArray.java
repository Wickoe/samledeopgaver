package opgave2_FindesEtHeltalIArray;

public class FindesDerEtHelttalIArray {

    public static void main(String[] args) {
        int[] liste = { 2, 4, 5, 6, 8, 9, 11, 15 };

        int findTal1 = 6;
        int findTal2 = 7;

        System.out.println("Tal1 findes: " + findesDerHeltalIArray(liste, findTal1));
        System.out.println("Tal2 findes: " + findesDerHeltalIArray(liste, findTal2));
    }

    public static boolean findesDerHeltalIArray(int[] array, int target) {
        return findesDerHeltalIArrayHelper(array, target, 0, array.length - 1);
    }

    public static boolean findesDerHeltalIArrayHelper(int[] array, int target, int left,
        int right) {
        boolean found = false;
        
        if (left <= right) {
            int middle = (left + right) / 2;
            int k = array[middle];
            if (k == target) {
                found = true;
            }
            else if (k > target) {
                found = findesDerHeltalIArrayHelper(array, target, left,
                    middle - 1);
            }
            else {
                found = findesDerHeltalIArrayHelper(array, target, middle + 1,
                    right);
            }
        }
        return found;
    }

}

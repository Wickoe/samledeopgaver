package opgave4_Binomial;

public class Binomial {

    public static void main(String[] args) {
        int m = 2;
        int n = 4;

        System.out.println(binomial(m, n));
    }

    public static int binomial(int m, int n) {
        int result = 0;

        if (m == 0 || m == n) {
            result = 1;
        }
        else {
            result = binomial(m, n - 1) + binomial(m - 1, n - 1);
        }
        return result;
    }

}

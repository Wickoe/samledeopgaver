package opgave3_Ackermann;

public class Ackermann {

    public static void main(String[] args) {
        int x = 1;
        int y = 3;

        System.out.println(ackermann(x, y));
    }

    public static int ackermann(int x, int y) {
        int result = 0;

        if (x == 0) {
            result = y + 1;
        }
        else if (y == 0) {
            result = ackermann(x - 1, 1);
        }
        else {
            result = ackermann(x - 1, ackermann(x, y - 1));
        }

        return result;
    }
}

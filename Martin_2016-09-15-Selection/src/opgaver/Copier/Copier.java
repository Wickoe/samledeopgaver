package opgaver.Copier;

public class Copier {
	private int paper;
	private boolean paperStuck;
	
	public Copier() {
		this(10);
	}
	
	public Copier(int paper) {
		this.paper = paper;
	}
	
	public void insertPaper(int paper) {
		this.paper += paper;
		
		if(this.paper > 500) {
			this.paper -= paper;
			System.out.println();
		}
	}
	
	public int getPaper() {
		return paper;
	}
	
	public void makeCopy() {
		if(paper == 0) {
			System.out.println("Out of paper");
		} else if(paperStuck) {
			System.out.println("Paperjam");
		} else {
			paper--;
		}
	}
	
	public void makeCopy(int copies) {
		if(copies > paper) {
			System.out.println("Not enough paper");
		} else if(paperStuck) {
			System.out.println("Paperjam");
		} else {
			paper -= copies;
		}
	}
	
	public void makePaperJam() {
		paperStuck = true;
	}
	
	public void fixJam() {
		paperStuck = false;
	}
}

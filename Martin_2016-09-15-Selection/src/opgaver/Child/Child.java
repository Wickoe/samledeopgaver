package opgaver.Child;

/**
 * Write a description of class Barn here.
 */
public class Child {
	private int age;
	private boolean boy; // true if the child is a boy

	public Child(int age, boolean boy) {
		this.age = age;
		this.boy = boy;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isBoy() {
		return boy;
	}

	public void setBoy(boolean boy) {
		this.boy = boy;
	}

	public String institution() {
		String institution = "Home";
		if(age > 1) {
			institution = "Nursery";
		}
		if(age > 2) {
			institution = "Kindergarden";
		}
		if(age > 5) {
			institution = "School";
		}
		if(age > 16) {
			institution = "Out of school";
		}
		return institution;
	}
	
	public String gender() {
		String gender = "Girl";
		
		if(boy) {
			gender = "Boy";
		}
		
		return gender;
	}
	
	public String team() {
		String team = "Tumbling girls";
		
		if(boy) {
			if(age < 8) {
				team = "Young cubs";
			} else if(age >= 8){
				team = "Cool boys";
			}
		} else {
			if(age >= 8) {
				team = "Springy girls";
			}
		}
		
		return team;
	}
}

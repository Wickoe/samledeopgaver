package opgaver.Child;

public class ChildApp {

	public static void main(String[] args) {
		Child c1 = new Child(4, true);// boy 4 years old
		Child c2 = new Child(2, false);// girl 2 years old
		Child c3 = new Child(2, true);
		Child c4 = new Child(6, true);
		Child c5 = new Child(17, true);
		Child c6 = new Child(4, false);
		Child c7 = new Child(6, false);
		Child c8 = new Child(17, false);

		System.out.println("Et barn på " + c1.getAge());
		System.out.println("Et barn på " + c1.gender());
		System.out.println("Team: " + c1.team());
		System.out.println("Gender: " + c2.gender());
		System.out.println("Team: " + c2.team());
		System.out.println("Gender: " + c3.gender());
		System.out.println("Team: " + c3.team());
		System.out.println("Gender: " + c4.gender());
		System.out.println("Team: " + c4.team());
		System.out.println("Gender: " + c5.gender());
		System.out.println("Team: " + c5.team());
		System.out.println("Gender: " + c6.gender());
		System.out.println("Team: " + c6.team());
		System.out.println("Gender: " + c7.gender());
		System.out.println("Team: " + c7.team());
		System.out.println("Gender: " + c8.gender());
		System.out.println("Team: " + c7.team());
	}

}

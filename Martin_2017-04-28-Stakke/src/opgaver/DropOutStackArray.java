package opgaver;

public class DropOutStackArray implements Stack {
	private Object[] stack;

	public DropOutStackArray() {
		this(10);
	}

	public DropOutStackArray(int size) {
		stack = new Object[size];
	}

	@Override
	public void push(Object element) {
		for (int i = 0; i < (stack.length - 1); i++) {
			stack[i] = stack[i + 1];
		}

		stack[stack.length - 1] = element;
	}

	@Override
	public Object pop() {
		Object o = stack[10];

		for (int i = stack.length - 1; i > 0; i--) {
			stack[i] = stack[i - 1];
		}

		return o;
	}

	@Override
	public Object peek() {
		return stack[stack.length - 1];
	}

	@Override
	public boolean isEmpty() {
		return stack[stack.length - 1] == null;
	}

	@Override
	public int size() {
		int stackSize = 0;

		for (Object o : stack) {
			if (o != null) {
				stackSize++;
			}
		}

		return stackSize;
	}

	@Override
	public String toString() {
		String toString = "[";
		@SuppressWarnings("unused")
		int objAt = 1;
		Object currObject = stack[0];
		toString += currObject.toString();

		// while (objAt < stack.length && currObject != null) {
		// currObject = stack[objAt];
		// toString += ", " + currObject.toString();
		// objAt++;
		// }

		// for (int i = 0; i < stack.length; i++) {
		// Object currObject = stack[i];
		//
		// if (currObject != null) {
		// toString += currObject.toString();
		// }
		// else {
		// toString += currObject;
		// }
		//
		// int check = i;
		// if ((check + 1) == stack.length) {
		// toString += "]";
		// }
		// else {
		// toString += ", ";
		// }
		// }

		return toString;
	}
}
package opgaver;

public class ArrayStack implements Stack {
	private Object[] stack;
	private int head;

	public ArrayStack() {
		this(10);
	}

	public ArrayStack(int size) {
		stack = new Object[size];
		head = 0;
	}

	@Override
	public void push(Object element) {
		stack[head] = element;
		head++;
		
		if (head == stack.length) {
			Object[] newStack = new Object[stack.length * 2];
			
			for (int i = 0; i < head; i++) {
				newStack[i] = stack[i];
			}
			
			stack = newStack;
		}
	}
	
	@Override
	public Object pop() {
		if (head > 0) {
			head--;
		}

		Object popped = stack[head];
		stack[head] = null;
		
		return popped;
	}
	
	@Override
	public Object peek() {
		Object peeked = null;
		if (head > 0) {
			return stack[head - 1];
		}

		return peeked;
	}
	
	@Override
	public boolean isEmpty() {
		return stack[0] == null;
	}
	
	@Override
	public int size() {
		return head;
	}
	
	@Override
	public String toString() {
		String toString = "[";
		
		for (int i = 0; i < head; i++) {
			toString += stack[i];

			if (i + 1 != head) {
				toString += ",";
			}
		}
		
		toString += "]";
		return toString;
	}
	
	public int arraySize() {
		return stack.length;
	}
	
	public static boolean checkParenteses(String s) {
		boolean isBalanced = true;
		ArrayStack stack = new ArrayStack();
		
		int charAt = 1;
		char currentChar = s.charAt(0);
		
		while (isBalanced && charAt < s.length()) {
			if (Character.isMirrored(currentChar + 1) || Character.isMirrored(currentChar + 2)
				|| Character.isMirrored(currentChar + 16)) {
				stack.push(currentChar);
			}
			else if (Character.isMirrored(currentChar)) {
				char popped = (char) stack.pop();
				isBalanced = (char) (currentChar - 1) == popped
					|| (char) (currentChar - 2) == popped || (char) (currentChar - 16) == popped;
			}
			
			currentChar = s.charAt(charAt);
			charAt++;
		}
		
		return isBalanced;
	}
}

package opgaver;

public class DropOutStackSingleLinkedList implements Stack {
	private Node first;
	private int fixedSize;

	private class Node {
		private Node next;
		private Object data;
	}

	public DropOutStackSingleLinkedList(int size) {
		first = null;
		this.fixedSize = size;
	}
	
	@Override
	public void push(Object element) {
		Node newNode = new Node();
		newNode.data = element;

		newNode.next = first;
		first = newNode;

		Node nodeAt = first;
		Node previous = null;
		int counter = 0;

		while (nodeAt.next != null) {
			previous = nodeAt;
			nodeAt = nodeAt.next;
			counter++;
		}

		if (counter >= fixedSize) {
			previous.next = null;
		}
	}
	
	@Override
	public Object pop() {
		Node popped = first;
		first = first.next;
		
		return popped;
	}
	
	@Override
	public Object peek() {
		return first;
	}
	
	@Override
	public boolean isEmpty() {
		return first == null;
	}
	
	@Override
	public int size() {
		int sizeCount = 0;
		Node nodeAt = first;

		while (nodeAt != null) {
			sizeCount++;
			nodeAt = nodeAt.next;
		}

		return sizeCount;
	}
	
	@Override
	public String toString() {
		String toString = "[";
		Node nodeAt = first;

		while (!isEmpty() && nodeAt.next != null) {
			toString += nodeAt.data.toString();
		}

		toString += "]";
		
		return toString;
	}
}

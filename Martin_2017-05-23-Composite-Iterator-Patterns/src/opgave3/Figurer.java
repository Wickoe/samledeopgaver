package opgave3;

import java.util.ArrayList;

public class Figurer implements FigurComponent {
	private ArrayList<FigurComponent> figurComponents;
	private String navn;

	public Figurer(String etNavn) {
		figurComponents = new ArrayList<>();
		navn = etNavn;
	}

	@Override
	public String getNavn() {
		return navn;
	}

	@Override
	public void setNavn(String etNavn) {
		navn = etNavn;
	}

	@Override
	public void draw(int tabSize) {
		for (int i = 0; i < tabSize; i++) {
			System.out.print("\t");
		}
		System.out.print(navn + ":");
		System.out.println();
		for (FigurComponent fc : figurComponents) {
			fc.draw(tabSize + 1);
		}
	}

	@Override
	public double getArea() {
		return figurComponents.size();
	}

	public void add(FigurComponent enFigurComponent) {
		figurComponents.add(enFigurComponent);
	}
}
package opgave3;

public interface FigurComponent {
	public String getNavn();

	public void setNavn(String etNavn);

	public void draw(int tabSize);
	
	public double getArea();
}

package opgave3;

public class Figur implements FigurComponent {
	private String navn;
	private double area;
	
	public Figur(String etNavn, double anArea) {
		navn = etNavn;
		area = anArea;
	}

	@Override
	public String getNavn() {
		return navn;
	}

	@Override
	public void setNavn(String etNavn) {
		navn = etNavn;
	}

	@Override
	public void draw(int tabSize) {
		for (int i = 0; i < tabSize; i++) {
			System.out.print("\t");
		}
		System.out.print(navn + ", " + area);
		System.out.println();
	}

	@Override
	public double getArea() {
		return area;
	}
}
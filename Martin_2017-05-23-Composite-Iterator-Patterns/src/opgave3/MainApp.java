package opgave3;

public class MainApp {
	public static void main(String[] args) {
		Figurer kvadrater = new Figurer("Kvadrater");
		kvadrater.add(new Figur("Kvadrat 5x5", 25));
		kvadrater.add(new Figur("Kvadrat 2x2", 4));
		kvadrater.add(new Figur("Kvadrat 6x6", 36));
		
		Figurer rektangler = new Figurer("Rektangler");
		rektangler.add(new Figur("Rektangel 5x6", 30));
		rektangler.add(new Figur("Rektangel 2x4", 8));
		rektangler.add(kvadrater);

		Figurer figurer = new Figurer("Figurer");
		Figurer cirkler = new Figurer("Cirkler");
		Figurer ellipser = new Figurer("Ellipse");

		figurer.add(ellipser);
		figurer.add(rektangler);
		
		cirkler.add(new Figur("Cirkel /m radius 5", 25));
		cirkler.add(new Figur("Cirkel /m radius 6", 30));
		
		ellipser.add(new Figur("Ellipse /m wRaius 5, hRadius 6", 32));
		ellipser.add(cirkler);

		figurer.draw(0);
	}
}
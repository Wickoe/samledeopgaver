package door;

public class DoorStayOpen extends DoorState {
	private Door door;

	public DoorStayOpen(Door aDoor) {
		door = aDoor;
	}
	
	@Override
	public String toString() {
		return "STAY OPEN";
	}

	@Override
	public void click() {
		door.setState(door.getClosingState());
		door.getState().complete();
	}
}

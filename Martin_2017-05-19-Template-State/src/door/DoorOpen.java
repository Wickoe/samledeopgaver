package door;

public class DoorOpen extends DoorState {
	private Door door;
	
	public DoorOpen(Door aDoor) {
		door = aDoor;
	}

	@Override
	public String toString() {
		return "OPEN";
	}
	
	@Override
	public void click() {
		door.setState(door.getStayOpenState());
		door.stopTimer();
	}

	@Override
	public void timeout() {
		door.startTimer(2000, event -> {
			door.setState(door.getClosingState());
			door.getState().complete();
		});
	}
}

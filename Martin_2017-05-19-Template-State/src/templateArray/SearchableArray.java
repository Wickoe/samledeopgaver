package templateArray;

import templateList.SearchPattern;

public class SearchableArray<E extends Comparable<E>> extends SearchPattern<E> {
	private E[] list;
	private int indexAt = -1;

	public SearchableArray(E[] aList) {
		list = aList;
	}
	
	@Override
	protected void init() {
		indexAt = 0;
	}
	
	@Override
	protected boolean isEmpty() {
		return indexAt < 0 || indexAt >= list.length;
	}
	
	@Override
	protected E select() {
		E element = null;

		if (!isEmpty()) {
			element = list[indexAt];
		}
		return element;
	}
	
	@Override
	protected void split(E m) {
		indexAt++;
	}
	
}

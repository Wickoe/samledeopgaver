package templateArray;

import templateList.SearchPattern;

public class SearchableArrayBin<E extends Comparable<E>> extends SearchPattern<E> {
	private E[] list;
	private int middle = -1;
	private int left = -1;
	private int right = -1;
	
	public SearchableArrayBin(E[] aList) {
		list = aList;
	}

	@Override
	protected void init() {
		left = 0;
		right = list.length - 1;
		middle = (right + left) / 2;
	}

	@Override
	protected boolean isEmpty() {
		return right < left;
	}

	@Override
	protected E select() {
		return list[middle];
	}

	@Override
	protected void split(E m) {
		if (m.compareTo(list[middle]) < 0) {
			right = middle - 1;
		}
		else {
			left = middle + 1;
		}
		
		middle = (right + left) / 2;
	}
}
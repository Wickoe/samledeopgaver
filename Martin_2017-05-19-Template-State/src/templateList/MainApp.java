package templateList;

import java.util.ArrayList;
import java.util.Collections;

import templateArray.SearchableArray;
import templateArray.SearchableArrayBin;

public class MainApp {
	public static void main(String[] args) {
		ArrayList<String> navne = new ArrayList<>();
		String[] navneTwoThree = new String[12];
		
		navne.add("Martin");
		navne.add("Nicolai");
		navne.add("Michael");
		navne.add("Leo");
		navne.add("Holme");
		navne.add("Skåde");
		navne.add("Viby");
		navne.add("Beder");
		navne.add("Staustrup");
		navne.add("Engdal");
		navne.add("Forældreskolen");
		navne.add("Malling");
		
		Collections.sort(navne);

		for (int i = 0; i < navne.size(); i++) {
			navneTwoThree[i] = navne.get(i);
		}

		SearchableList<String> navneLinearSearch = new SearchableList<>(navne);
		SearchableArray<String> navneLinearSearchTwo = new SearchableArray<>(navneTwoThree);
		SearchableArrayBin<String> navneBinearSearch = new SearchableArrayBin<>(navneTwoThree);
		
		System.out.println("Linear List search:");
		System.out.println(navneLinearSearch.search("Leo"));
		System.out.println(navneLinearSearch.search("Bo"));
		System.out.println(navneLinearSearch.search("Malling"));
		System.out.println(navneLinearSearch.search("Riisskov"));
		System.out.println();
		System.out.println("Linear Array search:");
		System.out.println(navneLinearSearchTwo.search("Leo"));
		System.out.println(navneLinearSearchTwo.search("Bo"));
		System.out.println(navneLinearSearchTwo.search("Malling"));
		System.out.println(navneLinearSearchTwo.search("Riisskov"));
		System.out.println();
		System.out.println("Binear Array search:");
		System.out.println(navneBinearSearch.search("Leo"));
		System.out.println(navneBinearSearch.search("Bo"));
		System.out.println(navneBinearSearch.search("Malling"));
		System.out.println(navneBinearSearch.search("Riisskov"));
	}
}
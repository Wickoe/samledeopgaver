package templateList;

import java.util.List;

public class SearchableList<E extends Comparable<E>> extends SearchPattern<E> {
	private List<E> list;
	private int indexAt;
	
	public SearchableList(List<E> list) {
		this.list = list;
	}

	@Override
	protected void init() {
		indexAt = 0;
	}
	
	@Override
	protected boolean isEmpty() {
		return list.size() <= indexAt;
	}
	
	@Override
	protected E select() {
		E element = null;
		
		if (indexAt >= 0 && indexAt < list.size()) {
			element = list.get(indexAt);
		}

		return element;
	}
	
	@Override
	protected void split(E m) {
		indexAt++;
	}
	
}

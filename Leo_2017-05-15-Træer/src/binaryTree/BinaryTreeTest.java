package binaryTree;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BinaryTreeTest {
    BinaryTree<Integer> tree;
    
    @Before
    public void setUp() {
        BinaryTree<Integer> n8 = new BinaryTree<>(8);
        BinaryTree<Integer> n9 = new BinaryTree<>(9);
        BinaryTree<Integer> n5 = new BinaryTree<>(5, n8, n9);

        BinaryTree<Integer> n4 = new BinaryTree<>(4);
        BinaryTree<Integer> n2 = new BinaryTree<>(2, n4, n5);
        
        BinaryTree<Integer> n6 = new BinaryTree<>(6);
        BinaryTree<Integer> n7 = new BinaryTree<>(7);
        BinaryTree<Integer> n3 = new BinaryTree<>(3, n6, n7);

        tree = new BinaryTree<Integer>(1, n2, n3);
    }

    @Test
    public void testSize() {
        assertEquals(9, tree.size());
    }
    
    @Test
    public void testLeft() {
        assertEquals(2, tree.left().data().intValue());
        assertEquals(4, tree.left().left().data().intValue());
    }
    
    @Test
    public void testRight() {
        assertEquals(3, tree.right().data().intValue());
        assertEquals(7, tree.right().right().data().intValue());
    }

    @Test
    public void testEmpty() {
        assertFalse(tree.isEmpty());
    }
    
    @Test
    public void testReplace() {
        tree.replace(10);
        assertEquals(10, tree.data().intValue());
    }

}

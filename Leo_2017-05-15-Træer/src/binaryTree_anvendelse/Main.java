package binaryTree_anvendelse;

public class Main
{

	public static void main(String[] args)
	{
		BinaryTree<Integer> fourth3 = new BinaryTree<Integer>(88, null, null);
		BinaryTree<Integer> fourth2 = new BinaryTree<Integer>(25, null, null);
		BinaryTree<Integer> fourth1 = new BinaryTree<Integer>(15, null, null);
		BinaryTree<Integer> third3 = new BinaryTree<Integer>(90, fourth3, null);
		BinaryTree<Integer> thrid2 = new BinaryTree<Integer>(30, fourth2, null);
		BinaryTree<Integer> third1 = new BinaryTree<Integer>(11, null, fourth1);
		BinaryTree<Integer> secound2 = new BinaryTree<Integer>(77, null, third3);
		BinaryTree<Integer> secound1 = new BinaryTree<Integer>(22, third1, thrid2);
		BinaryTree<Integer> first = new BinaryTree<Integer>(45, secound1, secound2);

		// print the height which should be 4
		System.out.println("Height: " + first.height());

		// preorder
		System.out.print("Preorder: ");
		System.out.println(first.toStringPreorder());

		// inorder
		System.out.print("Inorder: ");
		System.out.println(first.toStringInorder());

		// postorder
		System.out.print("Postorder: ");
		System.out.println(first.toStringPostorder());

		// sum
		System.out.print("Total sum: ");
		System.out.println(first.sum());

		// average
		System.out.print("Average: ");
		System.out.println(first.average());

	}

}

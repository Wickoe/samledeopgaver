package binaryTree_anvendelse;

/**
 * A binary tree in which each node has two children.
 */
public class BinaryTree<E> {
	private Node root;

	/**
	 * Constructs an empty tree.
	 */
	public BinaryTree() {
		root = null;
	}

	/**
	 * Constructs a tree with one node and no children.
	 *
	 * @param rootData
	 *            the data for the root
	 */
	public BinaryTree(E rootData) {
		root = new Node();
		root.data = rootData;
		root.left = null;
		root.right = null;
	}

	/**
	 * Constructs a binary tree.
	 *
	 * @param rootData
	 *            the data for the root
	 * @param left
	 *            the left subtree
	 * @param right
	 *            the right subtree
	 */
	public BinaryTree(E rootData, BinaryTree<E> left, BinaryTree<E> right) {
		root = new Node();
		root.data = rootData;
		if (left != null) {
			root.left = left.root;
		}
		if (right != null) {
			root.right = right.root;
		}
	}

	/**
	 * Checks whether this tree is empty.
	 *
	 * @return true if this tree is empty
	 */
	public boolean isEmpty() {
		return root == null;
	}

	/**
	 * Gets the data at the root of this tree.
	 *
	 * @return the root data
	 */
	public E data() {
		return root.data;
	}

	/**
	 * Gets the left subtree of this tree.
	 *
	 * @return the left child of the root
	 */
	public BinaryTree<E> left() {
		BinaryTree<E> result = new BinaryTree<E>();
		result.root = root.left;
		return result;
	}

	/**
	 * Gets the right subtree of this tree.
	 *
	 * @return the right child of the root
	 */
	public BinaryTree<E> right() {
		// TODO exercise 1
		BinaryTree<E> result = new BinaryTree<E>();
		result.root = root.right;
		return result;
	}

	/**
	 * @param rootData
	 *            the new data for the root
	 * @return the data previous in the root
	 */
	public E replace(E rootData) {
		E oldrootdata = root.data;
		root.data = rootData;
		return oldrootdata;
	}

	/**
	 * Returns true if n has no children
	 */
	@SuppressWarnings("unused")
	private boolean isLeaf(Node n) {
		boolean leaf = false;
		if (root.right == null && root.left == null) {
			leaf = true;
		}
		return leaf;
	}

	/**
	 * Returns true if n has at least one child
	 */
	@SuppressWarnings("unused")
	private boolean isInternal(Node n) {
		// TODO exercise 1
		boolean internal = false;
		if (root.right != null || root.left != null) {
			internal = true;
		}
		return internal;
	}

	/**
	 * The size of the tree
	 *
	 * @return the number of nodes in the tree
	 */
	public int size() {
		return size(root);
	}

	// Private recursive helper method
	private int size(Node n) {
		// TODO exercise 1
		if (n == null) {
			return 0;
		} else {
			return 1 + size(n.left) + size(n.right);
		}
	}

	public int height() {
		return height(root);
	}

	public int height(Node n) {
		if (n == null) {
			return 0;
		} else {
			return 1 + Math.max(height(n.left), height(n.right));
		}
	}

	private class Node {
		public E data;
		public Node left;
		public Node right;
	}

	// --------------------------------------------------------
	public String toStringPreorder() {
		return preorder(root);
	}

	public String preorder(Node root) {
		if (root != null) {
			System.out.printf("%d ", root.data);

			preorder(root.left);
			preorder(root.right);
		}
		return "";
	}

	// --------------------------------------------------------

	public String toStringInorder() {
		return inOrder(root);
	}

	public String inOrder(Node root) {
		if (root != null) {
			inOrder(root.left);
			// Visit the node by Printing the node data
			System.out.printf("%d ", root.data);
			inOrder(root.right);
		}
		return "";
	}

	// --------------------------------------------------------

	public String toStringPostorder() {
		return postOrder(root);
	}

	public String postOrder(Node root) {
		if (root != null) {
			postOrder(root.left);
			postOrder(root.right);
			// Visit the node by Printing the node data
			System.out.printf("%d ", root.data);
		}
		return "";
	}

	@Override
	public String toString() {
		return "" + root.data;
	}

	public int sum() {
		return sum(root);
	}

	// Private recursive helper method
	private int sum(Node n) {
		// TODO exercise 1
		if (n == null) {
			return 0;
		} else {
			int temp = (int) n.data;
			return temp + sum(n.left) + sum(n.right);
		}
	}

	public double average() {
		double average = sum(root);
		double temp = size(root);
		average = average / temp;
		return average;
	}

}

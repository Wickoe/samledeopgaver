package binaryTree;

public class DecisionTree {
	private static BinaryTree<String> questionTree;

	public static void main(String[] args) {
		initialize();
		questionTree.inOrder();
	}

	private static void initialize() {
		BinaryTree<String> tiger = new BinaryTree<>("It ia a tiger.");
		BinaryTree<String> zebra = new BinaryTree<>("It is a zebra.");
		BinaryTree<String> canivore = new BinaryTree<>(
			"Is it a carnivore?", tiger, zebra);
		
		BinaryTree<String> pig = new BinaryTree<>("It is a pig.");
		BinaryTree<String> stripes = new BinaryTree<>(
			"Does it have stripes?", canivore, pig);
		
		BinaryTree<String> penguin = new BinaryTree<>("It is a penguin.");
		BinaryTree<String> ostrich = new BinaryTree<>("It is an ostrich.");
		BinaryTree<String> swim = new BinaryTree<>("Does it swim?",
			penguin, ostrich);
		
		BinaryTree<String> eagle = new BinaryTree<>("It is an eagle.");
		BinaryTree<String> fly = new BinaryTree<>("Does it fly?", eagle,
			swim);
		
		questionTree = new BinaryTree<>(
			"Is it a mammal?", stripes, fly);
	}
}

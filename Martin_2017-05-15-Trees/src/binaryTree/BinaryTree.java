package binaryTree;

/**
 * A binary tree in which each node has two children.
 */
public class BinaryTree<E> {
	private Node root;

	/**
	 * Constructs an empty tree.
	 */
	public BinaryTree() {
		root = null;
	}

	/**
	 * Constructs a tree with one node and no children.
	 * @param rootData the data for the root
	 */
	public BinaryTree(E rootData) {
		root = new Node();
		root.data = rootData;
		root.left = null;
		root.right = null;
	}

	/**
	 * Constructs a binary tree.
	 * @param rootData the data for the root
	 * @param left the left subtree
	 * @param right the right subtree
	 */
	public BinaryTree(E rootData, BinaryTree<E> left, BinaryTree<E> right) {
		root = new Node();
		root.data = rootData;
		if (left != null) {
			root.left = left.root;
		}
		if (right != null) {
			root.right = right.root;
		}
	}

	/**
	 * Checks whether this tree is empty.
	 * @return true if this tree is empty
	 */
	public boolean isEmpty() {
		return root == null;
	}

	/**
	 * Gets the data at the root of this tree.
	 * @return the root data
	 */
	public E data() {
		return root.data;
	}

	/**
	 * Gets the left subtree of this tree.
	 * @return the left child of the root
	 */
	public BinaryTree<E> left() {
		BinaryTree<E> result = new BinaryTree<>();
		result.root = root.left;
		return result;
	}

	/**
	 * Gets the right subtree of this tree.
	 * @return the right child of the root
	 */
	public BinaryTree<E> right() {
		BinaryTree<E> result = new BinaryTree<>();
		result.root = root.right;
		return result;
	}

	/**
	 * @param rootData the new data for the root
	 * @return the data previous in the root
	 */
	public E replace(E rootData) {
		Node newRoot = new Node();
		newRoot.data = rootData;
		newRoot.left = root.left;
		newRoot.right = root.right;
		root = newRoot;
		return newRoot.data;
	}

	/**
	 * Returns true if n has no children
	 */
	private boolean isLeaf(Node n) {
		return n != null && n.right == null && n.left == null;
	}

	/**
	 * Returns true if n has at least one child
	 */
	private boolean isInternal(Node n) {
		return n != null && (n.left != null || n.right != null);
	}

	/**
	 * The size of the tree
	 * @return the number of nodes in the tree
	 */
	public int size() {
		return size(root);
	}

	// Private recursive helper method
	private int size(Node n) {
		int counter = 0;

		if (isInternal(n)) {
			if (n.left != null) {
				counter += size(n.left);
			}
			if (n.right != null) {
				counter += size(n.right);
			}
		}

		if (n != null) {
			counter++;
		}
		
		return counter;
	}

	public int height() {
		return height(root);
	}

	private int height(Node n) {
		int counter = 0;
		
		int leftHeight = 0;
		int rightHeight = 0;

		if (n.left != null) {
			leftHeight = height(n.left);
		}
		if (n.right != null) {
			rightHeight = height(n.right);
		}

		if (isInternal(n)) {
			counter = max(leftHeight, rightHeight);
		}

		counter++;
		
		return counter;
	}
	
	private int max(int heightLeft, int heightRight) {
		if (heightLeft > heightRight) {
			return heightLeft;
		}
		else {
			return heightRight;
		}
	}

	public void inOrder() {
		inOrder(root);
	}
	
	private void inOrder(Node n) {
		if (isInternal(n)) {
			inOrder(n.left);
			System.out.print(n.data.toString() + " ");
			inOrder(n.right);
		}

		if (isLeaf(n)) {
			if (n != null) {
				System.out.print(n.data.toString() + " ");
			}
			else {
				System.out.print(" ");
			}
		}
	}

	public void postOrder() {
		postOrder(root);
	}
	
	private void postOrder(Node n) {
		if (isInternal(n)) {
			postOrder(n.left);
			postOrder(n.right);
			System.out.print(n.data.toString() + " ");
		}

		if (isLeaf(n)) {
			if (n != null) {
				System.out.print(n.data.toString() + " ");
			}
			else {
				System.out.print(" ");
			}
		}
	}

	public void preOrder() {
		preOrder(root);
	}
	
	private void preOrder(Node n) {
		if (isInternal(n)) {
			System.out.print(n.data.toString() + " ");
			preOrder(n.left);
			preOrder(n.right);
		}

		if (isLeaf(n)) {
			if (n != null) {
				System.out.print(n.data.toString() + " ");
			}
			else {
				System.out.print(" ");
			}
		}
	}

	public int sumOf() {
		return sumOf(root);
	}

	private int sumOf(Node n) {
		if (n == null) {
			return 0;
		}

		int data = (int) n.data;
		return data + sumOf(n.left) + sumOf(n.right);
	}

	public double avg() {
		return sumOf() / (double) size();
	}
	
	private class Node {
		public E data;
		public Node left;
		public Node right;
	}
}

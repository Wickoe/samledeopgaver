package binaryTree;

public class Opgave2 {
	public static void main(String[] args) {
		BinaryTree<Integer> b1 = new BinaryTree<>();
		BinaryTree<Integer> b2 = new BinaryTree<>(15);
		BinaryTree<Integer> b3 = new BinaryTree<>(11, b1, b2);

		BinaryTree<Integer> b4 = new BinaryTree<>(25);
		BinaryTree<Integer> b5 = new BinaryTree<>();
		BinaryTree<Integer> b6 = new BinaryTree<>(30, b4, b5);
		
		BinaryTree<Integer> b7 = new BinaryTree<>(22, b3, b6);

		BinaryTree<Integer> b8 = new BinaryTree<>(88);
		BinaryTree<Integer> b9 = new BinaryTree<>();
		BinaryTree<Integer> b10 = new BinaryTree<>(90, b8, b9);
		
		BinaryTree<Integer> b11 = new BinaryTree<>();
		BinaryTree<Integer> b12 = new BinaryTree<>(77, b11, b10);

		BinaryTree<Integer> b13 = new BinaryTree<>(45, b7, b12);

		System.out.println(b13.height());
		b13.inOrder();
		System.out.println();
		b13.postOrder();
		System.out.println();
		b13.preOrder();
		System.out.println();
		System.out.println(b13.sumOf());
		System.out.println();
		System.out.println(b13.avg());
	}
}

package opgave1;

public class SortedLinkedList {
	private class Node implements Comparable<Node> {
		public Node next;
		public String data;
		
		@Override
		public int compareTo(Node o) {
			return data.compareToIgnoreCase(o.data);
		}
	}
	
	private Node first;
	
	public SortedLinkedList() {
		first = null;
	}
	
	/**
	* Tilføjer et element til listen, så listen fortsat er sorteret i
	* henhold til den naturlige ordning på elementerne.
	* @param element det der indsættes
	*/
	public void addElement(String element) {
		Node node = new Node();
		node.data = element;
		
		Node at = first;
		Node previous = null;
		boolean inserted = false;
		
		while (!inserted) {
			if (at == null) {
				node.next = first;
				first = node;
				inserted = true;
			}
			else {
				if (node.compareTo(at) > 0 && at.next == null) {
					at.next = node;
					inserted = true;
				}
				else if (node.compareTo(at) < 1) {
					if (at == first) {
						node.next = at;
						first = node;
					}
					else {
						node.next = at;
						previous.next = node;
					}

					inserted = true;
				}
				else {
					previous = at;
					at = at.next;
				}
			}
		}
	}

	public void addElement2(String element) {
		Node node = new Node();
		node.data = element;

		Node at = first;
		Node previous = null;
		boolean inserted = false;

		while (!inserted && at != null) {
			if (node.compareTo(at) >= 1) {
				previous = at;
				at = at.next;
			}
			else {
				inserted = true;
			}
		}
		
		if (previous == null) {
			node.next = at;
			first = node;
		}
		else {
			previous.next = node;
			node.next = at;
		}
	}
	
	/**
	* Fjerner et element fra listen.
	* @param element det element der fjernes
	* @return true hvis elementet blev fjernet, men ellers false.
	*/
	public boolean removeElement(String element) {
		Node at = first;
		Node previous = null;
		boolean removed = false;

		while (!removed && at != null) {
			if (at.data.equalsIgnoreCase(element)) {
				if (at.equals(first)) {
					first = first.next;
				}
				else {
					previous.next = at.next;
				}

				removed = true;
			}
			else {
				previous = at;
				at = at.next;
			}
		}

		return removed;
	}
	
	/**
	* Beregner antallet af elementer i listen.
	*/
	public int countElements() {
		int countSize = 0;
		Node at = first;

		while (at != null) {
			countSize++;
			at = at.next;
		}

		return countSize;
	}
	
	@Override
	public String toString() {
		String toString = "";
		
		Node at = first;
		
		while (at != null) {
			toString += at.data + ", ";
			at = at.next;
		}
		
		return toString;
	}
}

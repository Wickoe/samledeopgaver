package opgave1;

public class MainTestApp {
	public static void main(String[] args) {
		String martin = "Martin";
		String michael = "Michael";
		String leo = "Leo";
		String nicolai = "Nicolai";
		String chriss = "Chriss";

		SortedLinkedList sll = new SortedLinkedList();

		System.out.println("1");
		System.out.println(sll);

		sll.addElement2(michael);

		System.out.println("2");
		System.out.println(sll);

		sll.addElement2(martin);

		System.out.println("3");
		System.out.println(sll);

		sll.addElement2(leo);
		
		System.out.println("4");
		System.out.println(sll);
		
		sll.addElement2(nicolai);
		
		System.out.println("5");
		System.out.println(sll);
		
		sll.addElement2(chriss);

		System.out.println("6");
		System.out.println(sll);

		sll.removeElement(martin);

		System.out.println("7");
		System.out.println(sll);
	}
}

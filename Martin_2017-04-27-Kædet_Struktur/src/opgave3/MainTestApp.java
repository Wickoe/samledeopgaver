package opgave3;

public class MainTestApp {
	public static void main(String[] args) {
		String martin = "Martin";
		String michael = "Michael";
		String leo = "Leo";
		String nicolai = "Nicolai";
		String chriss = "Chriss";
		
		SortedLinkedList sll = new SortedLinkedList();
		sll.addElement(michael);
		sll.addElement(martin);
		sll.addElement(leo);
		sll.addElement(nicolai);
		sll.addElement(chriss);
		sll.removeElement("Martin");
		
		SortedLinkedList sll2 = new SortedLinkedList();
		sll2.addElement(martin);
		
		sll.addAll(sll2);

		System.out.println(sll);
	}
}

package opgave3;

public class SortedLinkedList {
	private class Node implements Comparable<Node> {
		public Node next;
		public String data;
		
		@Override
		public int compareTo(Node o) {
			return data.compareToIgnoreCase(o.data);
		}
	}
	
	private Node first;
	
	public SortedLinkedList() {
		first = null;
	}
	
	/**
	* Tilføjer et element til listen, så listen fortsat er sorteret i
	* henhold til den naturlige ordning på elementerne.
	* @param element det der indsættes
	*/
	public void addElement(String element) {
		Node node = new Node();
		node.data = element;

		Node at = first;
		Node previous = null;
		boolean inserted = false;

		while (!inserted && at != null) {
			if (node.compareTo(at) >= 1) {
				previous = at;
				at = at.next;
			}
			else {
				inserted = true;
			}
		}
		
		if (previous == null) {
			node.next = at;
			first = node;
		}
		else {
			previous.next = node;
			node.next = at;
		}
	}
	
	/**
	* Fjerner et element fra listen.
	* @param element det element der fjernes
	* @return true hvis elementet blev fjernet, men ellers false.
	*/
	public boolean removeElement(String element) {
		Node at = first;
		Node previous = null;
		boolean removed = false;

		while (!removed && at != null) {
			if (at.data.equalsIgnoreCase(element)) {
				if (at.equals(first)) {
					first = first.next;
				}
				else {
					previous.next = at.next;
				}

				removed = true;
			}
			else {
				previous = at;
				at = at.next;
			}
		}

		return removed;
	}
	
	/**
	* Beregner antallet af elementer i listen.
	*/
	public int countElements() {
		int countSize = 0;
		Node at = first;

		while (at != null) {
			countSize++;
			at = at.next;
		}

		return countSize;
	}
	
	@Override
	public String toString() {
		String toString = "";
		
		Node at = first;
		
		while (at != null) {
			toString += at.data + ", ";
			at = at.next;
		}
		
		return toString;
	}

	/**
	 * Tilføjer alle elementerne fra list til den aktuelle liste.
	 * Listen er fortsat sorteret i henhold til den naturlige ordning på
	 * elementerne.
	 */
	public void addAll(SortedLinkedList list) {
		Node atOwnList = first;
		Node atOtherList = list.first;

		while (atOwnList != null && atOtherList != null) {
			if (atOwnList.compareTo(atOtherList) > 0) {
				atOwnList = atOwnList.next;
			}
			else {
				Node otherNode = atOtherList;
				atOtherList = atOtherList.next;
				
				Node nextOwn = atOwnList.next;
				
				atOwnList.next = otherNode;
				otherNode.next = nextOwn;
			}
		}
	}
	
}

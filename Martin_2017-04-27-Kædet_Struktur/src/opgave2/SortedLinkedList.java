package opgave2;

public class SortedLinkedList {
	private class DNode implements Comparable<DNode> {
		public DNode previous;
		public DNode next;
		public String data;

		@Override
		public int compareTo(DNode o) {
			return data.compareToIgnoreCase(o.data);
		}
	}

	private DNode first;
	private DNode last;

	public SortedLinkedList() {
		first = new DNode();
		last = new DNode();

		first.next = last;
		first.previous = null;
		first.data = null;

		last.previous = first;
		last.next = null;
		last.data = null;
	}

	/**
	* Tilføjer et element til listen, så listen fortsat er sorteret i
	* henhold til den naturlige ordning på elementerne.
	* @param element det der indsættes
	*/
	public void addElement(String element) {
		DNode nyNode = new DNode();
		nyNode.data = element;

		DNode at = first.next;
		boolean inserted = false;

		while (!inserted && at != last) {
			if (nyNode.compareTo(at) < 1) {
				inserted = true;
			}
			else {
				at = at.next;
			}
		}
		
		at = at.previous;

		nyNode.previous = at;
		nyNode.next = at.next;
		at.next = nyNode;
		nyNode.next.previous = nyNode;
	}

	/**
	* Fjerner et element fra listen.
	* @param element det element der fjernes
	* @return true hvis elementet blev fjernet, men ellers false.
	*/
	public boolean removeElement(String element) {
		boolean removed = false;
		DNode at = first.next;

		while (!removed && at != last) {
			if (at.data.equalsIgnoreCase(element)) {
				at.previous.next = at.next;
				at.next.previous = at.previous;
				removed = true;
			}
			else {
				at = at.next;
			}
		}

		return removed;
	}

	/**
	* Beregner antallet af elementer i listen.
	*/
	public int countElements() {
		int countSize = 0;
		DNode at = first;
		
		while (at != null) {
			countSize++;
			at = at.next;
		}
		
		return countSize;
	}

	@Override
	public String toString() {
		String toString = "";

		DNode at = first.next;

		while (at != last) {
			toString += ", " + at.data;
			at = at.next;
		}

		return toString;
	}
}
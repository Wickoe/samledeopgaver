package opgave3_Product;

public class Product {

    public static void main(String[] args) {
        int a = 19;
        int b = 3;

        System.out.println(product(a, b));
        System.out.println(productRus(a, b));
    }

    public static int product(int a, int b) {
        int result = 0;

        if (a == 1) {
            result = b;
        }
        else if (a == 0) {
            result = 0;
        }
        else {
            result = product(a - 1, b) + b;
        }
        return result;
    }

    public static int productRus(int a, int b) {
        int result = 0;
        if (a == 0) {
            result = 0;
        }
        else if (a % 2 == 1) {
            result = productRus(a - 1, b) + b;
        }
        else {
            result = productRus(a / 2, b * b);
        }

        return result;
    }

}

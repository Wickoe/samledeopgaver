package opgave5_Euclids;

public class Euclids {

    public static void main(String[] args) {
        int a = 10;
        int b = 4;
        
        System.out.println(sfd(a, b));
    }

    public static int sfd(int a, int b) {
        int result = 0;
        if (b <= a) {
            result = b;
        }
        else if (a < b) {
            result = sfd(b, a);
        }
        else {
            result = sfd(b, a % b);
        }
        return result;
    }

}

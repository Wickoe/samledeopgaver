package opgave4_Reverse;

public class Reverse {

    public static void main(String[] args) {
        String ord = "RANSLIRPA";

        System.out.println(reverse(ord));
    }

    public static String reverse(String s) {
        String result = "";
        if (s.length() == 1) {
            result += s.charAt(0);
        }
        else {
            result = reverse(s.substring(1, s.length())) + s.charAt(0);
        }
        return result;
    }

}

package opgave1_Factorial;

public class Factorial {

	public static void main(String[] args) {
		int tal = 4;

		System.out.println(factorial(tal));
	}

	public static int factorial(int n) {
		int result = 0;
		// Termineringsregel
		if (n == 0) {
			result = 1;
		}
		// Rekurrensregel
		else {
			result = n * factorial(n - 1);
		}
		return result;
	}

}

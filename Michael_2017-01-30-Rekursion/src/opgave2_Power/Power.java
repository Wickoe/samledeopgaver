package opgave2_Power;

public class Power {

    public static void main(String[] args) {
        int tal1 = 4;
        int tal2 = 3;

        System.out.println(power(tal1, tal2));
        System.out.println(power2(tal1, tal2));
    }

    public static int power(int n, int p) {
        int result = 0;
        if (p == 0) {
            result = 1;
        }
        else {
            result = power(n, p - 1) * n;
        }
        return result;
    }

    public static int power2(int n, int p) {
        int result = 0;
        if (p == 0) {
            result = 1;
        }
        else if (p % 2 == 1) {
            result = power2(n, p - 1) * n;
        }
        else {
            result = power2(n * n, p / 2);
        }

        return result;
    }

}

package pigGames;

import java.util.Scanner;

public class PigGame {
	private Die die;
	private int upperLimit;
	private boolean gameOver;
	private String playerTurn;
	private boolean playerOneTurn;
	private Scanner in;

	private String playerOne;
	private int playerOnePoints;

	private String playerTwo;
	private int playerTwoPoints;

	public PigGame() {
		die = new Die();
		gameOver = false;
		playerOneTurn = true;
		upperLimit = -1;
	}

	public void WelcomeToGame() {
		System.out.println("Welcome to a game of Pig");
		System.out.println("The firs player to reach " + upperLimit + " wins the game");
		System.out.println("...");
	}

	public void gameOver() {
		String result = "Player ";

		if (playerOnePoints >= upperLimit) {
			result += playerOne + " wins with " + playerOnePoints + " points";
		} else {
			result += playerTwo + " wins with " + playerTwoPoints + " points";
		}

		System.out.println(result);
		gameOver = true;
	}

	public void takeTurn() {
		System.out.println("Player " + playerTurn + " do you want to roll the die? Type 'y' or 'n': ");
		boolean canContinue = true;
		String anwser = in.next();
		int tempPoints = 0;

		while (anwser.toLowerCase().equals("y") && canContinue) {
			die.roll();
			if (die.getFaceValue() == 1) {
				tempPoints = 0;
				canContinue = false;
			} else {
				System.out.println(playerTurn + " rolled " + die.getFaceValue());
				tempPoints += die.getFaceValue();
				System.out.println("Player " + playerTurn + " do you want to roll the die? Type 'y' or 'n': ");
				anwser = in.next();
			}
		}

		if (playerOneTurn) {
			playerOnePoints += tempPoints;
			playerTurn = playerTwo;
			if (playerOnePoints >= upperLimit) {
				gameOver();
			}
		} else {
			playerTwoPoints += tempPoints;
			playerTurn = playerOne;
			if (playerTwoPoints >= upperLimit) {
				gameOver();
			}
		}

		playerOneTurn = !playerOneTurn;
	}

	public void startGame() {
		WelcomeToGame();
		in = new Scanner(System.in);

		System.out.print("Player 1 type in your name ");
		playerOne = in.next();
		playerTurn = playerOne;
		System.out.print("Player 2 type in your name ");
		playerTwo = in.next();

		System.out.println("What is the upper limit for the game? Type in an integer");
		upperLimit = in.nextInt();

		while (!gameOver) {
			takeTurn();
		}
	}
}

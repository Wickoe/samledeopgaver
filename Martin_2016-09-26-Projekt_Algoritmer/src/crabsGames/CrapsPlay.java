package crabsGames;

import java.util.Scanner;

public class CrapsPlay {
	private Die die1;
	private Die die2;

	private int point;
	private boolean gameWon;
	private boolean gameOver;

	private Scanner in;

	public CrapsPlay() {
		die1 = new Die();
		die2 = new Die();
		point = -1;
		gameWon = false;
		gameOver = false;
	}

	public void welcomeToGame() {
		System.out.println("Welcome to a game of Craps");
		System.out.println("Rules for a game of Craps are as follows");
		System.out.println("First time the dices are rolled is called 'Come out roll'");
		System.out.println("...");
	}

	public void gameOver() {
		System.out.println("Did player win: " + gameWon);
		gameOver = true;
	}

	public void takeTurn() {
		die1.roll();
		die2.roll();

		int faceValue = die1.getFaceValue() + die2.getFaceValue();

		if (point < 2) {
			System.out.println("Come out roll: " + faceValue);

			gameWon = faceValue == 7;
			point = faceValue;

			if (faceValue == 2 || faceValue == 3 || faceValue == 12 || faceValue == 7) {
				gameOver();
			}
		} else {
			System.out.println("Rolled: " + faceValue);
			gameWon = faceValue == point;
			if (faceValue == 7 || gameWon) {
				gameOver();
			}
		}
	}

	public void startGame() {
		in = new Scanner(System.in);

		welcomeToGame();
		System.out.print("Do you want to roll the dices. Type 'y' or 'n': ");
		String anwser = in.next();
		boolean gameContinue = anwser.toLowerCase().equals("y");

		while (gameContinue && !gameOver) {
			takeTurn();
			if (!gameOver) {
				System.out.print("Do you want to roll the dices. Type 'y' or 'n': ");
				anwser = in.next();
				gameContinue = anwser.toLowerCase().equals("y");
			}
		}

		in.close();
	}
}

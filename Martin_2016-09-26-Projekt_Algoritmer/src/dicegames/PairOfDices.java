package dicegames;

/**
 * This class models one pair of dices. This is useful for games where you have
 * to throw two dice at once.
 */
public class PairOfDices {
	/**
	 * The first die in the pair.
	 */
	private Die die1;
	/**
	 * The second die in the pair.
	 */
	private Die die2;

	private int timesRolled;

	private int timesRolledPairOfEyes;

	private int timesRolledOnes;
	private int timesRolledTwos;
	private int timesRolledThrees;
	private int timesRolledFours;
	private int timesRolledFives;
	private int timesRolledSixes;

	/**
	 * Constructor for objects of class PairOfDices
	 */
	public PairOfDices() {
		die1 = new Die();
		die2 = new Die();
	}

	public PairOfDices(int sides) {
		die1 = new Die(sides);
		die2 = new Die(sides);
	}

	public void rollBothDices() {
		die1.roll();
		int faceValue = die1.getFaceValue();
		if (faceValue == 6) {
			timesRolledSixes++;
		} else if (faceValue == 5) {
			timesRolledFives++;
		} else if (faceValue == 4) {
			timesRolledFours++;
		} else if (faceValue == 3) {
			timesRolledThrees++;
		} else if (faceValue == 2) {
			timesRolledTwos++;
		} else {
			timesRolledOnes++;
		}
		die2.roll();
		faceValue = die2.getFaceValue();
		if (faceValue == 6) {
			timesRolledSixes++;
		} else if (faceValue == 5) {
			timesRolledFives++;
		} else if (faceValue == 4) {
			timesRolledFours++;
		} else if (faceValue == 3) {
			timesRolledThrees++;
		} else if (faceValue == 2) {
			timesRolledTwos++;
		} else {
			timesRolledOnes++;
		}

		if (die1.getFaceValue() == die2.getFaceValue()) {
			timesRolledPairOfEyes++;
		}
		timesRolled++;
	}

	public int sumOfDices() {
		return die1.getFaceValue() + die2.getFaceValue();
	}

	public int getTimesRolled() {
		return timesRolled;
	}

	public int getTimesRolledSixes() {
		return timesRolledSixes;
	}

	public int getTimesRolledFives() {
		return timesRolledFives;
	}

	public int getTimesRolledFours() {
		return timesRolledFours;
	}

	public int getTimesRolledThrees() {
		return timesRolledThrees;
	}

	public int getTimesRolledTwos() {
		return timesRolledTwos;
	}

	public int getTimesRolledOnes() {
		return timesRolledOnes;
	}

	public int getTimesRolledPairOfEyes() {
		return timesRolledPairOfEyes;
	}

	public void resetPairOfDice() {
		timesRolled = 0;
		timesRolledOnes = 0;
		timesRolledTwos = 0;
		timesRolledThrees = 0;
		timesRolledFours = 0;
		timesRolledFives = 0;
		timesRolledSixes = 0;
		timesRolledPairOfEyes = 0;
	}

	public void rolledValues() {
		System.out.println(die1.getFaceValue());
		System.out.println(die2.getFaceValue());
	}
}

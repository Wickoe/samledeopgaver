package dicegames;

import java.util.Scanner;

public class PlayPairOfDice {
	private Scanner in;
	private PairOfDices pod;

	public static void main(String[] args) {
		PlayPairOfDice ppod = new PlayPairOfDice();
		ppod.startGame();
	}

	public void startGame() {
		pod = new PairOfDices();
		in = new Scanner(System.in);

		System.out.print("Would you like to roll the dice. Type 'y' or 'n': ");
		String anwser = in.next();
		while (anwser.toLowerCase().equals("y")) {
			pod.rollBothDices();
			pod.rolledValues();
			System.out.print("Would you like to roll the dice. Type 'y' or 'n': ");
			anwser = in.next();
		}

		printResult();
		in.close();
	}

	private void printResult() {
		System.out.println("Times rolled dices: " + pod.getTimesRolled());
		System.out.println("Times rolled ones: " + pod.getTimesRolledOnes());
		System.out.println("Times rolled twos: " + pod.getTimesRolledTwos());
		System.out.println("Times rolled threes: " + pod.getTimesRolledThrees());
		System.out.println("Times Rolled fours: " + pod.getTimesRolledFours());
		System.out.println("Times rolled fives: " + pod.getTimesRolledFives());
		System.out.println("Times rolled sixes: " + pod.getTimesRolledSixes());
		System.out.println("Times rolled pair of eyes: " + pod.getTimesRolledPairOfEyes());
	}
}

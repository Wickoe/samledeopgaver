package opgaver.Vare;

import spiritusadapter.Spiritus;

public class SpiritusAdapter extends Vare {
	private Spiritus spiritus;

	public SpiritusAdapter(int pris, String navn, Spiritus spiritus) {
		super(spiritus.getPrisen(), spiritus.getBetgenelse());

		this.spiritus = spiritus;
	}

	@Override
	public double beregnMoms() {
		return spiritus.hentMoms();
	}

	@Override
	public String getNavn() {
		return spiritus.getBetgenelse();
	}

	@Override
	public int getPris() {
		return spiritus.getPrisen();
	}
}

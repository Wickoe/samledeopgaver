package opgave1;

public class QueueDemo {
	public static void main(String[] args) {
		Queue<String> q = new CircularArrayQueue<>();
		q.enqueue("Tom");
		q.enqueue("Diana");
		q.enqueue("Harry");
		q.enqueue("Thomas");
		q.enqueue("Bob");
		q.enqueue("Brian");
		System.out.println(q.getFront());
		System.out.println("isEmpty: " + q.isEmpty() + ", size: " + q.size());
		while (!q.isEmpty()) {
			System.out.println(q.dequeue());
		}
		System.out.println();
		System.out.println("isEmpty: " + q.isEmpty() + ", size: " + q.size());
		System.out.println();
	}
}

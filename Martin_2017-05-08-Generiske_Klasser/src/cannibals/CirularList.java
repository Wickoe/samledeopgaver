package cannibals;

import java.util.Random;

public class CirularList<E> {
	private class Node {
		private Node next;
		private Node previous;
		private E data;
	}

	private Node head, tail, start;
	private int size;
	
	public CirularList() {
		size = 0;
		head = null;
		tail = null;
	}
	
	public void add(E element) {
		Node newNode = new Node();
		newNode.data = element;
		
		if (isEmpty()) {
			head = newNode;
		}
		else {
			tail.next = newNode;
		}

		newNode.previous = tail;
		tail = newNode;
		tail.next = head;
		head.previous = tail;
	}

	public void startRandom() {
		Random r = new Random();

		int limit = r.nextInt(size) + 1;
		int temp = 0;
		
		Node nodeAt = head;
		
		while (nodeAt != null && temp < limit) {
			nodeAt = nodeAt.next;
			temp++;
		}

		start = nodeAt;
	}
	
	public void startAt(int startAt) {
		Node nodeAt = head;
		int tempCounter = 0;
		
		while (head != null && tempCounter < startAt) {
			nodeAt = nodeAt.next;
			tempCounter++;
		}

		start = nodeAt;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public String listFromStart() {
		String toStringFromStart = "[" + start.data.toString();

		Node nodeAt = start.next;

		while (nodeAt != null && nodeAt != start) {
			if (nodeAt.next != start) {
				toStringFromStart += ", ";
			}
			
			toStringFromStart += nodeAt.data.toString();
			
			nodeAt = nodeAt.next;
		}

		return toStringFromStart;
	}
	
	public void eat(int pladsFraStart) {
		int tempCounter = 0;
		Node nodeAt = start;

		while (tempCounter < pladsFraStart) {
			nodeAt = nodeAt.next;
			tempCounter++;
		}
		
	}
	
	public E remove(E element) {
		Node nodeAt = start;
		int tempStopper = 0;
		boolean found = false;

		while (nodeAt != null && tempStopper < size && !found) {
			if (nodeAt.data.equals(element)) {
				found = !found;
			}
			else {
				nodeAt = nodeAt.next;
			}
			
			tempStopper++;
		}
		
		E removed = null;

		if (found) {
			removed = nodeAt.data;
			nodeAt.previous.next = nodeAt.next;
			nodeAt.next.previous = nodeAt.previous;
		}

		return removed;
	}
}

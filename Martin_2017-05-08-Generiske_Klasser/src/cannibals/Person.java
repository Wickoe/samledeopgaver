package cannibals;

public class Person {
	private String name, sound;
	
	public Person(String aName, String aSound) {
		name = aName;
		sound = aSound;
	}

	@Override
	public String toString() {
		return name + ": " + sound;
	}
}

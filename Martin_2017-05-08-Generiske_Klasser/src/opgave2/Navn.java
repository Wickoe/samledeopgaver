package opgave2;

public class Navn implements Comparable<Navn> {
	private String fornavn;
	private String efternavn;
	
	public Navn(String fornavn, String efternavn) {
		this.fornavn = fornavn;
		this.efternavn = efternavn;
	}
	
	public String getFornavn() {
		return fornavn;
	}
	
	public void setFornavn(String fornavn) {
		this.fornavn = fornavn;
	}
	
	public String getEfternavn() {
		return efternavn;
	}
	
	public void setEfternavn(String efternavn) {
		this.efternavn = efternavn;
	}

	@Override
	public String toString() {
		return fornavn + " " + efternavn;
	}
	
	@Override
	public int compareTo(Navn o) {
		int compareStatus = fornavn.compareTo(o.getFornavn());
		if (compareStatus == 0) {
			compareStatus = efternavn.compareTo(o.getEfternavn());
		}
		return compareStatus;
	}
}

package opgave2;

public class Person<T extends Comparable<T>> implements Comparable<Person<T>> {
	private T navn;

	public Person(T etNavn) {
		navn = etNavn;
	}

	public void setNavn(T etNavn) {
		navn = etNavn;
	}

	public T getNavn() {
		return navn;
	}
	
	@Override
	public String toString() {
		return navn.toString();
	}

	@Override
	public int compareTo(Person<T> o) {
		return navn.compareTo(o.getNavn());
	}
}

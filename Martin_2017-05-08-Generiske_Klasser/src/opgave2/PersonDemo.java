package opgave2;

import java.util.ArrayList;
import java.util.Collections;

public class PersonDemo {
	public static void main(String[] args) {
		Person<String> stringPerson = new Person<>("Martin Hansen");

		System.out.println(stringPerson);
		
		Person<Navn> navnPerson = new Person<>(new Navn("Martin", "Hansen"));
		System.out.println(navnPerson);

		System.out.println();

		ArrayList<Person<Navn>> personList = new ArrayList<>();

		personList.add(new Person<>(new Navn("Findus", "Kat")));
		personList.add(new Person<>(new Navn("Pedersen", "Peder")));
		personList.add(new Person<>(new Navn("Kaj", "Frø")));
		personList.add(new Person<>(new Navn("Andrea", "Fugl")));
		personList.add(new Person<>(new Navn("Æsel", "Plys")));

		System.out.println(personList);
		Collections.sort(personList);
		System.out.println(personList);
	}
}

package opgave3;

public class LinkedBag<E> implements Bag<E> {
	private Node first, last;
	private int currentSize, maxSize;

	private class Node {
		private Node next, previous;
		private E data;
	}

	public LinkedBag() {
		this(10);
	}

	public LinkedBag(int maxSize) {
		this.maxSize = maxSize;
		currentSize = 0;
	}
	
	@Override
	public int getCurrentSize() {
		return currentSize;
	}
	
	@Override
	public boolean isFull() {
		return currentSize == maxSize;
	}
	
	@Override
	public boolean isEmpty() {
		return currentSize == 0;
	}
	
	@Override
	public boolean add(E newEntry) {
		Node newNode = new Node();
		newNode.data = newEntry;
		boolean isFull = isFull();
		
		if (!isFull) {
			if (isEmpty()) {
				first = newNode;
			}
			else {
				last.next = newNode;
				newNode.previous = last;
			}

			last = newNode;
			currentSize++;
		}
		
		return !isFull;
	}
	
	@Override
	public E remove() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean remove(E anEntry) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void clear() {
		first = null;
		last = null;
		currentSize = 0;
	}
	
	@Override
	public int getFrequencyOf(E anEntry) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public boolean contains(E anEntry) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public E[] toArray() {
		E[] array = (E[]) new Object[currentSize];
		return array;
	}
}

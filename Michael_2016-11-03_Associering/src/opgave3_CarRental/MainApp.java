package opgave3_CarRental;

public class MainApp {

    public static void main(String[] args) {
        Car car1 = new Car("asdasd", 1990);
        Car car2 = new Car("asdgfhsd", 2005);
        Car car3 = new Car("asdgfhasd", 2004);
        Car car4 = new Car("asdsdsfdsfasd", 2018);
        Car car5 = new Car("gfhfghfgh", 2017);

        Rental rental1 = new Rental(1, 10, "20/1");
        Rental rental2 = new Rental(2, 20, "25/1");

        rental1.addCars(car1);
        rental1.addCars(car2);
        rental1.addCars(car3);
        rental2.addCars(car4);
        rental2.addCars(car5);
        
        System.out.println(rental2.getPrice());
    }

}

package opgave3_CarRental;

import java.util.ArrayList;

public class Rental {
    private int number;
    private int days;
    private String date;
    private ArrayList<Car> cars;

    public Rental(int number, int days, String date) {
        this.number = number;
        this.days = days;
        this.date = date;
        this.cars = new ArrayList<>();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public ArrayList<Car> getCars() {
        return new ArrayList<>(cars);
    }

    public void addCars(Car car) {
        cars.add(car);
    }
    
    public void removeCars(Car car) {
        cars.remove(car);
    }

    public double getPrice() {
        double pris = 0;

        for (Car c : cars) {
            pris += c.getPricePerDay();
        }
        pris *= days;

        return pris;
    }
}

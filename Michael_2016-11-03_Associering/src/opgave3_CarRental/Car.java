package opgave3_CarRental;

public class Car {
    private String License;
    private double pricePerDay;
    private int purchaseYear;
    
    public Car(String license, int purchaseYear) {
        this.License = license;
        this.pricePerDay = 50.0;
        this.purchaseYear = purchaseYear;
    }

    public String getLicense() {
        return License;
    }

    public void setLicense(String license) {
        License = license;
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public int getPurchaseYear() {
        return purchaseYear;
    }

    public void setPurchaseYear(int purchaseYear) {
        this.purchaseYear = purchaseYear;
    }

}

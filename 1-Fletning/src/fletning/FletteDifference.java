package fletning;

import java.util.ArrayList;
import java.util.List;

public class FletteDifference<E extends Comparable<E>> {
	/*
	 * Worst-case time: O(n + m)
	 */
	@SuppressWarnings("unchecked")
	public List<E> goodCustomers(List<E> l1, E[] l2) {
		List<E> fletteDifference = (List<E>) new ArrayList<>();

		int leftIndexAt = 0;
		int rightIndexAt = 0;

		while (leftIndexAt < l1.size() && rightIndexAt < l2.length) {
			E leftTemp = l1.get(leftIndexAt);
			E rightTemp = l2[rightIndexAt];

			int compareStatus = leftTemp.compareTo(rightTemp);

			if (compareStatus < 0) {
				fletteDifference.add(leftTemp);
				leftIndexAt++;
			} else if (compareStatus > 0) {
				rightIndexAt++;
			} else {
				leftIndexAt++;
				rightIndexAt++;
			}
		}

		while (leftIndexAt < l1.size()) {
			fletteDifference.add(l1.get(leftIndexAt));
			leftIndexAt++;
		}

		return fletteDifference;
	}
}

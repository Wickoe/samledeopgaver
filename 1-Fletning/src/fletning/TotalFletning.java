package fletning;

import java.util.ArrayList;
import java.util.List;

public class TotalFletning<E extends Comparable<E>> {
	/*
	 * Worst-case time: O(n+m)
	 */
	@SuppressWarnings("unchecked")
	public List<E> fletAlleFra(List<E> l1, List<E> l2) {
		List<E> flettetList = (List<E>) new ArrayList<Object>();
		int leftIndex = 0;
		int rightIndex = 0;

		while (leftIndex < l1.size() && rightIndex < l2.size()) {
			if (l1.get(leftIndex).compareTo(l2.get(rightIndex)) < 0) {
				flettetList.add(l1.get(leftIndex));
				leftIndex++;
			} else if (l1.get(leftIndex).compareTo(l2.get(rightIndex)) > 0) {
				flettetList.add(l2.get(rightIndex));
				rightIndex++;
			} else {
				flettetList.add(l1.get(leftIndex));
				leftIndex++;

				flettetList.add(l2.get(rightIndex));
				rightIndex++;
			}
		}

		while (leftIndex < l1.size()) {
			flettetList.add(l1.get(leftIndex));
			leftIndex++;
		}

		while (rightIndex < l2.size()) {
			flettetList.add(l2.get(rightIndex));
			rightIndex++;
		}

		return flettetList;
	}
}

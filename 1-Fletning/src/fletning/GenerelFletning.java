package fletning;

import java.util.ArrayList;
import java.util.List;

public class GenerelFletning<E extends Comparable<E>> {
	/**
	 * Returnerer et sorteret array der indeholder alle elementer l1 og l2 har
	 * til fælles. Krav: l1 og l2 er sorterede, indeholder ikke dubletter og
	 * indeholder ikke tomme pladser
	 */
	/*
	 * Worst-case time: O(n)
	 */
	@SuppressWarnings("unchecked")
	public List<E> fællesTal(E[] l1, E[] l2) {
		List<E> foreningsMaengden = (List<E>) new ArrayList<Object>();
		int leftIndex = 0;
		int rightIndex = 0;

		while (leftIndex < l1.length && rightIndex < l2.length) {
			E left = l1[leftIndex];
			E right = l2[rightIndex];
			int compareStatus = left.compareTo(right);

			if (compareStatus < 0) {
				leftIndex++;
			} else if (compareStatus > 0) {
				rightIndex++;
			} else {
				foreningsMaengden.add(left);
				leftIndex++;
				rightIndex++;
			}
		}

		return foreningsMaengden;
	}
}

package tests;

import java.util.ArrayList;

import fletning.TotalFletning;

public class TotalFletTestApp {
	public static void main(String[] args) {
		ArrayList<Integer> l1 = new ArrayList<>();
		l1.add(2);
		l1.add(4);
		l1.add(6);
		l1.add(8);
		l1.add(10);
		l1.add(12);
		l1.add(14);

		ArrayList<Integer> l2 = new ArrayList<>();
		l2.add(1);
		l2.add(2);
		l2.add(4);
		l2.add(5);
		l2.add(6);
		l2.add(9);
		l2.add(12);
		l2.add(17);

		TotalFletning<Integer> totalFletTest = new TotalFletning<>();

		System.out.println(l1);
		System.out.println(l2);
		System.out.println(totalFletTest.fletAlleFra(l1, l2));
	}
}

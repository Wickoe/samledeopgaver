package tests;

import fletning.GenerelFletning;

public class GenerelFletningTestApp {
	public static void main(String[] args) {
		Integer[] l1 = { 2, 4, 6, 8, 10, 12, 14 };
		Integer[] l2 = { 1, 2, 4, 5, 6, 9, 12, 17 };

		GenerelFletning<Integer> generelFletning = new GenerelFletning<>();

		System.out.println(generelFletning.fællesTal(l1, l2));
	}
}

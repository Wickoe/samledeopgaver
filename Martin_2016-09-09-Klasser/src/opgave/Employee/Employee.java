package opgave.Employee;

/**
 * Klasse der beskriver en ansat.
 */
public class Employee {
	/**
	 * Attributter der beskriver den ansattes tilstand
	 */
	private String fornavn;
	private String efternavn;
	private String mellemNavne;
	private boolean trainee;
	private int age;

	/**
	 * Constructor, når den ansatte oprettes, skal den have et navn. Ved
	 * oprettelse er den ansatte en trainee
	 */
	public Employee(String aFirstName, String aMiddleName, String aLastName, int anAge) {
		this.fornavn = aFirstName;
		this.mellemNavne = aMiddleName;
		this.efternavn = aLastName;
		this.trainee = true;
		age = anAge;
	}

	/**
	 * Den ansattes navn kan ændres ved kald af setName metoden
	 */
	public void setName(String inputName) {
		this.fornavn = inputName;
	}

	/**
	 * Man kan få oplyst den ansattes navn, ved at kalde metoden getName
	 */
	public String getName() {
		return this.fornavn;
	}

	/**
	 * Den ansatte kan få ændret trainee status ved at kalde metoden setTrainess
	 */
	public void setTrainee(boolean trainee) {
		this.trainee = trainee;
	}

	/**
	 * Man kan få oplyst den ansatte er trainess aktivitet, ved at kalde metoden
	 * isTrainee
	 */
	public boolean isTrainee() {
		return this.trainee;
	}

	@Override
	public String toString() {
		return this.fornavn + " is trainee: " + this.trainee;
	}

	public void printEmployee() {
		System.out.println("*******************");
		System.out.println("* Name " + this.fornavn);
		System.out.println("* Mellemnavn " + this.mellemNavne);
		System.out.println("* Efternavn " + this.efternavn);
		System.out.println("* Trainee " + this.trainee);
		System.out.println("* Alder " + this.age);
		System.out.println("*******************");
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int anAge) {
		age = anAge;
	}
	
	public void increaseAgeByOne() {
		age++;
	}

	public String getEfternavn() {
		return efternavn;
	}

	public void setEfternavn(String efternavn) {
		this.efternavn = efternavn;
	}

	public String getMellemNavne() {
		return mellemNavne;
	}

	public void setMellemNavne(String mellemNavne) {
		this.mellemNavne = mellemNavne;
	}
}

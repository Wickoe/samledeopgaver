package opgave.Employee;

public class TestApp {

    public static void main(String[] args) {
        Employee emp = new Employee("Kristian", "", "Dorland", 20);
        System.out.println(emp.getName());
        
        emp.setName("Christian");
        System.out.println(emp.getName());
        
        System.out.println(emp.getAge());
        emp.setAge(24);
        System.out.println(emp.getAge());
        emp.increaseAgeByOne();
        System.out.println(emp.getAge());
        System.out.println();
        emp.printEmployee();
    }

}

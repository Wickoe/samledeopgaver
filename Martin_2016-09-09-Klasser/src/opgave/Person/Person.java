package opgave.Person;

public class Person {
	private String navn;
	private String address;
	private int monthlySalary;
	private int cvVirksomheder;
	
	public Person(String etNavn, String enAddress, int aMonthlySalary) {
		navn = etNavn;
		address = enAddress;
		monthlySalary = aMonthlySalary;
	}
	
	public void setNavn(String etNavn) {
		navn = etNavn;
	}
	
	public String getNavn() {
		return navn;
	}
	
	public void setAddress(String enAddress) {
		address = enAddress;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setMonthlySalary(int aMonthlySalary) {
		monthlySalary = aMonthlySalary;
	}
	
	public int getMonthlySalary() {
		return monthlySalary;
	}
	
	public void printPerson() {
		System.out.println("Navn: " + navn);
		System.out.println("Address: " + address);
		System.out.println("Monthlysalary: " + monthlySalary);
	}
	
	public double yearlySalary() {
		return (monthlySalary * 12) * 1.025;
	}
	
	public void addVirksomhed() {
		cvVirksomheder++;
	}
	
	public int getVirksomheder() {
		return cvVirksomheder;
	}
}

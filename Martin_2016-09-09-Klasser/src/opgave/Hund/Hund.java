package opgave.Hund;

//public Hund class {
//Klasse deklarationen skal være
public class Hund {
	// private string navn;
	// Der findes ingen simple typer kaldet string
	// Der findes den simple type char og warper klassen String (med stort 'S')
	private String navn;
	
	// private Int nummer;
	// Der findes ingen wrapper klasse kaldet 'Int'
	// Der findes en simpel klasse kaldet 'int'
	private int nummer;
	
	// public Hund(String n; int nr) {
	//		navn = n;
	//		nummer = nr;
	// }
	// Der anvendes en forkert syntaks i forhold til parameterne angivelsen
	// Parameterne for en metode og constructors sepereres ved brug af kolon (',')
	public Hund(String n, int nr) {
		navn = n;
		nummer = nr;
	}
	
	// public setNavn(String navn) {
	//		navn = navn;
	// }
	// Der er anvendt en dårlig navngivelse for parameterne samt manglede returtype.
	// Parameterens navn og en global variabel er kaldt det samme.
	// Der skal anvendes key-worded 'this.(global-variabel)' 
	// for at det virker efter hensigten
	public void setNavn(String navn) {
		this.navn = navn;
	}
	
	// public void getNavn() {
	// 		return Navn;
	// }
	// Der anvendes en forkert returtype samt et forkert global variabel navn.
	public String getNavn() {
		return navn;
	}
	 
	// public int getNummer {
	// 		return this.nummer;
	// }
	// Mangler at angive parameter-paranteser.
	public int getNummer() {
		return this.nummer;
	}
//}
}
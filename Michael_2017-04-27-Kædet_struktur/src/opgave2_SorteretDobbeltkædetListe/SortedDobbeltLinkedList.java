package opgave2_SorteretDobbeltkædetListe;

public class SortedDobbeltLinkedList {
    private class DNode {
        public String data;
        public DNode next;
        public DNode previous;
    }
    
    private DNode first;
    private DNode last;
    
    public SortedDobbeltLinkedList() {
        first = new DNode();
        last = new DNode();
        first.next = last;
        last.previous = first;
    }
    
    /**
    * Tilføjer et element til listen, så listen fortsat er sorteret i
    * henhold til den naturlige ordning på elementerne.
    * @param element det der indsættes
    */
    public void addElement(String element) {
        DNode node = new DNode();
        node.data = element;
        
        DNode temp = first.next;
        
        boolean found = false;
        
        while (!found && temp != last) {
            if (temp.data.compareTo(element) >= 0) {
                found = true;
            }
            else {
                temp = temp.next;
            }
        }

        temp = temp.previous;

        node.next = temp.next;
        node.previous = temp;
        temp.next = node;
        node.next.previous = node;
        
    }
    
    @Override
    public String toString() {
        DNode node = first;
        String result = "";
        while (node != null) {
            result += node.data + " ";
            node = node.next;
        }
        return result.trim();
    }
}

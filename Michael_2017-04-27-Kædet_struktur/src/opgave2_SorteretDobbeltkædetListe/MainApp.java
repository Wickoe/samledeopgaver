package opgave2_SorteretDobbeltkædetListe;

public class MainApp {
    
    public static void main(String[] args) {
        SortedDobbeltLinkedList liste = new SortedDobbeltLinkedList();
        
        liste.addElement("d");
        liste.addElement("a");
        liste.addElement("c");
        liste.addElement("b");
        liste.addElement("f");
        
        System.out.println(liste);
    }
    
}

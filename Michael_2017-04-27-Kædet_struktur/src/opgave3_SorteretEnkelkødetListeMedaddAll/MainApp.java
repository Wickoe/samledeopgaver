package opgave3_SorteretEnkelkødetListeMedaddAll;

public class MainApp {

    public static void main(String[] args) {
        SortedLinkedList liste = new SortedLinkedList();

        liste.addElement("d");
        liste.addElement("a");
        liste.addElement("c");
        liste.addElement("b");
        liste.addElement("f");
        
        SortedLinkedList liste2 = new SortedLinkedList();

        liste2.addElement("j");
        liste2.addElement("k");
        liste2.addElement("h");
        liste2.addElement("i");
        liste2.addElement("l");

        System.out.println(liste);
        System.out.println(liste.countElements());
        
        liste.removeElement("d");
        liste.removeElement("a");

        System.out.println(liste);
        System.out.println(liste.countElements());
        
        System.out.println();

        System.out.println(liste);
        //liste.addAll(liste2);
        System.out.println(liste);
    }

}

package opgave3_SorteretEnkelkødetListeMedaddAll;

public class SortedLinkedList {
    private class Node {
        public String data;
        public Node next;

    }
    
    private Node first;

    public SortedLinkedList() {
    }
    
    /**
    * Tilføjer et element til listen, så listen fortsat er sorteret i
    * henhold til den naturlige ordning på elementerne.
    * @param element det der indsættes
    */
    public void addElement(String element) {
        Node node = new Node();
        node.data = element;

        Node at = first;
        Node pre = null;

        boolean found = false;

        while (!found) {
            if (at == null) {
                first = node;
                found = true;
            }
            else {
                if (node.data.compareToIgnoreCase(at.data) > 0 && at.next == null) {
                    at.next = node;
                    found = true;
                }
                else if (node.data.compareToIgnoreCase(at.data) <= 0) {
                    if (at == first) {
                        node.next = at;
                        first = node;
                    }
                    else {
                        node.next = at;
                        pre.next = node;
                    }
                    found = true;
                }
                else {
                    pre = at;
                    at = at.next;
                }
            }
        }
    }

    /**
    * Fjerner et element fra listen.
    * @param element det element der fjernes
    * @return true hvis elementet blev fjernet, men ellers false.
    */
    public boolean removeElement(String element) {
        Node node = first;
        Node pre = null;
        boolean found = false;

        while (!found && node != null) {
            if (node.data.equals(element)) {
                if (node == first) {
                    first = first.next;
                }
                else {
                    pre.next = node.next;
                }
                found = true;
            }
            pre = node;
            node = node.next;
        }
        return found;
    }
    
    /**
    * Beregner antallet af elementer i listen.
    */
    public int countElements() {
        int count = 0;
        Node node = first;

        while (node != null) {
            count++;
            node = node.next;
        }
        return count;
    }
    
    @Override
    public String toString() {
        Node node = first;
        String result = "";
        while (node != null) {
            result += node.data + " ";
            node = node.next;
        }
        return result.trim();
    }
    
    /**
    * Tilføjer alle elementerne fra list til den aktuelle liste.
    * Listen er fortsat sorteret i henhold til den naturlige ordning på
    * elementerne.
    */
    public void addAll(SortedLinkedList list) {
        //mangler
    }
    
}

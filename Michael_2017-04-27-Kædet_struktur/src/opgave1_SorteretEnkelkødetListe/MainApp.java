package opgave1_SorteretEnkelkødetListe;

public class MainApp {

    public static void main(String[] args) {
        SortedLinkedList liste = new SortedLinkedList();

        liste.addElement("d");
        liste.addElement("a");
        liste.addElement("c");
        liste.addElement("b");
        liste.addElement("f");

        System.out.println(liste);
        System.out.println(liste.countElements());
        
        liste.removeElement("d");
        liste.removeElement("a");

        System.out.println(liste);
        System.out.println(liste.countElements());
    }

}

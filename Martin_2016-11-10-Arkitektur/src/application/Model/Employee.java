package application.Model;

public class Employee {
	private String name;
	private int wage; // hourly wage
	private int employmentYear;
	private Company company;

	public Employee(String name, int wage) {
		this.name = name;
		this.wage = wage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWage() {
		return wage;
	}

	public void setWage(int wage) {
		this.wage = wage;
	}

	@Override
	public String toString() {
		return name + " (kr " + wage + ")";
	}

	public Company getCompany() {
		return company;
	}

	/*
	 * Jeg har komboneret set metoden for company og employmentYear i samme
	 * metode.
	 */
	public void setCompany(Company company, int emplooymentYear) {
		Company oldCompany = this.company;
		this.company = company;
		if (oldCompany != null) {
			oldCompany.removeEmployee(this);
		}
		if (company != null) {
			this.employmentYear = emplooymentYear;
			if (!company.getEmployees().contains(this)) {
				company.addEmployee(this, emplooymentYear);
			}
		}
	}

	public int getEmploymentYear() {
		return employmentYear;
	}

	/**
	 * Returns the weekly salary of this employee.
	 */
	public int weeklySalary() {
		int salary = wage * company.getHours();
		return salary;
	}
}

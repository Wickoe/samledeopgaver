package hashing;

public interface HashSet {
	public boolean add(Object x);
	
	public boolean remove(Object x);
	
	public boolean contains(Object x);
	
	public int size();
}

package hashing;

/**
 * This class implements a hash set using an array and linear probing to handle collisions.
 */
public class HashSetLinearProbing implements HashSet {
	private Object[] buckets;
	private int currentSize;
	
	private enum State {
		DELETED
	}
	
	/**
	 * Constructs a hash table.
	 * @param bucketsLength the length of the buckets array
	 */
	public HashSetLinearProbing(int bucketsLength) {
		buckets = new Object[bucketsLength];
		currentSize = 0;
	}
	
	/**
	 * Tests for set membership.
	 * @param x an object
	 * @return true if x is an element of this set
	 */
	@Override
	public boolean contains(Object x) {
		int hashValue = hashValue(x);
		boolean contains = buckets[hashValue].equals(x);
		
		while (!contains && buckets[hashValue] != null) {
			contains = buckets[hashValue].equals(x);

			hashValue = (hashValue + 1) % buckets.length;
		}
		
		return contains;
	}
	
	/**
	 * Adds an element to this set.
	 * @param x an object
	 * @return true if x is a new object, false if x was already in the set
	 */
	@Override
	public boolean add(Object x) {
		boolean contains = contains(x);
		int hashValue = hashValue(x);
		Object currObj = buckets[hashValue];

		while (!contains && currObj != null && currObj != State.DELETED) {
			hashValue = (hashValue + 1) % buckets.length;
			
			currObj = buckets[hashValue];
		}
		
		if (!contains) {
			buckets[hashValue] = x;
			currentSize++;
		}

		return !contains;
	}
	
	/**
	 * Removes an object from this set.
	 * @param x an object
	 * @return true if x was removed from this set, false if x was not an element of this set
	 */
	@Override
	public boolean remove(Object x) {
		boolean contains = false;
		int hashValue = hashValue(x);
		Object currObj = buckets[hashValue];
		
		while (!contains && currObj != null) {
			if (currObj.equals(x)) {
				contains = true;
			}
			else {
				hashValue = (hashValue + 1) % buckets.length;
				currObj = buckets[hashValue];
			}
		}
		
		if (contains) {
			buckets[hashValue] = State.DELETED;
			currentSize--;
		}
		
		return contains;
	}
	
	/**
	 * Gets the number of elements in this set.
	 * @return the number of elements
	 */
	@Override
	public int size() {
		return currentSize;
	}
	
	private int hashValue(Object x) {
		int h = x.hashCode();
		if (h < 0) {
			h = -h;
		}
		h = h % buckets.length;
		return h;
	}
	
	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < buckets.length; i++) {
			int value = buckets[i] != null && !buckets[i].equals(State.DELETED)
				? hashValue(buckets[i]) : -1;
			result += i + "\t" + buckets[i] + "(h:" + value + ")\n";
		}
		return result;
	}
	
}

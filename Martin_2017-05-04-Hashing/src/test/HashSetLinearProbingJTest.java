package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import hashing.HashSetLinearProbing;

public class HashSetLinearProbingJTest {
	@Test
	public void testAddLinearProbing() {
		HashSetLinearProbing names = new HashSetLinearProbing(13);
		names.add("Harry");
		names.add("Sue");
		names.add("Nina");
		names.add("Susannah");
		names.add("Larry");
		names.add("Eve");
		names.add("Sarah");
		names.add("Adam");
		names.add("Tony");
		names.add("Katherine");
		names.add("Juliet");
		names.add("Romeo");
		assertEquals(12, names.size());
	}

	@Test
	public void testRemoveLinearProbing() {
		HashSetLinearProbing names = new HashSetLinearProbing(13);
		names.add("Harry");
		names.add("Sue");
		names.add("Nina");
		names.add("Susannah");
		names.add("Larry");
		names.remove("Larry");
		names.remove("Nina");
		assertEquals(3, names.size());
	}

	@Test
	public void testContainsLinearProbing() {
		HashSetLinearProbing names = new HashSetLinearProbing(13);
		names.add("Harry");
		names.add("Sue");
		names.add("Nina");
		names.add("Susannah");
		names.add("Larry");
		names.add("Eve");
		names.add("Sarah");
		names.add("Adam");
		names.add("Tony");
		names.add("Katherine");
		names.add("Juliet");
		names.add("Romeo");
		assertTrue(names.contains("Nina"));
		assertTrue(names.contains("Romeo"));
		assertTrue(names.contains("Sarah"));

		names.remove("Romeo");
		assertFalse(names.contains("Romeo"));
	}
}

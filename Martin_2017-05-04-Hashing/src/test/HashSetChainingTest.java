package test;

import static org.junit.Assert.*;

import org.junit.Test;

import hashing.HashSetChaining;

public class HashSetChainingTest {
    @Test
    public void testAddChaining() {
        HashSetChaining names = new HashSetChaining(13);
        names.add("Harry");
        names.add("Sue");
        names.add("Nina");
        names.add("Susannah");
        names.add("Larry");
        names.add("Eve");
        names.add("Sarah");
        names.add("Adam");
        names.add("Tony");
        names.add("Katherine");
        names.add("Juliet");
        names.add("Romeo");
        assertEquals(12, names.size());
    }
    
    @Test
    public void testRemoveChaining() {
        HashSetChaining names = new HashSetChaining(13);
        names.add("Harry");
        names.add("Sue");
        names.add("Nina");
        names.add("Susannah");
        names.add("Larry");
        names.remove("Larry");
        names.remove("Nina");
        assertEquals(3, names.size());
    }
    
    @Test
    public void testContainsChaining() {
        HashSetChaining names = new HashSetChaining(4);
        names.add("Harry");
        names.add("Sue");
        names.add("Nina");
        names.add("Susannah");
        names.add("Larry");
        names.add("Eve");
        names.add("Sarah");
        names.add("Adam");
        names.add("Tony");
        names.add("Katherine");
        names.add("Juliet");
        names.add("Romeo");
        assertTrue(names.contains("Nina"));
        assertTrue(names.contains("Romeo"));
        assertTrue(names.contains("Sarah"));
        
        names.remove("Romeo");
        assertFalse(names.contains("Romeo"));
    }

}

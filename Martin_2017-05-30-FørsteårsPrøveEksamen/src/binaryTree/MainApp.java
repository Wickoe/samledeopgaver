package binaryTree;

public class MainApp {
	public static void main(String[] args) {
		BinaryTree<String> two = new BinaryTree<>("2");
		BinaryTree<String> four = new BinaryTree<>("4");
		BinaryTree<String> twoPlusFour = new BinaryTree<>("+", two, four);
		BinaryTree<String> seven = new BinaryTree<>("7");
		BinaryTree<String> plusTimesSeven = new BinaryTree<>("*", twoPlusFour, seven);
		BinaryTree<String> three = new BinaryTree<>("3");
		BinaryTree<String> eight = new BinaryTree<>("8");
		BinaryTree<String> threePlusEight = new BinaryTree<>("+", three, eight);
		BinaryTree<String> timesPlusPlus = new BinaryTree<>("+", plusTimesSeven, threePlusEight);
		
		System.out.println("Optrædelser af '+': " + timesPlusPlus.countElements("+"));
		System.out.println("Værdien af træet: " + timesPlusPlus.value());
		System.out.println("Højden på træet: " + timesPlusPlus.height());
	}
}

package binaryTree;

/**
 * A binary tree in which each node has two children.
 */
public class BinaryTree<E> {
	private Node root;
	
	/**
	 * Constructs an empty tree.
	 */
	public BinaryTree() {
		root = null;
	}
	
	/**
	 * Constructs a tree with one node and no children.
	 * @param rootData the data for the root
	 */
	public BinaryTree(E rootData) {
		root = new Node();
		root.data = rootData;
		root.left = null;
		root.right = null;
	}
	
	/**
	 * Constructs a binary tree.
	 * @param rootData the data for the root
	 * @param left the left subtree
	 * @param right the right subtree
	 */
	public BinaryTree(E rootData, BinaryTree<E> left, BinaryTree<E> right) {
		root = new Node();
		root.data = rootData;
		if (left != null) {
			root.left = left.root;
		}
		if (right != null) {
			root.right = right.root;
		}
	}
	
	/**
	 * Checks whether this tree is empty.
	 * @return true if this tree is empty
	 */
	public boolean isEmpty() {
		return root == null;
	}
	
	/**
	 * Gets the data at the root of this tree.
	 * @return the root data
	 */
	public E data() {
		return root.data;
	}
	
	/**
	 * Gets the left subtree of this tree.
	 * @return the left child of the root
	 */
	public BinaryTree<E> left() {
		BinaryTree<E> result = new BinaryTree<>();
		result.root = root.left;
		return result;
	}
	
	/**
	 * Gets the right subtree of this tree.
	 * @return the right child of the root
	 */
	public BinaryTree<E> right() {
		BinaryTree<E> result = new BinaryTree<>();
		result.root = root.right;
		return result;
	}
	
	/**
	 * Replace the root data with some new data
	 * @param newData the new data for the root
	 * @return the data in the root before replacement
	 */
	public E replace(E newData) {
		E oldData = this.root.data;
		this.root.data = newData;
		return oldData;
	}
	
	/**
	 * The size of the tree
	 * @return the number of nodes in the tree
	 */
	public int size() {
		return size(root);
	}
	
	// Private recursive helper method
	private int size(Node n) {
		if (n == null) {
			return 0;
		}
		return 1 + size(n.left) + size(n.right);
	}
	
	private class Node {
		public E data;
		public Node left;
		public Node right;
	}
	
	//-------------------------------------------------------------------
	
	public int countElements(E element) {
		return elementCounter(element, root);
	}

	private int elementCounter(E element, Node nodeAt) {
		int counter = 0;
		
		if (nodeAt != null) {
			if (element.equals(nodeAt.data)) {
				counter++;
			}

			counter += elementCounter(element, nodeAt.left);
			counter += elementCounter(element, nodeAt.right);
		}
		
		return counter;
	}

	private boolean isLeaf(Node aNode) {
		return aNode.left == null && aNode.right == null;
	}

	// husk opgave omkring inorder print
	
	public int value() {
		return value(root);
	}
	
	private int value(Node n) {
		int value = 0;

		if (n != null) {
			if (isLeaf(n)) {
				String data = (String) n.data;
				value = Integer.valueOf(data);
			}
			else {
				int valueLeft = value(n.left);
				int valueRight = value(n.right);

				if (n.data.equals("*")) {
					value = valueLeft * valueRight;
				}
				else if (n.data.equals("+")) {
					value = valueLeft + valueRight;
				}
			}
		}

		return value;
	}
	
	public int height() {
		return height(root);
	}
	
	private int height(Node nodeAt) {
		int height = 0;
		if (nodeAt != null) {
			if (!isLeaf(nodeAt)) {
				height = max(height(nodeAt.left), height(nodeAt.right));
			}
			height++;
		}
		
		return height;
	}
	
	private int max(int first, int second) {
		int max = first;
		if (first < second) {
			max = second;
		}
		
		return max;
	}
}
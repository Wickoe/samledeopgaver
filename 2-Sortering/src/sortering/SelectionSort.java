package sortering;

public class SelectionSort<E extends Comparable<E>> {
	/*
	 * Worst-case time: O(n^2)
	 */
	public void selectionSort(E[] array) {
		for (int i = 0; i < array.length; i++) {
			int minPos = i;

			for (int j = i + 1; j < array.length; j++)
				if (array[j].compareTo(array[minPos]) < 0) {
					minPos = j;
				}

			swap(array, i, minPos);
		}
	}

	public void swap(E[] array, int from, int to) {
		E temp = array[from];

		array[from] = array[to];
		array[to] = temp;
	}
}

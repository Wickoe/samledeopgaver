package sortering;

public class InsertionSort<E extends Comparable<E>> {
	/*
	 * Worst-case time: O(n^2)
	 */
	public void insertionSort(E[] array) {
		for (int i = 1; i < array.length; i++) {
			E next = array[i];

			int j = i;
			boolean found = false;

			while (!found && j > 0) {
				found = next.compareTo(array[j - 1]) > 0;

				if (!found) {
					array[j] = array[j - 1];
					j--;
				}
			}

			array[j] = next;
		}
	}
}

package sortering;

public class Bubblesort<E extends Comparable<E>> {
	/*
	 * Worst-case time: O(n^2)
	 */
	public void bubbleSort(E[] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < i - 1; j++) {
				if (array[j].compareTo(array[j + 1]) > 0) {
					swap(array, j, j + 1);
				}
			}
		}
	}

	private void swap(E[] array, int from, int to) {
		E temp = array[from];

		array[from] = array[to];
		array[to] = temp;
	}
}

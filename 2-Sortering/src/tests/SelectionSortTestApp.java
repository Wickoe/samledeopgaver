package tests;

import sortering.SelectionSort;

public class SelectionSortTestApp {
	public static void main(String[] args) {
		String[] s = { "Erna", "Elly", "Laurits", "Bertha", "Christian", "August", "Marius", "John", "Tove", "Poul",
				"Torkild" };
		SelectionSort<String> stringNavne = new SelectionSort<>();

		for (String name : s) {
			System.out.print(name + " ");
		}

		System.out.println();
		System.out.println();

		stringNavne.selectionSort(s);

		for (String name : s) {
			System.out.print(name + " ");
		}
	}
}

package tests;

import sortering.Bubblesort;

public class BubbleSortTestApp {
	public static void main(String[] args) {
		String[] s = { "Erna", "Elly", "Laurits", "Bertha", "Christian", "August", "Marius", "John", "Tove", "Poul",
				"Torkild" };
		Bubblesort<String> stringNavne = new Bubblesort<>();

		for (String name : s) {
			System.out.print(name + " ");
		}

		System.out.println();
		System.out.println();

		stringNavne.bubbleSort(s);

		for (String name : s) {
			System.out.print(name + " ");
		}
	}
}

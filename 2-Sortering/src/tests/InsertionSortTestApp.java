package tests;

import sortering.InsertionSort;

public class InsertionSortTestApp {
	public static void main(String[] args) {
		String[] s = { "Erna", "Elly", "Laurits", "Bertha", "Christian", "August", "Marius", "John", "Tove", "Poul",
				"Torkild" };
		InsertionSort<String> stringNavne = new InsertionSort<>();

		for (String name : s) {
			System.out.print(name + " ");
		}

		System.out.println();
		System.out.println();

		stringNavne.insertionSort(s);

		for (String name : s) {
			System.out.print(name + " ");
		}
	}
}

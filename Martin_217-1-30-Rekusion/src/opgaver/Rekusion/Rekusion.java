package opgaver.Rekusion;

/**
 * Models a recursion class
 *
 * @author Martin
 *
 */
public class Rekusion {
	/**
	 * Recursively calculate the factorial of n
	 *
	 * @param n
	 *            the integer to calculate the factorial of
	 * @return the recursively calculated factorial of n
	 */
	public static int factorial(int n) {
		if (n == 0) {
			return 1;
		} else {
			return n * factorial(n - 1);
		}
	}

	public static int power(int n, int p) {
		if (p == 0) {
			return 1;
		} else {
			return n * power(n, p - 1);
		}
	}

	public static int power2(int n, int p) {
		if (p == 0) {
			return 1;
		} else if (p % 2 == 1) {
			return n * power2(n, p - 1);
		} else {
			return power2(n * n, p / 2);
		}
	}

	public static int product(int a, int b) {
		if (a == 0) {
			return a;
		} else if (a == 1) {
			return b;
		} else {
			return b + product(a - 1, b);
		}
	}

	public static int productRus(int a, int b) {
		if (a == 0) {
			return a;
		} else if (a >= 1 && a % 2 == 1) {
			return b + productRus(a - 1, b);
		} else {
			return productRus(a / 2, b * 2);
		}
	}

	public static String reverse(String s) {
		int left = 0;
		int right = s.length() - 1;

		if (left < right) {
			return s.charAt(right) + reverse(s.substring(left + 1, right)) + s.charAt(left);
		} else {
			return s;
		}
	}
}

package opgaver.PersonAppSeperatWindow;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class PersonApp extends Application {
	private ListView<Person> lvwPersons;
	private WindowPerson wp;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Person administration");
		wp = new WindowPerson("Person information", stage);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblPersons = new Label("Persons");
		pane.add(lblPersons, 0, 0);

		lvwPersons = new ListView<>();
		pane.add(lvwPersons, 0, 1);

		Button btnAddPerson = new Button("Add person");
		btnAddPerson.setOnAction(event -> addPersonAction());
		pane.add(btnAddPerson, 1, 1);
	}

	private void addPersonAction() {
		Person person = wp.createPerson();

		if (person != null) {
			lvwPersons.getItems().add(person);
		}
	}
}

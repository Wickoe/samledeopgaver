package opgaver.PersonAppSeperatWindow;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WindowPerson extends Stage {
	private TextField txfName, txfTitle;
	private CheckBox cboxSenior;
	private Person person;

	public WindowPerson(String title, Stage owner) {
		initOwner(owner);
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblName = new Label("Name");
		pane.add(lblName, 0, 0);

		txfName = new TextField();
		pane.add(txfName, 1, 0);

		Label lblTitle = new Label("Title");
		pane.add(lblTitle, 0, 1);

		txfTitle = new TextField();
		pane.add(txfTitle, 1, 1);

		cboxSenior = new CheckBox("Senior");
		pane.add(cboxSenior, 0, 3, 2, 1);

		Button btnOk = new Button("Ok");
		btnOk.setOnAction(event -> okAction());
		pane.add(btnOk, 0, 4);

		Button btnCancel = new Button("Cancel");
		btnCancel.setOnAction(event -> cancelAction());
		pane.add(btnCancel, 1, 4);

		person = null;
	}

	private void okAction() {
		String name = txfName.getText().trim();
		String title = txfTitle.getText().trim();
		boolean isSenior = cboxSenior.isSelected();

		if (name.length() > 0 && title.length() > 0) {
			person = new Person(name, title, isSenior);
			clearFields();
			close();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Missing information");
			alert.setHeaderText("Missing information");
			alert.setContentText("Be sure to type in a name, a title and if the person is a senior");
			alert.showAndWait();
		}
	}

	private void cancelAction() {
		clearFields();
		close();
	}

	private void clearFields() {
		txfName.clear();
		txfTitle.clear();
		cboxSenior.setSelected(false);
	}

	public Person createPerson() {
		showAndWait();
		return person;
	}
}

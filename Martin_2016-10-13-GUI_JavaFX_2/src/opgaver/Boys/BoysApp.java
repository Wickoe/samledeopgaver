package opgaver.Boys;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class BoysApp extends Application {
	private ArrayList<String> boys;
	private ListView<String> lvwNames;
	private TextField txfName;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Boys");
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setVgap(10);
		pane.setHgap(10);

		Label lblNames = new Label("Names:");
		pane.add(lblNames, 0, 0);

		boys = new ArrayList<String>();

		lvwNames = new ListView<String>();
		lvwNames.getItems().addAll(boys);
		pane.add(lvwNames, 1, 0);

		Label lblName = new Label("Name");
		pane.add(lblName, 0, 1);

		txfName = new TextField();
		pane.add(txfName, 1, 1);

		Button addName = new Button("Add");
		addName.setOnAction(event -> addNameAction());
		pane.add(addName, 2, 1);
	}

	private void addNameAction() {
		String name = txfName.getText().trim();

		if (name.length() > 0) {
			boys.add(name);
			clearFields();
			lvwNames.getItems().setAll(boys);
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Missing information");
			alert.setHeaderText("Missing information");
			alert.setContentText("Input a boys name i the textfield before you add");
			alert.showAndWait();
		}
	}

	private void clearFields() {
		txfName.clear();
	}
}

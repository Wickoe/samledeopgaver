package opgaver.BoysAndGirls;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class BoysAndGirlsApp extends Application {
	private ArrayList<String> boysNames, girlsNames;
	private ListView<String> lvwNames;
	private TextField txfName;
	private boolean boys;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Boys and girls");
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setVgap(10);
		pane.setHgap(10);

		boysNames = new ArrayList<>();
		girlsNames = new ArrayList<>();

		Label lblNames = new Label("Names");
		pane.add(lblNames, 0, 1);

		lvwNames = new ListView<String>();
		pane.add(lvwNames, 1, 1, 3, 1);

		RadioButton boys = new RadioButton("Boys");
		boys.setOnAction(event -> boysChoosenAction());

		RadioButton girls = new RadioButton("Girls");
		girls.setOnAction(event -> girlsChoosenAction());

		HBox namesToggled = new HBox();
		ToggleGroup groupNamesToggled = new ToggleGroup();

		boys.setToggleGroup(groupNamesToggled);
		girls.setToggleGroup(groupNamesToggled);

		namesToggled.getChildren().add(boys);
		namesToggled.getChildren().add(girls);

		pane.add(namesToggled, 2, 0);
		groupNamesToggled.getToggles().get(0).setSelected(true);

		Label lblName = new Label("Name");
		pane.add(lblName, 0, 3);

		txfName = new TextField();
		pane.add(txfName, 1, 3, 3, 1);

		Button btnAddName = new Button("Add");
		btnAddName.setOnAction(event -> addNameAction());
		pane.add(btnAddName, 5, 3);

		this.boys = true;
	}

	private void addNameAction() {
		String name = txfName.getText();
		if (name.length() > 0) {
			if (boys) {
				boysNames.add(name);
				lvwNames.getItems().setAll(boysNames);
			} else {
				girlsNames.add(name);
				lvwNames.getItems().setAll(girlsNames);
			}
			clearFields();
		}
	}

	private void boysChoosenAction() {
		boys = true;
		lvwNames.getItems().setAll(boysNames);
	}

	private void girlsChoosenAction() {
		boys = false;
		lvwNames.getItems().setAll(girlsNames);
	}

	private void clearFields() {
		txfName.clear();
	}
}

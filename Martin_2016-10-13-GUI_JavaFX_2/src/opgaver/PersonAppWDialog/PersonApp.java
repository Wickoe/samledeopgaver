package opgaver.PersonAppWDialog;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class PersonApp extends Application {
	private TextField txfName, txfTitle;
	private CheckBox cboxSenior;
	private ListView<Person> lvwPersons;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Person administration");
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setVgap(10);
		pane.setHgap(10);

		Label lblName = new Label("Name");
		pane.add(lblName, 0, 0);

		txfName = new TextField();
		pane.add(txfName, 1, 0);

		Label lblTitle = new Label("Title");
		pane.add(lblTitle, 0, 1);

		txfTitle = new TextField();
		pane.add(txfTitle, 1, 1);

		cboxSenior = new CheckBox("Sednior");
		pane.add(cboxSenior, 0, 2);

		Button btnAddPerson = new Button("Add person");
		btnAddPerson.setOnAction(event -> addPersonAction());
		pane.add(btnAddPerson, 3, 2);

		Label lblPersons = new Label("Persons");
		pane.add(lblPersons, 0, 3);

		lvwPersons = new ListView<>();
		pane.add(lvwPersons, 1, 3, 1, 2);
	}

	private void addPersonAction() {
		String name = txfName.getText().trim();
		String title = txfTitle.getText().trim();
		boolean isSenior = cboxSenior.isSelected();

		if (name.length() > 0 && title.length() > 0) {
			lvwPersons.getItems().add(new Person(name, title, isSenior));
			clearTextFields();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Missing information");
			alert.setHeaderText("Missing information");
			alert.setContentText("Be sure to type in a name, a title and if the person is a senior");
			alert.showAndWait();
		}
	}

	private void clearTextFields() {
		txfName.clear();
		txfTitle.clear();
		cboxSenior.setSelected(false);
	}
}

package storage;

import java.util.ArrayList;

import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.Person;
import application.model.Registration;

public class Storage {
	private static ArrayList<Person> persons = new ArrayList<>();
	private static ArrayList<Hotel> hotels = new ArrayList<>();
	private static ArrayList<Conference> conferences = new ArrayList<>();
	private static ArrayList<Registration> registrations = new ArrayList<>();
	private static ArrayList<Excursion> excursions = new ArrayList<>();

	// ==========================================================
	// Storage for persons

	/**
	 * Returns the storage persons ArrayList.
	 *
	 * @return ArrayList<Person>(persons)
	 */
	public static ArrayList<Person> getPersons() {
		return new ArrayList<Person>(persons);
	}

	/**
	 * Adds a person to the storage persons ArrayList.
	 *
	 * @param person
	 */
	public static void addPerson(Person person) {
		persons.add(person);
	}

	/**
	 * Removes a person from the storage persons ArrayList.
	 *
	 * @param person
	 */
	public static void removePerson(Person person) {
		persons.remove(person);
	}

	// ==========================================================
	// Storage for hotels

	/**
	 * Returns the storage hotels ArrayList.
	 *
	 * @return ArrayList<Hotel>(hotels)
	 */
	public static ArrayList<Hotel> getHotels() {
		return new ArrayList<Hotel>(hotels);
	}

	/**
	 * Adds a hotel to the storage hotels ArrayList.
	 *
	 * @param hotel
	 */
	public static void addHotel(Hotel hotel) {
		hotels.add(hotel);
	}

	/**
	 * Removes a hotel from the storage hotels ArrayList.
	 *
	 * @param hotel
	 */
	public static void removeHotel(Hotel hotel) {
		hotels.remove(hotel);
	}

	// ==========================================================
	// Storage for conferences

	/**
	 * Returns the storage conferences ArrayList.
	 *
	 * @return ArrayList<Conference>(conferences)
	 */
	public static ArrayList<Conference> getConferences() {
		return new ArrayList<Conference>(conferences);
	}

	/**
	 * Adds a conference to the storage conferences ArrayList.
	 *
	 * @param hotel
	 */
	public static void addConference(Conference conference) {
		conferences.add(conference);
	}

	/**
	 * Removes a conference from the storage conferences ArrayList.
	 *
	 * @param conference
	 */
	public static void removeConference(Conference conference) {
		conferences.remove(conference);
	}

	// ==========================================================
	// Storage for registrations

	/**
	 * Returns the storage registrations ArrayList.
	 *
	 * @return ArrayList<Registration>(registrations)
	 */
	public static ArrayList<Registration> getRegistration() {
		return new ArrayList<Registration>(registrations);
	}

	/**
	 * Adds a registration to the registrations storage ArrayList.
	 *
	 * @param registration
	 */
	public static void addRegistration(Registration registration) {
		registrations.add(registration);
	}

	/**
	 * Removes a registration from the registrations storage ArrayList.
	 *
	 * @param registration
	 */
	public static void removeRegistration(Registration registration) {
		registrations.remove(registration);
	}

	// ==========================================================
	// Storage for excursions

	/**
	 * Returns the storage excursions ArrayList.
	 *
	 * @return ArrayList<Excursion>(excursions)
	 */
	public static ArrayList<Excursion> getExcursion() {
		return new ArrayList<Excursion>(excursions);
	}

	/**
	 * Adds an excursion to the storage excursions ArrayList.
	 *
	 * @param excursion
	 */
	public static void addExcursion(Excursion excursion) {
		excursions.add(excursion);
	}

	/**
	 * Removes an excursion from the storage excursions ArrayList.
	 * 
	 * @param excursion
	 */
	public static void removeExcursion(Excursion excursion) {
		excursions.remove(excursion);
	}
}

package application.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Locale;

import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.HotelReservation;
import application.model.HotelService;
import application.model.Participant;
import application.model.Person;
import application.model.Registration;
import application.model.Role;
import application.model.Speaker;
import storage.Storage;

public class Service {

    // ======================================================================================================
    // Calculation methods

    /**
     * Calculates the amount of days a person is at a conference.
     *
     * @param arrivalDate
     * @param departureDate
     * @return daysSpentAtConference
     */
    public static int calcTotalDaysSpentAtConference(LocalDate arrivalDate,
        LocalDate departureDate) {
        int daysSpentAtConference = 1;

        daysSpentAtConference += (int) ((ChronoUnit.DAYS.between(arrivalDate, departureDate)));
        // Used to check if the calculation is correct.
        // System.out.println("Participation days is " + daysSpentAtConference);
        return daysSpentAtConference;
    }

    /**
     * Calculates the total participation cost of a registration.
     *
     * @param aRegistration
     * @param daysSpentAtConference
     * @param conferenceRole
     * @return
     */
    private static double calcConferenceParticipationCost(Registration aRegistration,
        int daysSpentAtConference,
        Role conferenceRole) {
        double participationCost = 0;

        if (conferenceRole instanceof Participant) {
            participationCost +=
                aRegistration.getConference().getParticipationPrice() * daysSpentAtConference;
        }
        // Used to check if the calculation is correct.
        // System.out.println("Participation cost is " + participationCost);
        return participationCost;
    }

    /**
     * Calculates the total companion cost of a registration.
     *
     * @param excursionsChosen
     * @return excursionTotalCost
     */
    private static double calcCompanionParticipationCost(ArrayList<Excursion> excursionsChosen) {
        double excursionTotalCost = 0;

        for (Excursion e : excursionsChosen) {
            excursionTotalCost += e.getExcursionPrice();
        }
        // Used to check if the calculation is correct.
        // System.out.println("Companion cost is " + excursionTotalCost);
        return excursionTotalCost;
    }

    /**
     * Calculates the total hotel cost of a registration.
     *
     * @param aHotelReservation
     * @param daysSpentAtConference
     * @return totalHotelCost
     */
    private static double calcTotalHotelCost(HotelReservation aHotelReservation,
        int daysSpentAtConference) {
        double totalHotelCost = 0;

        if (aHotelReservation.wantsSingleRoom()) {
            totalHotelCost +=
                aHotelReservation.getHotel().getSingleRoomPrice() * (daysSpentAtConference - 1);
        }
        else {
            totalHotelCost +=
                aHotelReservation.getHotel().getDoubleRoomPrice() * (daysSpentAtConference - 1);
        }

        for (HotelService s : aHotelReservation.getServicesChosen()) {
            totalHotelCost += s.getServicePrice() * (daysSpentAtConference - 1);
        }
        // Used to check if the calculation is correct.
        // System.out.println("Hotel cost is " + totalHotelCost);
        return totalHotelCost;
    }

    /**
     * Calculates the total price of a registration. Participation cost,
     * companion cost & hotel cost is included.
     *
     * @param aRegistration
     * @return totalPrice
     */
    public static double calculateTotalConferencePrice(Registration aRegistration) {
        int daysSpentAtConference = calcTotalDaysSpentAtConference(aRegistration.getArrivalDate(),
            aRegistration.getDepartureDate());
        double totalPrice = 0;

        totalPrice += calcConferenceParticipationCost(aRegistration, daysSpentAtConference,
            aRegistration.getParticipantRole());

        if (aRegistration.getCompanion() != null) {
            totalPrice += calcCompanionParticipationCost(aRegistration.getChosenExcursions());
        }

        if (aRegistration.getHotelReservation() != null) {
            totalPrice +=
                calcTotalHotelCost(aRegistration.getHotelReservation(), daysSpentAtConference);
        }

        return totalPrice;
    }

    // ======================================================================================================

    /**
     * Initializes the storage of the program. It contains persons, conferences,
     * hotels and excursions.
     */
    public static void initStorage() {
        ArrayList<Excursion> havOgHimmelExcursions = new ArrayList<>();
        havOgHimmelExcursions.add(Service.createExcursion("ByrundturOdense, incl. frokost", 125));
        havOgHimmelExcursions.add(Service.createExcursion("Egeskov", 75));
        havOgHimmelExcursions
            .add(Service.createExcursion("Trapholt Museum, Kolding, incl frokost", 200));

        Conference havOgHimmel =
            createConference("Hav og Himmel", 1500.0, LocalDate.of(2017, 4, 18),
                LocalDate.of(2017, 4, 20), havOgHimmelExcursions, "Hav og himmel");

        Hotel hvideSvane =
            Service.createHotel("Hotel Hvide Svane", "incl. bath", "5", "", "", 1050.0, 1250.0,
                "Lars Kristiansen", "larsk@gmail.com");
        Hotel hotelPhonix = Service.createHotel("Høtel Phønix", "Høtel Phønix", "3", "", "", 700.0,
            800.0, "Emil Emil",
            "emilemilemil@gmail.com");
        Hotel pensionTusindfryd =
            Service.createHotel("Pension Tusindfryd", "Hotel Tusindfryd", "2", "", "", 500.0,
                600.0, "Jes Dorph", "jes@yahoo.com");

        hvideSvane.createService("WiFi", 50);
        hotelPhonix.createService("Bath", 200);
        hotelPhonix.createService("WiFi", 75);
        pensionTusindfryd.createService("Breakfast", 100);

        Person madsen =
            Service.createPerson("Finn Madsen", "Krogagre 45", "Aarhus", "Danmark", "88888888");
        Person petersen =
            Service.createPerson("Niels Petersen", "Ringgade 3", "Aarhus", "Danmark", "88888888");
        Person sommer =
            Service.createPerson("Peter Sommer", "Ringgade 1", "Aarhus", "Danmark", "88888888");
        Person jensen = Service.createPerson("Lone Jensen", "Brobjergvænget 400", "Odense",
            "Libanon", "88888888");

        @SuppressWarnings("unused")
        Registration r1 = Service.createRegistration(madsen, havOgHimmel, LocalDate.of(2017, 4, 18),
            LocalDate.of(2017, 4, 20), createSpeaker(false));
        Registration r2 =
            Service.createRegistration(petersen, havOgHimmel, LocalDate.of(2017, 4, 18),
                LocalDate.of(2017, 4, 20), createSpeaker(false));
        Registration r3 = Service.createRegistration(sommer, havOgHimmel, LocalDate.of(2017, 4, 18),
            LocalDate.of(2017, 4, 20), createSpeaker(false));
        Registration r4 = Service.createRegistration(jensen, havOgHimmel, LocalDate.of(2017, 4, 18),
            LocalDate.of(2017, 4, 20), createSpeaker(true));

        r2.setHotelReservation(
            Service.createHotelReservation(hvideSvane, new ArrayList<HotelService>(), r2, true));

        ArrayList<HotelService> sommerChosenHotelService = new ArrayList<>();
        sommerChosenHotelService.add(hvideSvane.getServices().get(0));
        r3.setHotelReservation(
            Service.createHotelReservation(hvideSvane, sommerChosenHotelService, r3, false));
        r3.createCompanion("Mie Sommer" + r3.getPerson().getName());
        r3.addExcursion(havOgHimmelExcursions.get(1));
        r3.addExcursion(havOgHimmelExcursions.get(2));

        ArrayList<HotelService> jensenChosenHotelService = new ArrayList<>();
        jensenChosenHotelService.add(hvideSvane.getServices().get(0));
        r4.setHotelReservation(
            Service.createHotelReservation(hvideSvane, jensenChosenHotelService, r4, false));
        r4.createCompanion("Jan Madsen" + r4.getPerson().getName());
        r4.addExcursion(havOgHimmelExcursions.get(0));
        r4.addExcursion(havOgHimmelExcursions.get(1));
    }

    // ======================================================================================================
    // Methods relating to Persons

    /**
     * Creates a person and adds it to storage.
     *
     * @param aName
     * @param anAddress
     * @param aCity
     * @param aCountry
     * @param aPhoneNumber
     * @return reference of person created
     */
    public static Person createPerson(String aName, String anAddress, String aCity, String aCountry,
        String aPhoneNumber) {
        Person p = new Person(aName, anAddress, aCity, aCountry, aPhoneNumber);
        Storage.addPerson(p);
        return p;
    }

    /**
     * Updates person with an index.
     *
     * @param personIndex
     * @param name
     * @param address
     * @param city
     * @param country
     * @param phoneNr
     */
    public static void updatePerson(int personIndex, String name, String address, String city,
        String country,
        String phoneNr) {

        Person p = Storage.getPersons().get(personIndex);

        if (p != null) {
            p.setName(name);
            p.setAddress(address);
            p.setCity(city);
            p.setCountry(country);
            p.setPhoneNumber(phoneNr);
        }
    }

    /**
     * Updates person with a reference.
     *
     * @param personIndex
     * @param name
     * @param address
     * @param city
     * @param country
     * @param phoneNr
     */
    public static void updatePerson(Person p, String name, String address, String city,
        String country,
        String phoneNr) {

        if (p != null) {
            p.setName(name);
            p.setAddress(address);
            p.setCity(city);
            p.setCountry(country);
            p.setPhoneNumber(phoneNr);
        }
    }

    /**
     * Removes a person from storage.
     *
     * @param person
     */
    public static void deletePerson(Person person) {

        for (Registration r : person.getAllRegistrations()) {
            deleteRegistration(r);
        }

        Storage.removePerson(person);

    }

    // ======================================================================================================
    // Methods relating to Registration

    /**
     * If true then the role is a 'speaker' else it is a participant.
     *
     * @param speaker
     *
     * @return role
     */
    public static Role createSpeaker(boolean speaker) {
        Role role;

        if (speaker) {
            role = new Speaker();
        }
        else {
            role = new Participant();
        }

        return role;
    }

    private static Role createParticipant() {
        return new Participant();
    }

    private static Role createSpeaker() {
        return new Speaker();
    }

    /**
     * Creates a registration for a conference.
     *
     * @param person
     * @param conference
     * @param arrivalDate
     * @param departureDate
     * @param participantRole
     * @return
     */
    public static Registration createRegistration(Person person, Conference conference,
        LocalDate arrivalDate,
        LocalDate departureDate, Role participantRole) {

        Registration registration =
            new Registration(person, conference, arrivalDate, departureDate, participantRole);

        Storage.addRegistration(registration);

        return registration;

    }

    /**
     * Updates a registration for a conference.
     *
     * @param registration
     * @param participantRole
     * @param arrivalDate
     * @param departureDate
     */
    public static void updateRegistration(Registration registration, Role participantRole,
        LocalDate arrivalDate,
        LocalDate departureDate) {

        registration.setParticipantRole(participantRole);
        registration.setArrivalDate(arrivalDate);
        registration.setDepartureDate(departureDate);

    }

    /**
     * Deletes a registration from a conference.
     *
     * @param registration
     */
    public static void deleteRegistration(Registration registration) {
        // registration needs to be removed in person, conference, hotel
        Hotel hotel = null;
        HotelReservation hotelReservation = null;
        Person person = null;
        Conference conference = null;

        if (registration != null) {
            person = registration.getPerson();
            person.removeRegistrations(registration);
            if (person != null) {

                hotel = registration.getHotelReservation().getHotel();
                hotelReservation = registration.getHotelReservation();
                conference = registration.getConference();
            }
        }

        hotel.removeHotelReservation(hotelReservation);
        conference.removeRegistration(registration);
        storage.Storage.removeRegistration(registration);

    }

    /**
     * Adds an excursion to a registration.
     *
     * @param registration
     * @param excursion
     */
    public static void RegistrationAddExcursion(Registration registration, Excursion excursion) {

        registration.addExcursion(excursion);
    }

    public static Registration createRegistration(Person person, Conference conference,
        String arrivalDate,
        String departureDate, boolean speaker) {

        Role role;
        if (speaker) {
            role = Service.createSpeaker();
        }
        else {
            role = Service.createParticipant();
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
        formatter = formatter.withLocale(Locale.ENGLISH);

        Registration registration =
            new Registration(person, conference, LocalDate.parse(arrivalDate),
                LocalDate.parse(departureDate), role);

        Storage.addRegistration(registration);

        return registration;
    }

    public static void updateRegistration(Registration registration, LocalDate arrivalDate,
        LocalDate departureDate) {

        registration.setArrivalDate(arrivalDate);
        registration.setDepartureDate(departureDate);

    }

    public static void updateRegistration(Registration registration, String text) {
        registration.createCompanion(text);
    }

    /**
     * Removes an excursion from a registration.
     *
     * @param registration
     * @param excursion
     */
    public static void RegistrationRemoveExcursion(Registration registration, Excursion excursion) {

        registration.removeExcursion(excursion);
    }

    // ======================================================================================================
    // Methods relating to Hotels

    /**
     * Creates a hotel.
     *
     * @param name
     * @param description
     * @param stars
     * @param telephoneNumber
     * @param address
     * @param singleRoomPrice
     * @param doubleRoomPrice
     * @param hotelManager
     * @param hotelManagerEmail
     * @return hotel
     */
    public static Hotel createHotel(String name, String description, String stars,
        String telephoneNumber,
        String address, Double singleRoomPrice, Double doubleRoomPrice, String hotelManager,
        String hotelManagerEmail) {

        Hotel hotel = new Hotel(name, description, stars, telephoneNumber, address, singleRoomPrice,
            doubleRoomPrice,
            hotelManager, hotelManagerEmail);

        Storage.addHotel(hotel);
        return hotel;
    }

    /**
     * Updates a hotel.
     *
     * @param hotel
     * @param description
     * @param stars
     * @param telephoneNumber
     * @param address
     * @param singleRoomPrice
     * @param doubleRoomPrice
     * @param hotelManager
     * @param hotelManagerEmail
     */
    public static void updateHotel(Hotel hotel, String description, String stars,
        String telephoneNumber,
        String address, Double singleRoomPrice, Double doubleRoomPrice, String hotelManager,
        String hotelManagerEmail) {

        hotel.setDescription(description);
        hotel.setStars(stars);
        hotel.setPhoneNumber(telephoneNumber);
        hotel.setAddress(address);
        hotel.setSingleRoomPrice(singleRoomPrice);
        hotel.setDoubleRoomPrice(doubleRoomPrice);
        hotel.setHotelManager(hotelManager);
        hotel.setHotelManagerEmail(hotelManagerEmail);

    }

    /**
     * Checks if a hotel is removable - If there are any reservations registered
     * to it, it is not removable.
     *
     * @param hotel
     * @return boolean hotelIsRemovable
     */
    public static boolean hotelIsRemovable(Hotel hotel) {

        if (hotel.getReservations().size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * Deletes a hotel.
     *
     * @param hotel
     */
    public static void deleteHotel(Hotel hotel) {

        if (hotelIsRemovable(hotel)) {
            Storage.removeHotel(hotel);
        }
    }

    // ======================================================================================================
    // Methods relating to conference

    /**
     * Creates a conference.
     *
     * @param conferenceName
     * @param participationPrice
     * @param startDate
     * @param endDate
     * @param excursions
     * @param description
     * @return conference
     */
    public static Conference createConference(String conferenceName, Double participationPrice,
        LocalDate startDate,
        LocalDate endDate, ArrayList<Excursion> excursions, String description) {

        Conference conference =
            new Conference(conferenceName, participationPrice, startDate, endDate, excursions,
                description);

        storage.Storage.addConference(conference);
        return conference;
    }

    /**
     * Updates a conference.
     *
     * @param conference
     * @param conferenceName
     * @param participationPrice
     * @param startDate
     * @param endDate
     * @param excursions
     * @param description
     */
    public static void UpdateConference(Conference conference, String conferenceName,
        Double participationPrice,
        LocalDate startDate, LocalDate endDate, ArrayList<Excursion> excursions,
        String description) {
        conference.setName(conferenceName);
        conference.setParticipationPrice(participationPrice);
        conference.setStartDate(startDate);
        conference.setEndDate(endDate);
    }

    /**
     * Deletes a conference.
     *
     * @param conference
     */
    public static void deleteConference(Conference conference) {
        for (Registration r : conference.getRegistrations()) {
            Service.deleteRegistration(r);
        }
        Storage.removeConference(conference);
    }

    // ======================================================================================================
    // Methods relating excursions

    /**
     * Creates a excursion.
     *
     * @param description
     * @param excursionPrice
     * @return excursion
     */
    public static Excursion createExcursion(String description, double excursionPrice) {
        Excursion excursion = new Excursion(description, excursionPrice);
        Storage.addExcursion(excursion);
        return excursion;
    }

    /**
     * Deletes a excursion.
     *
     * @param excursion
     */
    public static void deleteExcursion(Excursion excursion) {

        Storage.removeExcursion(excursion);
    }

    /**
     * Updates an excursion.
     *
     * @param excursion
     * @param description
     * @param excursionPrice
     */
    public static void updateExcursion(Excursion excursion, String description,
        double excursionPrice) {
        excursion.setDescription(description);
        excursion.setExcursionPrice(excursionPrice);
    }

    // ======================================================================================================
    // Methods relating to hotel service

    /**
     * Creates a hotel reservation, includes services chosen.
     *
     * @param hotel
     * @param servicesChosen
     * @param registration
     * @param wantsSingleRoom
     * @return hotelReservation
     */
    public static HotelReservation createHotelReservation(Hotel hotel,
        ArrayList<HotelService> servicesChosen,
        Registration registration, boolean wantsSingleRoom) {
        HotelReservation hotelReservation =
            new HotelReservation(hotel, servicesChosen, registration, wantsSingleRoom);
        return hotelReservation;

    }
}
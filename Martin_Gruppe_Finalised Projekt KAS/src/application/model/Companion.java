package application.model;

public class Companion {

	private String name;

	// ===================================================================
	/**
	 * Initializes a companion. May only be called from Registration. Companion
	 * name also reflects name and ID number of the person relating to the
	 * companion. Syntax is "CompanionName (ID PersonName)".
	 *
	 * @param name
	 */
	Companion(String name) {
		this.name = name;

	}

	// ===================================================================
	// Simple get-set methods

	/**
	 * Gets the name of a companion.
	 *
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name of a companion.
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

}

package application.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Person {

	private String name;
	private String address;
	private String city;
	private String country;
	private String firmName;
	private String phoneNumber;
	private String email;
	private ArrayList<Registration> registrations;

	// ===================================================================

	/**
	 * Initializes a person.
	 *
	 * @param name
	 * @param address
	 * @param city
	 * @param country
	 * @param phoneNumber
	 */
	public Person(String name, String address, String city, String country, String phoneNumber) {
		this.name = name;
		setAddress(address);
		setCity(city);
		setCountry(country);
		setPhoneNumber(phoneNumber);
		registrations = new ArrayList<>();
	}

	// ===================================================================
	// Simple get-set methods

	/**
	 * Returns the name of a person.
	 *
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name of a person.
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the address of a person.
	 *
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address of a person.
	 *
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Returns the city of a person.
	 *
	 * @return city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city of a person.
	 *
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Returns the country of a person.
	 *
	 * @return country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country of a person.
	 *
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Returns the firm name of a person.
	 *
	 * @return firmName
	 */
	public String getFirmName() {
		return firmName;
	}

	/**
	 * Sets the firm name of a person.
	 *
	 * @param firmName
	 */
	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	/**
	 * Returns the phone number of a person.
	 *
	 * @return phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number of a person.
	 *
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * returns the email of a person.
	 *
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email of a person.
	 *
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	// ===================================================================
	// Methods relating to registration

	/**
	 * Adds a registration to the registrations ArrayList.
	 *
	 * @param registration
	 */
	public void addRegistration(Registration registration) {
		registrations.add(registration);
	}

	/**
	 * Removes a registration from the registrations ArrayList.
	 *
	 * @param registration
	 */
	public void removeRegistrations(Registration registration) {
		registrations.remove(registration);
	}

	/**
	 * Returns the registrations ArrayList.
	 *
	 * @return ArrayList<>(registrations)
	 */
	public ArrayList<Registration> getAllRegistrations() {
		return new ArrayList<>(registrations);
	}

	/**
	 * Returns all active registrations from the registrations ArrayList.
	 *
	 * @return ArrayList<>(activeRegistrations)
	 */
	public ArrayList<Registration> getActiveRegistrations() {
		ArrayList<Registration> activeRegistrations = new ArrayList<>();
		for (Registration r : registrations) {
			if (LocalDate.now().isBefore(r.getDepartureDate())) {
				activeRegistrations.add(r);
			}
		}

		return activeRegistrations;
	}

	// ===================================================================

	@Override
	public String toString() {
		String toString = name;
		return toString;
	}
}
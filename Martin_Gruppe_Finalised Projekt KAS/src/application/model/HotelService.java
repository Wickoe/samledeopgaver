package application.model;

public class HotelService {

	private String description;
	private double servicePrice;

	// ===================================================================

	/**
	 * Initilizes a hotel service.
	 * 
	 * @param description
	 * @param servicePrice
	 */
	HotelService(String description, double servicePrice) {
		this.description = description;
		this.servicePrice = servicePrice;
	}

	// ===================================================================
	// Simple get-set methods

	/**
	 * Returns the description of a hotel service.
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of a hotel service.
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the price of a hotel service.
	 *
	 * @return servicePrice
	 */
	public double getServicePrice() {
		return servicePrice;
	}

	/**
	 * Sets the price of a hotel service.
	 *
	 * @param servicePrice
	 */
	public void setServicePrice(double servicePrice) {
		this.servicePrice = servicePrice;
	}
}
package application.model;

import java.util.ArrayList;

public class HotelReservation {
	private ArrayList<HotelService> servicesChosen;
	private Hotel hotel;
	private Registration registration;
	private boolean wantsSingleRoom;

	// ===================================================================

	/**
	 * Initializes a hotel reservation.
	 *
	 * @param hotel
	 * @param servicesChosen
	 * @param registration
	 * @param wantsSingleRoom
	 */
	public HotelReservation(Hotel hotel, ArrayList<HotelService> servicesChosen, Registration registration,
			boolean wantsSingleRoom) {
		this.hotel = hotel;
		this.registration = registration;
		this.servicesChosen = servicesChosen;
		this.wantsSingleRoom = wantsSingleRoom;
	}

	// ===================================================================
	// Simple get-set methods

	/**
	 * Returns a hotel.
	 *
	 * @return hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * Sets the hotel of a hotel reservation.
	 *
	 * @param hotel
	 */
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/**
	 * Returns a registration.
	 *
	 * @return registration
	 */
	public Registration getRegistration() {
		return registration;
	}

	/**
	 * Sets the registration of a hotel reservation.
	 *
	 * @param registration
	 */
	public void setRegistration(Registration registration) {
		this.registration = registration;
	}

	/**
	 * Returns whether the reservation wants a single room.
	 *
	 * @return wantsSingleRoom
	 */
	public boolean wantsSingleRoom() {
		return wantsSingleRoom;
	}

	/**
	 * Sets if the person wants a single room.
	 *
	 * @param wantsSingleRoom
	 */
	public void setSingleRoom(boolean wantsSingleRoom) {
		this.wantsSingleRoom = wantsSingleRoom;
	}

	// ===================================================================
	// Methods relating to hotelservices

	/**
	 * Returns the services chosen ArrayList.
	 *
	 * @return ArrayList<>(servicesChosen)
	 */
	public ArrayList<HotelService> getServicesChosen() {
		return new ArrayList<>(servicesChosen);
	}

	/**
	 * Adds a hotel service to the services chosen ArrayList.
	 * 
	 * @param service
	 */
	public void addHotelService(HotelService service) {
		servicesChosen.add(service);
	}

	/**
	 * Removes a hotel service from the services chosen ArrayList.
	 * 
	 * @param service
	 */
	public void removeHotelService(HotelService service) {
		servicesChosen.remove(service);
	}
}
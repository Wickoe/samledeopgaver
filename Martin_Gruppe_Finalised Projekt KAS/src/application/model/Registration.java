package application.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Registration {

	private LocalDate arrivalDate;
	private LocalDate departureDate;
	private Conference conference;
	private Person person;
	private ArrayList<Excursion> excursions = new ArrayList<>();
	private HotelReservation hotelReservation;
	private Companion companion;
	private Role participationRole;

	// ==========================================================

	/**
	 * Initializes a registration.
	 *
	 * @param person
	 * @param conference
	 * @param arrivalDate
	 * @param departureDate
	 * @param participation
	 */
	public Registration(Person person, Conference conference, LocalDate arrivalDate, LocalDate departureDate,
			Role participation) {
		this.excursions = new ArrayList<>();
		this.person = person;
		this.conference = conference;
		this.arrivalDate = arrivalDate;
		this.departureDate = departureDate;
		this.participationRole = participation;
		if (!this.conference.getRegistrations().contains(this)) {
			this.conference.addRegistration(this);
		}
	}

	// ==========================================================
	// Simple get-set methods

	/**
	 * Returns the date of arrival.
	 *
	 * @return arrivalDate
	 */
	public LocalDate getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * Sets the arrival date of a registration.
	 *
	 * @param arrivalDate
	 */
	public void setArrivalDate(LocalDate arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * Returns the date of departure.
	 *
	 * @return departureDate
	 */
	public LocalDate getDepartureDate() {
		return departureDate;
	}

	/**
	 * Sets the departure date of a registration.
	 *
	 * @param departureDate
	 */
	public void setDepartureDate(LocalDate departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * Returns the conference of a registration.
	 *
	 * @return conference
	 */
	public Conference getConference() {
		return this.conference;
	}

	/**
	 * Sets the conference of a registration.
	 *
	 * @param conference
	 */
	public void setConference(Conference conference) {
		this.conference = conference;
	}

	/**
	 * Returns the person of a registration.
	 *
	 * @return person
	 */
	public Person getPerson() {
		return this.person;
	}

	/**
	 * Sets the person of a registration.
	 *
	 * @param person
	 */
	public void setPerson(Person person) {
		this.person = person;
	}

	/**
	 * Returns the hotel reservation of a registration.
	 *
	 * @return hotelReservation
	 */
	public HotelReservation getHotelReservation() {
		return this.hotelReservation;
	}

	/**
	 * Sets the hotel reservation of a registration.
	 *
	 * @param hotelReservation
	 */
	public void setHotelReservation(HotelReservation hotelReservation) {
		this.hotelReservation = hotelReservation;
	}

	/**
	 * Returns the role of a registration (Participant if true, Speaker if
	 * false)
	 *
	 * @return participationRole
	 */
	public Role getParticipantRole() {
		return participationRole;
	}

	/**
	 * Sets the role of a registration to a participant.
	 *
	 * @param participantRole
	 */
	public void setParticipantRole(Role participantRole) {
		this.participationRole = participantRole;
	}

	/**
	 * Returns whether the person belonging to the registration is a speaker.
	 *
	 * @return speaker
	 */
	public boolean isSpeaker() {
		boolean speaker = false;
		if (participationRole instanceof Speaker) {
			speaker = true;
		}
		return speaker;
	}

	// ==========================================================
	// Methods relating to companion. Note that this is a composition.

	/**
	 * Creates a companion for the registration.
	 *
	 * @param name
	 */
	public void createCompanion(String name) {
		companion = new Companion(name);

	}

	/**
	 * Deletes a companion from the registration.
	 */
	public void deleteCompanion() {
		this.companion = null;
	}

	/**
	 * Returns a companion from the registration.
	 *
	 * @return companion
	 */
	public Companion getCompanion() {
		return companion;
	}

	// ==========================================================
	// Methods relating to chosenExcursions

	/**
	 * Returns the excursions ArrayList.
	 *
	 * @return ArrayList<>(excursions)
	 */
	public ArrayList<Excursion> getChosenExcursions() {
		return new ArrayList<>(excursions);
	}

	/**
	 * Adds an excursion to the excursions ArrayList.
	 *
	 * @param excursion
	 */
	public void addExcursion(Excursion excursion) {
		excursions.add(excursion);
		if (!excursion.getRegistrations().contains(this)) {
			excursion.addRegistartion(this);
		}
	}

	/**
	 * Removes an excursion from the excursions ArrayList.
	 *
	 * @param excursion
	 */
	public void removeExcursion(Excursion excursion) {
		excursions.remove(excursion);
		if (excursion.getRegistrations().contains(this)) {
			excursion.removeRegistration(this);
		}
	}

	public void removeConference() {
		if (conference.getRegistrations().contains(this)) {
			conference.removeRegistration(this);
		}
		conference = null;
	}

	@Override

	public String toString() {
		String toString = "";
		toString = person.getName() + " attends " + conference.getName();
		return toString;
	}

}

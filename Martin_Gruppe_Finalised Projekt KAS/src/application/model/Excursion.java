package application.model;

import java.util.ArrayList;

public class Excursion {
	private String description;
	private double excursionPrice;
	private ArrayList<Registration> registrations;

	// ===================================================================

	/**
	 * Initializes a excursion.
	 *
	 * @param description
	 * @param excursionPrice
	 */
	public Excursion(String description, double excursionPrice) {
		this.description = description;
		this.excursionPrice = excursionPrice;
		registrations = new ArrayList<>();
	}

	// ===================================================================
	// Simple get-set methods

	/**
	 * Returns the description of an excursion.
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of an excursion.
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the price of an excursion.
	 *
	 * @return excursionPrice
	 */
	public double getExcursionPrice() {
		return excursionPrice;
	}

	/**
	 * Sets the price of an excursion.
	 *
	 * @param excursionPrice
	 */
	public void setExcursionPrice(double excursionPrice) {
		this.excursionPrice = excursionPrice;
	}

	// ===================================================================
	// Methods relating to registration

	/**
	 * Adds a registration to the registration ArrayList.
	 *
	 * @param registration
	 */
	public void addRegistartion(Registration registration) {
		registrations.add(registration);
		if (!registration.getChosenExcursions().contains(this)) {
			registration.addExcursion(this);
		}
	}

	/**
	 * Removes a registration from the registration ArrayList.
	 *
	 * @param registration
	 */
	public void removeRegistration(Registration registration) {
		registrations.remove(registration);
		if (registration.getChosenExcursions().contains(this)) {
			registration.removeExcursion(this);
		}
	}

	/**
	 * Returns the registration ArrayList.
	 *
	 * @return Arraylist<>(registrations)
	 */
	public ArrayList<Registration> getRegistrations() {
		return new ArrayList<>(registrations);
	}

	@Override
	public String toString() {
		String toString = description;
		return toString;
	}
}
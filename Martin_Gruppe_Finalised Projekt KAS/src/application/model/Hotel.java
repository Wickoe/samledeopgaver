package application.model;

import java.util.ArrayList;

public class Hotel {
	private String name;
	private String description;
	private String address;
	private String stars;
	private String phoneNumber;
	private ArrayList<HotelService> services;
	private ArrayList<HotelReservation> reservations;
	private double singleRoomPrice;
	private double doubleRoomPrice;
	private String hotelManager;
	private String hotelManagerEmail;

	// ===================================================================

	/**
	 * Initializes a hotel.
	 * 
	 * @param name
	 * @param description
	 * @param stars
	 * @param telephoneNumber
	 * @param address
	 * @param singleRoomPrice
	 * @param doubleRoomPrice
	 * @param hotelManager
	 * @param hotelManagerEmail
	 */
	public Hotel(String name, String description, String stars, String telephoneNumber, String address,
			double singleRoomPrice, double doubleRoomPrice, String hotelManager, String hotelManagerEmail) {
		setName(name);
		setDescription(description);
		setStars(stars);
		setPhoneNumber(telephoneNumber);
		setAddress(address);
		this.reservations = new ArrayList<>();
		this.singleRoomPrice = singleRoomPrice;
		this.doubleRoomPrice = doubleRoomPrice;
		this.services = new ArrayList<>();
		setHotelManager(hotelManager);
		setHotelManagerEmail(hotelManagerEmail);

	}

	// ===================================================================
	// Methods relating to hotelservices

	/**
	 * Creates a service for a hotel.
	 *
	 * @param serviceDescription
	 * @param serviceCostPrice
	 * @return
	 */
	public HotelService createService(String serviceDescription, double serviceCostPrice) {
		HotelService service = new HotelService(serviceDescription, serviceCostPrice);
		services.add(service);
		return service;
	}

	/**
	 * Deletes a service for a hotel.
	 *
	 * @param service
	 */
	public void deleteService(HotelService service) {
		services.remove(service);
	}

	/**
	 * Returns the services of a hotel.
	 *
	 * @return ArrayList<>(services)
	 */
	public ArrayList<HotelService> getServices() {
		return new ArrayList<>(services);
	}

	// ===================================================================
	// Methods relating to hotelreservations

	/**
	 * Adds a hotel reservation to the hotel reservation ArrayList.
	 *
	 * @param reservation
	 */
	public void addHotelReservation(HotelReservation reservation) {
		reservations.add(reservation);
	}

	/**
	 * Removes a hotel reservation from the hotel reservation ArrayList.
	 *
	 * @param reservation
	 */
	// TODO
	public void removeHotelReservation(HotelReservation reservation) {
		reservations.remove(reservation);
	}

	/**
	 * Returns the reservations of a hotel.
	 *
	 * @return ArrayList<>(reservations)
	 */
	public ArrayList<HotelReservation> getReservations() {
		return new ArrayList<>(reservations);
	}

	// ===================================================================
	// Simple get-set methods

	/**
	 * Returns the name of a hotel.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of a hotel.
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the description of a hotel.
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of a hotel.
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the amount of stars a hotel has.
	 *
	 * @return stars
	 */
	public String getStars() {
		return stars;
	}

	/**
	 * Sets the amount of stars a hotel has.
	 *
	 * @param stars
	 */
	public void setStars(String stars) {
		this.stars = stars;
	}

	/**
	 * Returns the phonenumber of a hotel.
	 *
	 * @return phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number of a hotel.
	 *
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Returns the address of a hotel.
	 *
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address of a hotel.
	 *
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Returns the price of a double room in a hotel.
	 *
	 * @return doubleRoomPrice
	 */
	public double getDoubleRoomPrice() {
		return this.doubleRoomPrice;
	}

	/**
	 * Sets the price of a double room in a hotel.
	 *
	 * @param doubleRoomPrice
	 */
	public void setDoubleRoomPrice(double doubleRoomPrice) {
		this.doubleRoomPrice = doubleRoomPrice;
	}

	/**
	 * Returns the price of a single room of a hotel.
	 *
	 * @return singleRoomPrice
	 */
	public double getSingleRoomPrice() {
		return this.singleRoomPrice;
	}

	/**
	 * Sets the price of a single room in a hotel.
	 *
	 * @param singleRoomPrice
	 */
	public void setSingleRoomPrice(double singleRoomPrice) {
		this.singleRoomPrice = singleRoomPrice;
	}

	/**
	 * Returns the name of a hotel manager.
	 *
	 * @return hotelManager
	 */
	public String getHotelManager() {
		return this.hotelManager;
	}

	/**
	 * Sets the name of a hotel manager.
	 *
	 * @param hotelManager
	 */
	public void setHotelManager(String hotelManager) {
		this.hotelManager = hotelManager;
	}

	/**
	 * Returns the email of a hotel manager.
	 *
	 * @return hotelManagerEmail;
	 */
	public String getManagerEmail() {
		return this.hotelManagerEmail;
	}

	/**
	 * Sets the email of a hotel manager.
	 *
	 * @param managerEmail
	 */
	public void setHotelManagerEmail(String hotelManagerEmail) {
		this.hotelManagerEmail = hotelManagerEmail;
	}

	@Override
	public String toString() {
		String toString = name;
		return toString;
	}
}
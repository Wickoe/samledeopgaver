package application.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Conference {
	private String name;
	private String description;
	private double participationPrice;
	private LocalDate startDate;
	private LocalDate endDate;
	private ArrayList<Registration> registrations;
	private ArrayList<Excursion> excursions;

	// ===================================================================

	/**
	 * Initializes a conference.
	 *
	 * @param conferenceName
	 * @param participationPrice
	 * @param startDate
	 * @param endDate
	 * @param registrations
	 * @param excursions
	 * @param description
	 */
	public Conference(String conferenceName, double participationPrice, LocalDate startDate, LocalDate endDate,
			ArrayList<Excursion> excursions, String description) {
		this.setName(conferenceName);
		this.setDescription(description);
		this.setParticipationPrice(participationPrice);
		this.setStartDate(startDate);
		this.setEndDate(endDate);
		this.excursions = excursions;
		this.registrations = new ArrayList<>();
	}

	// ===================================================================
	// Methods relating to registrations

	/**
	 * Adds a registration to the registration ArrayList.
	 *
	 * @param registration
	 */
	public void addRegistration(Registration registration) {
		registrations.add(registration);
	}

	/**
	 * Removes a registration from the registration ArrayList.
	 *
	 * @param registration
	 */
	public void removeRegistration(Registration registration) {
		registrations.remove(registration);
	}

	/**
	 * Returns the registration ArrayList.
	 *
	 * @return ArrayList<>(registrations)
	 */
	public ArrayList<Registration> getRegistrations() {
		return new ArrayList<>(registrations);
	}

	// ===================================================================
	// Methods relating to excursions

	/**
	 * Adds an excursion to the excursion ArrayList.
	 *
	 * @param excursion
	 */
	public void addExcursion(Excursion excursion) {
		excursions.add(excursion);
	}

	/**
	 * Removes an excursion from the excursion ArrayList.
	 *
	 * @param excursion
	 */
	public void removeExcursion(Excursion excursion) {
		excursions.remove(excursion);
	}

	/**
	 * Returns the excursions ArrayList.
	 *
	 * @return Arraylist<>(excursions)
	 */
	public ArrayList<Excursion> getExcursions() {
		return new ArrayList<>(excursions);
	}

	// ===================================================================
	// Simple get-set methods

	/**
	 * Gets the name of a conference.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of a conference
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the description of a conference
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of a conference.
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the participation price of a conference.
	 *
	 * @return participationPrice
	 */
	public double getParticipationPrice() {
		return participationPrice;
	}

	/**
	 * Sets the participation price of a conference.
	 *
	 * @param participationPrice
	 */
	public void setParticipationPrice(double participationPrice) {
		this.participationPrice = participationPrice;
	}

	// ===================================================================
	// LocalDate methods

	/**
	 * Returns the start date of a conference.
	 *
	 * @return startDate
	 */
	public LocalDate getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date of a conference.
	 *
	 * @param startDate
	 */
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	/**
	 * Returns the end date of a conference.
	 *
	 * @return endDate
	 */
	public LocalDate getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date of a conference
	 *
	 * @param endDate
	 */
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		String toString = name;
		return toString;
	}
}
package gui;

import application.model.Excursion;
import application.service.Service;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ExcursionWindow extends Stage {
	private Excursion createdExcursion;
	private TextField txfDescription, txfPrice;
	private Button btnCreate, btnCancel;
	private boolean newExcursion;
	private Excursion excursion;

	public ExcursionWindow(String title) {
		createdExcursion = null;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblDescription = new Label("Description:");
		txfDescription = new TextField();
		pane.add(lblDescription, 0, 0);
		pane.add(txfDescription, 1, 0);

		Label lblPrice = new Label("Price:");
		txfPrice = new TextField();
		pane.add(lblPrice, 0, 1);
		pane.add(txfPrice, 1, 1);

		btnCreate = new Button("Create");
		pane.add(btnCreate, 0, 2);

		btnCancel = new Button("Cancel");
		btnCancel.setOnAction(event -> cancelAction());
		pane.add(btnCancel, 1, 2);

	}

	public void createExcursion() {
		setTitle("Create excursion");
		btnCreate.setText("Create");
		btnCreate.setOnAction(event -> createAction());
	}

	private void createAction() {
		String description = txfDescription.getText().trim();
		double price = Double.valueOf(txfPrice.getText().trim());

		try {
			price = Double.valueOf(txfPrice.getText().trim());
		} catch (Exception e) {
			price = 0;
		}
		if (description.length() != 0 || description == null) {
			createdExcursion = Service.createExcursion(description, price);
			newExcursion = true;
			clearTextFields();
			hide();
		} else

		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Not enough information given");
			alert.setHeaderText("Give more information");
			alert.setContentText("You have to give an excursions description and price.");
			alert.showAndWait();
		}
	}

	public void cancelAction() {
		clearTextFields();
		newExcursion = false;
		hide();
	}

	public void clearTextFields() {
		txfDescription.clear();
		txfPrice.clear();
	}

	public Excursion getCreatedExcursion() {
		Excursion e = createdExcursion;
		createdExcursion = null;
		return e;
	}

	public void parseAndUpdateExcursion(Excursion excursion) {
		setTitle("Update excursion");
		btnCreate.setText("Update");
		btnCreate.setOnAction(event -> updateAction());

		txfDescription.setText(excursion.getDescription());
		txfPrice.setText("" + excursion.getExcursionPrice());
		this.excursion = excursion;

	}

	public boolean haveNewExcursion() {
		return newExcursion;
	}

	public void updateAction() {
		String description = txfDescription.getText().trim();
		double price = Double.valueOf(txfPrice.getText().trim());

		if (description.length() != 0 || description == null) {
			Service.updateExcursion(excursion, description, price);
			hide();
			clearTextFields();
		} else

		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Not enough information given");
			alert.setHeaderText("Give more information");
			alert.setContentText("You have to give an excursions description and price.");
			alert.showAndWait();
		}

	}
}

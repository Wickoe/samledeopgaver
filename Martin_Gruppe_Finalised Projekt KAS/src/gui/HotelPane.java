package gui;

import application.model.Hotel;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import storage.Storage;

public class HotelPane extends GridPane {
	private TextField txfName, txfDoubleRoom, txfDescription, txfStars, txfPhoneNumber, txfAddress, txfSingleRoom,
			txfHotelManager, txfHotelManagerEmail;
	private ListView<Hotel> lvwHotels;
	private HotelWindow dia;
	private Button btnDelete, btnUpdate, btnCreate;

	public HotelPane() {
		setHgap(10);
		setVgap(10);

		Label lblHotels = new Label("Hotels:");
		this.add(lblHotels, 0, 0);

		lvwHotels = new ListView<>();
		this.add(lvwHotels, 0, 1, 1, 3);
		lvwHotels.getItems().setAll(Storage.getHotels());

		HBox hbxButtons = new HBox();
		this.add(hbxButtons, 0, 4, 3, 1);
		hbxButtons.setPadding(new Insets(10, 0, 0, 0));
		hbxButtons.setAlignment(Pos.BASELINE_CENTER);

		btnCreate = new Button("Create");
		hbxButtons.getChildren().add(btnCreate);
		btnCreate.setOnAction(event -> createAction());

		btnUpdate = new Button("Update");
		btnUpdate.setDisable(true);
		hbxButtons.getChildren().add(btnUpdate);
		btnUpdate.setOnAction(event -> updateAction());

		btnDelete = new Button("Delete");
		btnDelete.setDisable(true);
		hbxButtons.getChildren().add(btnDelete);
		btnDelete.setOnAction(event -> deleteAction());

		VBox vertBox = new VBox();
		vertBox.setSpacing(1);

		Label lblName = new Label("Name");
		txfName = new TextField();
		txfName.setEditable(false);

		Label lblDescription = new Label("Description");
		txfDescription = new TextField();
		txfDescription.setEditable(false);

		Label lblStars = new Label("Stars");
		txfStars = new TextField();
		txfStars.setEditable(false);

		Label lblPhoneNumber = new Label("Phonenumber");
		txfPhoneNumber = new TextField();
		txfPhoneNumber.setEditable(false);

		Label lblAddress = new Label("Address");
		txfAddress = new TextField();
		txfAddress.setEditable(false);

		Label lblSingleRoom = new Label("Singleroom Price");
		txfSingleRoom = new TextField();
		txfSingleRoom.setEditable(false);

		Label lblDoubleRoom = new Label("Doubleroom Price");
		txfDoubleRoom = new TextField();
		txfDoubleRoom.setEditable(false);

		Label lblHotelManager = new Label("Hotel Manager");
		txfHotelManager = new TextField();
		txfHotelManager.setEditable(false);

		Label lblHotelManagerEmail = new Label("Hotel Managers Email");
		txfHotelManagerEmail = new TextField();
		txfHotelManagerEmail.setEditable(false);

		vertBox.getChildren().addAll(lblName, txfName, lblDescription, txfDescription, lblStars, txfStars,
				lblPhoneNumber, txfPhoneNumber, lblAddress, txfAddress, lblSingleRoom, txfSingleRoom, lblDoubleRoom,
				txfDoubleRoom, lblHotelManager, txfHotelManager, lblHotelManagerEmail, txfHotelManagerEmail);
		this.add(vertBox, 2, 1);
		ChangeListener<Hotel> listener = (ov, oldHotel, newHotel) -> backgroundInformationUpdate();
		lvwHotels.getSelectionModel().selectedItemProperty().addListener(listener);

		dia = new HotelWindow("");
	}

	private void deleteAction() {
		Service.deleteHotel(lvwHotels.getSelectionModel().getSelectedItem());
		this.lvwHotels.getItems().setAll(Storage.getHotels());

	}

	private void createAction() {
		dia.createHotel();
		dia.showAndWait();
		lvwHotels.getItems().setAll(Storage.getHotels());
		clearTextFields();
	}

	public void updateAction() {
		dia.parseAndUpdateHotel(lvwHotels.getSelectionModel().getSelectedItem());
		dia.showAndWait();
		lvwHotels.getItems().setAll(Storage.getHotels());
		backgroundInformationUpdate();
	}

	public void backgroundInformationUpdate() {
		Hotel h = this.lvwHotels.getSelectionModel().getSelectedItem();
		if (h != null) {
			btnDelete.setDisable(false);
			btnUpdate.setDisable(false);
			txfName.setText(h.getName());
			txfDescription.setText(h.getDescription());
			txfStars.setText(h.getStars());
			txfPhoneNumber.setText(h.getPhoneNumber());
			txfAddress.setText(h.getAddress());
			txfSingleRoom.setText("" + h.getSingleRoomPrice());
			txfDoubleRoom.setText("" + h.getDoubleRoomPrice());
			txfHotelManager.setText(h.getHotelManager());
			txfHotelManagerEmail.setText(h.getManagerEmail());
		} else {
			clearTextFields();
			btnDelete.setDisable(true);
			btnUpdate.setDisable(true);
		}
	}

	private void clearTextFields() {
		txfName.clear();
		txfDescription.clear();
		txfStars.clear();
		txfPhoneNumber.clear();
		txfAddress.clear();
		txfSingleRoom.clear();
		txfDoubleRoom.clear();
		txfHotelManager.clear();
		txfHotelManagerEmail.clear();
	}

}
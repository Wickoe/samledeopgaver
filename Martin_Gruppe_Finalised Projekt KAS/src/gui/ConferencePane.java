package gui;

import java.util.ArrayList;

import application.model.Conference;
import application.model.Registration;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import storage.Storage;

public class ConferencePane extends GridPane {
	private ListView<Conference> conferences;
	private ListView<Registration> registrations;
	private ConferenceWindow cfw;

	public ConferencePane() {
		setVgap(10);
		setHgap(10);

		initConferencePart();
		initRegistrationPart();

		cfw = new ConferenceWindow();
	}

	private void initConferencePart() {
		conferences = new ListView<>();
		conferences.getItems().setAll(Storage.getConferences());

		Label lblConferences = new Label("Conferences");
		this.add(lblConferences, 0, 0);
		this.add(conferences, 0, 1);

		ChangeListener<Conference> conferenceListener = (ov, oldConference,
				newConference) -> showRegistredRegistrations();
		conferences.getSelectionModel().selectedItemProperty().addListener(conferenceListener);

		Button createConference = new Button("Create conference");
		Button updateConference = new Button("Update conference");
		Button deleteConference = new Button("Delete conference");

		createConference.setOnAction(event -> createConferenceAction());
		updateConference.setOnAction(event -> updateConferenceAction());
		deleteConference.setOnAction(event -> deleteConferenceAction());

		HBox buttonsConferencesVbox = new HBox();
		buttonsConferencesVbox.getChildren().addAll(createConference, updateConference, deleteConference);

		this.add(buttonsConferencesVbox, 0, 2);

	}

	private void initRegistrationPart() {
		registrations = new ListView<>();
		Label lblRegistrations = new Label("Registrations");
		this.add(lblRegistrations, 2, 0);
		this.add(registrations, 2, 1);
		ChangeListener<Registration> registrationListener = (ov, oldRegistration,
				newRegistration) -> registrationsChanged();
		registrations.getSelectionModel().selectedItemProperty().addListener(registrationListener);
	}

	private void createConferenceAction() {
		cfw.updateConferenceWindow(null);
		cfw.showAndWait();

	}

	private void updateConferenceAction() {
		Conference c = conferences.getSelectionModel().getSelectedItem();
		if (c != null) {
			cfw.updateConferenceWindow(c);
			cfw.showAndWait();
			registrations.getItems().setAll(c.getRegistrations());
			cfw.updateConference();
		} else {
			Alert dialog = new Alert(Alert.AlertType.INFORMATION);
			dialog.setTitle("Choose a conference");
			dialog.setHeaderText("Choose a conference");
			dialog.setContentText("You must choose a conference first");
			dialog.showAndWait();
		}
	}

	private void deleteConferenceAction() {
		Conference c = conferences.getSelectionModel().getSelectedItem();
		if (c != null) {
			Service.deleteConference(c);
			conferences.getItems().setAll(Storage.getConferences());
		}
	}

	private void registrationsChanged() {

	}

	private void showRegistredRegistrations() {
		Conference c = conferences.getSelectionModel().getSelectedItem();
		if (c != null) {
			registrations.getItems().setAll(c.getRegistrations());
		} else {
			ArrayList<Registration> r = new ArrayList<>();
			registrations.getItems().setAll(r);
		}
	}

}
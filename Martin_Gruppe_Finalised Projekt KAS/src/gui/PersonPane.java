package gui;

import application.model.Person;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import storage.Storage;

public class PersonPane extends GridPane {
	private TextField txfName;
	private TextField txfAdresse;
	private TextField txfBy;
	private TextField txfLand;
	private TextField txfTlf;
	private ListView<Person> lvwPersons;
	private PersonWindow dia;
	private Button btnDelete;
	private Button btnUpdate;
	private Button btnCreate;

	public PersonPane() {
		setHgap(10);
		setVgap(10);

		Label lblPersons = new Label("Persons:");
		this.add(lblPersons, 0, 0);

		lvwPersons = new ListView<>();
		this.add(lvwPersons, 0, 1, 1, 10);
		lvwPersons.getItems().setAll(Storage.getPersons());

		HBox hbxButtons = new HBox();
		this.add(hbxButtons, 0, 11, 3, 1);
		hbxButtons.setPadding(new Insets(10, 0, 0, 0));
		hbxButtons.setAlignment(Pos.BASELINE_CENTER);

		btnCreate = new Button("Create");
		hbxButtons.getChildren().add(btnCreate);
		btnCreate.setOnAction(event -> createAction());

		btnUpdate = new Button("Update");
		btnUpdate.setDisable(true);
		hbxButtons.getChildren().add(btnUpdate);
		btnUpdate.setOnAction(event -> updateAction());

		btnDelete = new Button("Delete");
		btnDelete.setDisable(true);
		hbxButtons.getChildren().add(btnDelete);
		btnDelete.setOnAction(event -> deleteAction());

		Label lblName = new Label("Name");
		this.add(lblName, 2, 0);
		txfName = new TextField();
		this.add(txfName, 2, 1);
		txfName.setEditable(false);

		Label lblAdresse = new Label("Adresse");
		this.add(lblAdresse, 2, 2);
		txfAdresse = new TextField();
		this.add(txfAdresse, 2, 3);
		txfAdresse.setEditable(false);

		Label lblBy = new Label("By");
		this.add(lblBy, 2, 4);
		txfBy = new TextField();
		this.add(txfBy, 2, 5);
		txfBy.setEditable(false);

		Label lblLand = new Label("Land");
		this.add(lblLand, 2, 6);
		txfLand = new TextField();
		this.add(txfLand, 2, 7);
		txfLand.setEditable(false);

		Label lblTlf = new Label("Telefon nummer");
		this.add(lblTlf, 2, 8);
		txfTlf = new TextField();
		this.add(txfTlf, 2, 9);
		txfTlf.setEditable(false);

		ChangeListener<Person> listener = (ov, oldPerson, newPerson) -> backgroundInformationUpdate();
		lvwPersons.getSelectionModel().selectedItemProperty().addListener(listener);

		dia = new PersonWindow("");
	}

	private void deleteAction() {
		Service.deletePerson(lvwPersons.getSelectionModel().getSelectedItem());
		this.lvwPersons.getItems().setAll(Storage.getPersons());

	}

	private void createAction() {
		dia.createPerson();
		dia.showAndWait();
		lvwPersons.getItems().setAll(Storage.getPersons());
		clearTextFields();
	}

	public void updateAction() {
		dia.parseAndUpdatePerson(lvwPersons.getSelectionModel().getSelectedItem());
		dia.showAndWait();
		lvwPersons.getItems().setAll(Storage.getPersons());
		backgroundInformationUpdate();
	}

	public void backgroundInformationUpdate() {
		Person p = this.lvwPersons.getSelectionModel().getSelectedItem();
		if (p != null) {
			btnDelete.setDisable(false);
			btnUpdate.setDisable(false);
			txfName.setText(p.getName());
			txfAdresse.setText(p.getAddress());
			txfBy.setText(p.getCity());
			txfLand.setText(p.getCountry());
			txfTlf.setText(p.getPhoneNumber());
		} else {
			clearTextFields();
			btnDelete.setDisable(true);
			btnUpdate.setDisable(true);
		}
	}

	private void clearTextFields() {
		txfName.clear();
		txfAdresse.clear();
		txfBy.clear();
		txfLand.clear();
		txfTlf.clear();
	}

}
package gui;

import application.model.Hotel;
import application.service.Service;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class HotelWindow extends Stage {
	private Hotel createdHotel;
	private TextField txfName, txfDescription, txfStars, txfAddress, txfPhoneNumber, txfSingleRoomPrice,
			txfDoubleRoomPrice, txfHotelManager, txfHotelManagerEmail;
	private Button btnCreate;
	private boolean newHotel;
	private Hotel hotel;

	public HotelWindow(String title) {
		createdHotel = null;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblName = new Label("Name:");
		txfName = new TextField();
		pane.add(lblName, 0, 1);
		pane.add(txfName, 1, 1);

		Label lblAddress = new Label("Adress:");
		txfAddress = new TextField();
		pane.add(lblAddress, 0, 2);
		pane.add(txfAddress, 1, 2);

		Label lblDescription = new Label("Description:");
		txfDescription = new TextField();
		pane.add(lblDescription, 0, 3);
		pane.add(txfDescription, 1, 3);

		Label lblStars = new Label("Stars:");
		txfStars = new TextField();
		pane.add(lblStars, 0, 4);
		pane.add(txfStars, 1, 4);

		Label lblPhoneNumber = new Label("Phone Number:");
		txfPhoneNumber = new TextField();
		pane.add(lblPhoneNumber, 0, 5);
		pane.add(txfPhoneNumber, 1, 5);

		Label lblSingleRoomPrice = new Label("Singleroom Price");
		txfSingleRoomPrice = new TextField();
		pane.add(lblSingleRoomPrice, 0, 6);
		pane.add(txfSingleRoomPrice, 1, 6);

		Label lblDoubleRoomPrice = new Label("Doubleroom Price");
		txfDoubleRoomPrice = new TextField();
		pane.add(lblDoubleRoomPrice, 0, 7);
		pane.add(txfDoubleRoomPrice, 1, 7);

		Label lblHotelManager = new Label("Hotel Manager");
		txfHotelManager = new TextField();
		pane.add(lblHotelManager, 0, 8);
		pane.add(txfHotelManager, 1, 8);

		Label lblHotelManagerEmail = new Label("Hotel Manager's Email");
		txfHotelManagerEmail = new TextField();
		pane.add(lblHotelManagerEmail, 0, 9);
		pane.add(txfHotelManagerEmail, 1, 9);

		Button btnCancel = new Button("Cancel");
		btnCancel.setOnAction(event -> cancelAction());
		pane.add(btnCancel, 1, 10);

		btnCreate = new Button("");
		pane.add(btnCreate, 0, 10);
	}

	private void cancelAction() {
		clearTextFields();
		newHotel = false;
		hide();
	}

	private void createAction() {
		String name = txfName.getText().trim();
		String description = txfAddress.getText().trim();
		String stars = txfDescription.getText().trim();
		String phoneNumber = txfPhoneNumber.getText().trim();
		String address = txfStars.getText().trim();
		String hotelManager = txfHotelManager.getText().trim();
		String hotelManagerEmail = txfHotelManagerEmail.getText().trim();
		double singleRoomPrice;
		double doubleRoomPrice;

		try {
			singleRoomPrice = Double.valueOf(txfSingleRoomPrice.getText().trim());
			doubleRoomPrice = Double.valueOf(txfDoubleRoomPrice.getText().trim());
		} catch (Exception e) {
			singleRoomPrice = 0;
			doubleRoomPrice = 0;
		}

		if ((name.length() != 0 || name != null) && (description.length() != 0 || description != null)
				&& (stars.length() != 0 || stars != null) && (phoneNumber.length() != 0 || phoneNumber != null)
				&& (address.length() != 0 || address == null)) {
			createdHotel = Service.createHotel(name, description, stars, phoneNumber, address, singleRoomPrice,
					doubleRoomPrice, hotelManager, hotelManagerEmail);
			newHotel = true;
			clearTextFields();
			hide();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Not enough information given");
			alert.setHeaderText("Give more information");
			alert.setContentText(
					"You have to give a hotels name, description, stars, address, phonenumber, singleroom price and doubleroom price.");
			alert.showAndWait();
		}

	}

	public Hotel getCreatedHotel() {
		Hotel h = createdHotel;
		createdHotel = null;
		return h;
	}

	private void clearTextFields() {
		txfName.clear();
		txfAddress.clear();
		txfDescription.clear();
		txfStars.clear();
		txfPhoneNumber.clear();
		txfSingleRoomPrice.clear();
		txfDoubleRoomPrice.clear();
		txfHotelManager.clear();
		txfHotelManagerEmail.clear();
		hotel = null;
	}

	public boolean haveNewHotel() {
		return newHotel;
	}

	public void parseAndUpdateHotel(Hotel hotel) {
		setTitle("Update hotel");
		btnCreate.setText("Update");
		btnCreate.setOnAction(event -> updateAction());

		txfName.setText(hotel.getName());
		txfAddress.setText(hotel.getAddress());
		txfDescription.setText(hotel.getDescription());
		txfStars.setText(hotel.getStars());
		txfPhoneNumber.setText(hotel.getPhoneNumber());
		txfSingleRoomPrice.setText("" + hotel.getSingleRoomPrice());
		txfDoubleRoomPrice.setText("" + hotel.getDoubleRoomPrice());
		txfHotelManager.setText(hotel.getHotelManager());
		txfHotelManagerEmail.setText(hotel.getManagerEmail());
		this.hotel = hotel;
	}

	public void updateAction() {
		String name = txfName.getText().trim();
		String address = txfAddress.getText().trim();
		String description = txfDescription.getText().trim();
		String stars = txfStars.getText().trim();
		String phoneNumber = txfPhoneNumber.getText().trim();
		String hotelManager = txfHotelManager.getText().trim();
		String hotelManagerEmail = txfHotelManagerEmail.getText().trim();
		double singleRoomPrice = Double.valueOf(txfSingleRoomPrice.getText().trim());
		double doubleRoomPrice = Double.valueOf(txfDoubleRoomPrice.getText().trim());
		if (((name.length() != 0 || name != null) && (address.length() != 0 || address != null)
				&& (description.length() != 0 || description != null) && (stars.length() != 0 || stars != null)
				&& (phoneNumber.length() != 0 || phoneNumber == null))) {
			Service.updateHotel(hotel, address, description, stars, phoneNumber, singleRoomPrice, doubleRoomPrice,
					hotelManager, hotelManagerEmail);
			hide();
			clearTextFields();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Not enough information given");
			alert.setHeaderText("Give more information");
			alert.setContentText("You have to give a persons name, address, city, country and phonenumber");
			alert.showAndWait();
		}

	}

	public void createHotel() {
		setTitle("Create hotel");
		btnCreate.setText("Create");
		btnCreate.setOnAction(event -> createAction());
	}
}
package gui;

import application.service.Service;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static void main(String[] args) {
		Service.initStorage();
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("KAS");
		BorderPane pane = new BorderPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// makes tab panes ------------------------------------------------------
	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		initTabPane(tabPane);
		pane.setCenter(tabPane);
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		// add tab and pane for persons
		// --------------------------------------------

		Tab tabPersons = new Tab("Persons");
		tabPane.getTabs().add(tabPersons);

		PersonPane personerPane = new PersonPane();
		tabPersons.setContent(personerPane);
		tabPersons.setOnSelectionChanged(event -> personerPane.backgroundInformationUpdate());

		// add tab and pane for conferences
		// --------------------------------------------

		Tab tabKonference = new Tab("Conferences");
		tabPane.getTabs().add(tabKonference);

		ConferencePane konferencePane = new ConferencePane();
		tabKonference.setContent(konferencePane);

		// add tab and pane for hotels
		// --------------------------------------------

		Tab tabHoteller = new Tab("Hotels");
		tabPane.getTabs().add(tabHoteller);

		HotelPane HotelPane = new HotelPane();
		tabHoteller.setContent(HotelPane);
		tabHoteller.setOnSelectionChanged(event -> HotelPane.backgroundInformationUpdate());

		// add tab and pane for excursions
		// --------------------------------------------

		Tab tabExcursions = new Tab("Excursions");
		tabPane.getTabs().add(tabExcursions);

		ExcursionPane excursionsPane = new ExcursionPane();
		tabExcursions.setContent(excursionsPane);

		// add tab and pane for statistic
		// --------------------------------------------

		// Not nescesarry.
		// Tab tabStatestik = new Tab("Statistics");
		// tabPane.getTabs().add(tabStatestik);
		//
		// ExcursionPane statisticsPane = new ExcursionPane();
		// tabStatestik.setContent(statisticsPane);
		// tabStatestik.setOnSelectionChanged(event ->
		// statisticsPane.updateControls());

	}

}
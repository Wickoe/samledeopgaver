package gui;

import java.time.LocalDate;
import java.util.ArrayList;

import application.model.Conference;
import application.model.HotelReservation;
import application.model.Person;
import application.model.Registration;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import storage.Storage;

public class RegistrationWindow extends Stage {
	private Conference conference;
	private GridPane pane;
	private ListView<Person> persons;
	private PersonWindow psw;
	private ToggleGroup companionGroup;
	private boolean haveCompanion;
	private TextField showCompanion;
	private ArrayList<RadioButton> arrivalDates;
	private ArrayList<RadioButton> departureDates;
	private TextField txfHotelReservation;
	private HotelReservation reservation;
	private Registration registration;
	private RadioButton participantRole;
	private RadioButton speakerRole;
	private String role;
	private Button createRegistration;
	private RadioButton companion;
	private RadioButton noCompanion;

	public RegistrationWindow() {
		pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setVgap(10);
		pane.setHgap(10);

		arrivalDates = new ArrayList<>();
		departureDates = new ArrayList<>();

		companionGroup = new ToggleGroup();

		noCompanion = new RadioButton("No companion");
		noCompanion.setToggleGroup(companionGroup);
		noCompanion.setSelected(true);
		noCompanion.setOnAction(event -> toggleNoCompanionAction());

		showCompanion = new TextField();
		pane.add(showCompanion, 5, 1, 2, 1);
		haveCompanion = false;
		showCompanion.setDisable(true);

		companion = new RadioButton("Companion");
		companion.setToggleGroup(companionGroup);
		companion.setOnAction(event -> toggleCompanionAction());

		pane.add(companion, 5, 0);
		pane.add(noCompanion, 6, 0);

		Button createPerson = new Button("Create person");
		createPerson.setOnAction(event -> createPersonAction());
		pane.add(createPerson, 0, 6);

		Label lblPersons = new Label("Person");
		pane.add(lblPersons, 0, 0);
		persons = new ListView<>();
		persons.getItems().setAll(Storage.getPersons());
		pane.add(persons, 0, 1, 1, 5);
		ChangeListener<Person> peronsListener = (ov, oldPerson, newPerson) -> personSelectionAction();
		persons.getSelectionModel().selectedItemProperty().addListener(peronsListener);

		createRegistration = new Button("Create registration");
		createRegistration.setOnAction(event -> createRegistrationAction());
		pane.add(createRegistration, 10, 10);

		Button createHotelReservation = new Button("Create hotelreservation");
		createHotelReservation.setOnAction(event -> createHotelReservationAction());
		createHotelReservation.setDisable(true);
		pane.add(createHotelReservation, 5, 2);
		Button removeHotelReservation = new Button("Remove hotelreservation");
		removeHotelReservation.setOnAction(event -> removeHotelReservation());
		pane.add(removeHotelReservation, 6, 2);
		Button updateReservation = new Button("Update hotel reservation");
		updateReservation.setOnAction(event -> updateHotelReservation());

		txfHotelReservation = new TextField();
		txfHotelReservation.setEditable(false);
		pane.add(txfHotelReservation, 5, 3, 2, 1);
		if (reservation != null) {
			txfHotelReservation.setText("Hotel reservation at " + reservation.getHotel());
		}

		ToggleGroup tgroupRole = new ToggleGroup();

		participantRole = new RadioButton("Participant");
		speakerRole = new RadioButton("Speaker");

		participantRole.setToggleGroup(tgroupRole);
		participantRole.setOnAction(event -> roleAction(participantRole.getText()));
		speakerRole.setToggleGroup(tgroupRole);
		speakerRole.setOnAction(event -> roleAction(speakerRole.getText()));

		pane.add(participantRole, 5, 4);
		pane.add(speakerRole, 6, 4);
	}

	private void roleAction(String text) {
		role = text;
	}

	private void removeHotelReservation() {

	}

	private void createHotelReservationAction() {
		if (registration != null) {
			HotelReservationWindow hsr = new HotelReservationWindow();
			hsr.addOrUpdateHotelReservation("Create hotel reservation", null, registration);
			hsr.showAndWait();
		}
	}

	private void updateHotelReservation() {
		if (registration != null) {
			HotelReservationWindow hsr = new HotelReservationWindow();
			hsr.addOrUpdateHotelReservation("Update hotel reservation", null, registration);
			hsr.showAndWait();
		}
	}

	private void createRegistrationAction() {
		RadioButton arrivalDate = null;
		RadioButton departureDate = null;
		for (int i = 0; i < arrivalDates.size(); i++) {
			if (arrivalDates.get(i).isSelected()) {
				arrivalDate = arrivalDates.get(i);
			}
			if (departureDates.get(i).isSelected()) {
				departureDate = departureDates.get(i);
			}
		}
		boolean speaker = false;
		if (role.equals("Speaker")) {
			speaker = true;
		}
		registration = Service.createRegistration(persons.getSelectionModel().getSelectedItem(), conference,
				arrivalDate.getText(), departureDate.getText(), speaker);
		if (haveCompanion) {
			Service.updateRegistration(registration, showCompanion.getText());
		}
		hide();
	}

	private void toggleCompanionAction() {
		haveCompanion = true;
		showCompanion.setDisable(false);
	}

	private void toggleNoCompanionAction() {
		haveCompanion = false;
		showCompanion.setDisable(true);
	}

	private Person personSelectionAction() {
		return persons.getSelectionModel().getSelectedItem();
	}

	private void createPersonAction() {
		psw = new PersonWindow("Create person");
		psw.createPerson();
		psw.showAndWait();
		persons.getItems().setAll(Storage.getPersons());
	}

	public void initUpdateWindow(Conference conference, LocalDate startDate, LocalDate endDate) {
		this.conference = conference;
		setTitle("Registre for " + conference.getName());
		ToggleGroup arrivalDateGroup = new ToggleGroup();
		ToggleGroup departureDateGroup = new ToggleGroup();

		int daysAtConference = Service.calcTotalDaysSpentAtConference(startDate, endDate);
		Label lblArrivalDate = new Label("Arrival date");
		Label lblDepartureDate = new Label("Departure date");
		pane.add(lblArrivalDate, 3, 0);
		pane.add(lblDepartureDate, 4, 0);
		for (int i = 0; i < daysAtConference; i++) {
			RadioButton rb1 = new RadioButton(LocalDate
					.of(startDate.getYear(), startDate.getMonthValue(), startDate.getDayOfMonth() + i).toString());
			rb1.setToggleGroup(arrivalDateGroup);
			arrivalDates.add(rb1);

			rb1 = new RadioButton(LocalDate
					.of(startDate.getYear(), startDate.getMonthValue(), startDate.getDayOfMonth() + i).toString());
			rb1.setToggleGroup(departureDateGroup);
			departureDates.add(rb1);
		}
		for (int j = 0; j < arrivalDates.size(); j++) {
			pane.add(arrivalDates.get(j), 3, (j + 1));
			pane.add(departureDates.get(j), 4, (j + 1));
		}
	}

	public Registration getRegistration() {
		return registration;
	}

	public void createConferenceWindow() {
		// TODO Auto-generated method stub

	}

	public void updateFields(Registration r) {
		initUpdateWindow(r.getConference(), r.getArrivalDate(), r.getDepartureDate());
		this.registration = r;
		persons.getSelectionModel().select(registration.getPerson());
		if (registration.getCompanion() != null) {
			companion.setSelected(true);
			showCompanion.setText(registration.getCompanion().getName());
		} else {
			noCompanion.setSelected(true);
		}

	}
}
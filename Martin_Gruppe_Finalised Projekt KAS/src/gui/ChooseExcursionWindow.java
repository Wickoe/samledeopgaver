package gui;

import java.util.ArrayList;
import java.util.Iterator;

import application.model.Conference;
import application.model.Excursion;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import storage.Storage;

public class ChooseExcursionWindow extends Stage {
	@SuppressWarnings("unused")
	private Excursion createdExcursion;
	@SuppressWarnings("unused")
	private TextField txfDescription, txfPrice;
	private Button btnOK, btnCancel;
	@SuppressWarnings("unused")
	private boolean newExcursion;
	@SuppressWarnings("unused")
	private Excursion excursion;
	private ListView<Excursion> lvwChoose;
	@SuppressWarnings("unused")
	private Conference conference;

	public ChooseExcursionWindow(String title, Conference conference) {
		this.conference = conference;
		createdExcursion = null;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setHgap(10);
		pane.setVgap(10);

		lvwChoose = new ListView<>();
		lvwChoose.getItems().setAll(Storage.getExcursion());
		lvwChoose.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		pane.add(lvwChoose, 0, 0);

		btnOK = new Button("Ok");
		btnOK.setOnAction(event -> OKAction());
		pane.add(btnOK, 0, 1);
		btnOK.setAlignment(Pos.BASELINE_CENTER);

		btnCancel = new Button("Cancel");
		btnCancel.setOnAction(event -> cancelAction());
		pane.add(btnCancel, 1, 1);
		btnCancel.setAlignment(Pos.BASELINE_CENTER);

	}

	public ArrayList<Excursion> OKAction() {
		ArrayList<Excursion> lol = new ArrayList<>();

		Iterator<Excursion> iter = lvwChoose.getSelectionModel().getSelectedItems().iterator();

		while (iter.hasNext()) {
			lol.add(iter.next());
		}
		hide();
		return lol;
	}

	public void cancelAction() {
		hide();
	}

}

package gui;

import application.model.Person;
import application.service.Service;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class PersonWindow extends Stage {
	private Person createdPerson;
	private TextField txfName, txfAddress, txfCity, txfCountry, txfTlf;
	private boolean newPerson;
	private Person person;
	private Button btnCreate;

	public PersonWindow(String title) {
		createdPerson = null;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblName = new Label("Name:");
		txfName = new TextField();
		pane.add(lblName, 0, 1);
		pane.add(txfName, 1, 1);

		Label lblAddress = new Label("Adress:");
		txfAddress = new TextField();
		pane.add(lblAddress, 0, 2);
		pane.add(txfAddress, 1, 2);

		Label lblCity = new Label("City:");
		txfCity = new TextField();
		pane.add(lblCity, 0, 3);
		pane.add(txfCity, 1, 3);

		Label lblCountry = new Label("Country:");
		txfCountry = new TextField();
		pane.add(lblCountry, 0, 4);
		pane.add(txfCountry, 1, 4);

		Label lblPhonenumber = new Label("Phone Number:");
		txfTlf = new TextField();
		pane.add(lblPhonenumber, 0, 5);
		pane.add(txfTlf, 1, 5);

		Button btnCancel = new Button("Cancel");
		btnCancel.setOnAction(event -> cancelAction());
		pane.add(btnCancel, 1, 6);

		btnCreate = new Button("");
		pane.add(btnCreate, 0, 6);
	}

	private void cancelAction() {
		clearTextFields();
		newPerson = false;
		hide();
	}

	private void createAction() {
		String name = txfName.getText().trim();
		String address = txfAddress.getText().trim();
		String city = txfCity.getText().trim();
		String country = txfCountry.getText().trim();
		String phoneNr = txfTlf.getText().trim();

		if ((name.length() != 0 || name != null) && (address.length() != 0 || address != null)
				&& (city.length() != 0 || city != null) && (country.length() != 0 || country != null)
				&& (phoneNr.length() != 0 || phoneNr == null)) {
			createdPerson = Service.createPerson(name, address, city, country, phoneNr);
			newPerson = true;
			clearTextFields();
			hide();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Not enough information given");
			alert.setHeaderText("Give more information");
			alert.setContentText("You have to give a persons name, address, city, country and phonenumber");
			alert.showAndWait();
		}
	}

	public Person getCreatedPerson() {
		Person p = createdPerson;
		createdPerson = null;
		return p;
	}

	private void clearTextFields() {
		txfName.clear();
		txfAddress.clear();
		txfCity.clear();
		txfCountry.clear();
		txfTlf.clear();
		person = null;
	}

	public boolean haveNewPerson() {
		return newPerson;
	}

	public void parseAndUpdatePerson(Person person) {
		setTitle("Update person");
		btnCreate.setText("Update");
		btnCreate.setOnAction(event -> updateAction());

		txfName.setText(person.getName());
		txfAddress.setText(person.getAddress());
		txfCity.setText(person.getCity());
		txfCountry.setText(person.getCountry());
		txfTlf.setText(person.getPhoneNumber());
		this.person = person;
	}

	public void updateAction() {
		String name = txfName.getText().trim();
		String address = txfAddress.getText().trim();
		String city = txfCity.getText().trim();
		String country = txfCountry.getText().trim();
		String phoneNr = txfTlf.getText().trim();
		if (((name.length() != 0 || name != null) && (address.length() != 0 || address != null)
				&& (city.length() != 0 || city != null) && (country.length() != 0 || country != null)
				&& (phoneNr.length() != 0 || phoneNr == null))) {
			Service.updatePerson(person, name, address, city, country, phoneNr);
			hide();
			clearTextFields();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Not enough information given");
			alert.setHeaderText("Give more information");
			alert.setContentText("You have to give a persons name, address, city, country and phonenumber");
			alert.showAndWait();
		}
	}

	public void createPerson() {
		setTitle("Create person");
		btnCreate.setText("Create");
		btnCreate.setOnAction(event -> createAction());
	}
}
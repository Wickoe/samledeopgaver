package gui;

import java.util.ArrayList;

import application.model.Excursion;
import application.model.Person;
import application.model.Registration;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import storage.Storage;

public class ExcursionPane extends GridPane {

	private ListView<Excursion> lvwExcursions;
	private TextField txfDescription, txfPrice;
	private Button btnDelete;
	private Button btnUpdate;
	private Button btnCreate;
	private ExcursionWindow dia, diaConference;
	private ListView<Person> lvwPersons;

	public ExcursionPane() {
		setVgap(10);
		setHgap(10);

		Label lblExcursions = new Label("Excursions:");

		Label lblPersons = new Label("Persons:");

		this.add(lblExcursions, 0, 0);

		this.add(lblPersons, 1, 0);

		// list view
		lvwExcursions = new ListView<Excursion>();
		this.add(lvwExcursions, 0, 1, 1, 10);
		lvwExcursions.getItems().setAll(Storage.getExcursion());

		lvwPersons = new ListView<Person>();
		this.add(lvwPersons, 1, 1, 1, 8);

		HBox hbxButtons = new HBox();
		this.add(hbxButtons, 0, 11, 3, 1);
		hbxButtons.setPadding(new Insets(10, 0, 0, 0));
		hbxButtons.setAlignment(Pos.BASELINE_CENTER);

		btnCreate = new Button("Create");
		hbxButtons.getChildren().add(btnCreate);
		btnCreate.setOnAction(event -> this.createAction());

		btnUpdate = new Button("Update");
		btnUpdate.setDisable(true);
		hbxButtons.getChildren().add(btnUpdate);
		btnUpdate.setOnAction(event -> this.updateAction());

		btnDelete = new Button("Delete");
		btnDelete.setDisable(true);
		hbxButtons.getChildren().add(btnDelete);
		btnDelete.setOnAction(event -> this.deleteAction());

		Label lblDescription = new Label("Description");
		this.add(lblDescription, 2, 1);
		txfDescription = new TextField();
		txfDescription.setEditable(false);
		this.add(txfDescription, 2, 2);

		Label lblPrice = new Label("Price");
		this.add(lblPrice, 2, 3);
		txfPrice = new TextField();
		txfPrice.setEditable(false);
		this.add(txfPrice, 2, 4);

		ChangeListener<Excursion> listener = (ov, oldExcursion, newExcursion) -> this.backgroundInformationUpdate();
		lvwExcursions.getSelectionModel().selectedItemProperty().addListener(listener);

		dia = new ExcursionWindow("");
		diaConference = new ExcursionWindow("");

	}

	private void backgroundInformationUpdate() {
		Excursion e = this.lvwExcursions.getSelectionModel().getSelectedItem();
		if (e != null) {
			btnDelete.setDisable(false);
			btnUpdate.setDisable(false);
			txfDescription.setText(e.getDescription());
			txfPrice.setText("" + e.getExcursionPrice());

			ArrayList<Person> personList = new ArrayList<>();

			for (Registration r : e.getRegistrations()) {
				personList.add(r.getPerson());

			}
			lvwPersons.getItems().setAll(personList);

		} else {
			clearTextFields();
			btnDelete.setDisable(true);
			btnUpdate.setDisable(true);

		}
	}

	public void clearTextFields() {
		txfDescription.clear();
		txfPrice.clear();
	}

	public void updateActionConference() {
		diaConference.parseAndUpdateExcursion(lvwExcursions.getSelectionModel().getSelectedItem());
		diaConference.showAndWait();
		lvwExcursions.getItems().setAll(Storage.getExcursion());
		backgroundInformationUpdate();
	}

	public void updateAction() {
		dia.parseAndUpdateExcursion(lvwExcursions.getSelectionModel().getSelectedItem());
		dia.showAndWait();
		lvwExcursions.getItems().setAll(Storage.getExcursion());
		backgroundInformationUpdate();
	}

	private void deleteAction() {
		Service.deleteExcursion(lvwExcursions.getSelectionModel().getSelectedItem());
		this.lvwExcursions.getItems().setAll(Storage.getExcursion());
	}

	private void createAction() {
		dia.createExcursion();
		dia.showAndWait();
		lvwExcursions.getItems().setAll(Storage.getExcursion());
		clearTextFields();
	}

}

package gui;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

import application.model.Conference;
import application.model.Excursion;
import application.model.Registration;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import storage.Storage;

public class ConferenceWindow extends Stage {
    private Conference conference;
    private TextField name;
    private TextField description;
    private TextField participantPrice;
    private TextField startDate;
    private TextField endDate;
    private TextField priceTotalForRegistration;
    private ListView<Registration> registrations;
    private ListView<Excursion> excursions;
    private Button btnCancel;
    private Button btnConferenceChange;
    private Button btnAddRegistration;
    private Button btnDeleteRegistration;
    private Button btnAddExcursion;
    private Button btnDeleteExcursion;
    private RegistrationWindow rgw;
    private GridPane pane;
    private ChooseExcursionWindow chooseExcursionWindow;
    
    public ConferenceWindow() {
        pane = new GridPane();
        initContent();
        
        Scene scene = new Scene(pane);
        setScene(scene);
    }
    
    private void initContent() {
        pane.setVgap(10);
        pane.setHgap(10);
        
        Label lblName = new Label("Name");
        name = new TextField();
        pane.add(lblName, 0, 0);
        pane.add(name, 1, 0);
        
        Label lblDescription = new Label("Description");
        description = new TextField();
        pane.add(lblDescription, 0, 1);
        pane.add(description, 1, 1);
        
        Label lblPrice = new Label("Participation price");
        participantPrice = new TextField();
        pane.add(lblPrice, 0, 2);
        pane.add(participantPrice, 1, 2);
        
        Label lblStartDate = new Label("Conference start date");
        startDate = new TextField();
        pane.add(lblStartDate, 0, 3);
        pane.add(startDate, 1, 3);
        
        Label lblEndDate = new Label("Conference end date");
        endDate = new TextField();
        pane.add(lblEndDate, 0, 4);
        pane.add(endDate, 1, 4);
        
        Label lblRegistrations = new Label("Registrations");
        registrations = new ListView<>();
        pane.add(lblRegistrations, 2, 1);
        pane.add(registrations, 3, 0, 1, 6);
        
        Label lblExcursions = new Label("Excursions");
        excursions = new ListView<>();
        pane.add(lblExcursions, 0, 5);
        pane.add(excursions, 1, 5);
        
        btnConferenceChange = new Button();
        pane.add(btnConferenceChange, 0, 8);
        
        btnAddRegistration = new Button("Add registration");
        btnAddRegistration.setOnAction(event -> addRegistrationAction());
        pane.add(btnAddRegistration, 2, 8);
        btnDeleteRegistration = new Button("Delete registration");
        btnDeleteRegistration.setOnAction(event -> deleteRegistrationAction());
        pane.add(btnDeleteRegistration, 3, 8);
        
        HBox regHbox = new HBox();
        regHbox.getChildren().addAll(btnAddRegistration, btnDeleteRegistration);
        pane.add(regHbox, 3, 7);
        
        btnAddExcursion = new Button("Add excursion");
        btnAddExcursion.setOnAction(event -> addExcursionAction());
        pane.add(btnAddExcursion, 10, 10);
        btnDeleteExcursion = new Button("Delete excursion");
        btnDeleteExcursion.setOnAction(event -> deleteExcursionAction());
        pane.add(btnDeleteExcursion, 10, 11);
        
        HBox exHbox = new HBox();
        exHbox.getChildren().addAll(btnAddExcursion, btnDeleteExcursion);
        pane.add(exHbox, 1, 7);
        
        ChangeListener<Registration> registrationListener = (ov, oldRegistration,
            newRegistration) -> registrationListenerAction();
        registrations.getSelectionModel().selectedItemProperty().addListener(registrationListener);
        Button btnUpdateRegistration = new Button("Update registration");
        btnUpdateRegistration.setOnAction(event -> updateRegistrationAction());
        pane.add(btnUpdateRegistration, 4, 8);
        
        priceTotalForRegistration = new TextField();
        priceTotalForRegistration.setEditable(false);
        Label lblPriceforRegistration = new Label("Price: ");
        Button registrationPrice = new Button("Calculate price");
        registrationPrice.setOnAction(event -> showPriceAction());
        
        pane.add(priceTotalForRegistration, 6, 1);
        pane.add(lblPriceforRegistration, 6, 0);
        pane.add(registrationPrice, 6, 2, 1, 2);
        
    }
    
    private Registration registrationListenerAction() {
        return registrations.getSelectionModel().getSelectedItem();
    }
    
    public void addExcursionAction() {
        chooseExcursionWindow = new ChooseExcursionWindow("Choose excursions", conference);
        chooseExcursionWindow.showAndWait();
        setAllExcursions();
        
    }
    
    private void cancelAction() {
        clearTextFields();
        hide();
    }
    
    private void showPriceAction() {
        Registration r = registrations.getSelectionModel().getSelectedItem();
        if (r != null) {
            this.priceTotalForRegistration.setText("" + Service.calculateTotalConferencePrice(r));
        }
    }
    
    private void addRegistrationAction() {
        String conferenceName = name.getText().trim();
        String sStartDate = startDate.getText().trim();
        String sEndDate = endDate.getText().trim();
        
        if (conference != null
            && (conferenceName.length() > 0 && sStartDate.length() > 0 && sEndDate.length() > 0)) {
            
            rgw = new RegistrationWindow();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
            formatter = formatter.withLocale(Locale.ENGLISH);
            rgw.initUpdateWindow(conference, LocalDate.parse(startDate.getText().trim()),
                LocalDate.parse(endDate.getText().trim()));
            rgw.showAndWait();
            if (rgw.getRegistration() != null) {
                registrations.getItems().add(rgw.getRegistration());
            }
        }
        else if (conferenceName.length() > 0 && sStartDate.length() > 0 && sEndDate.length() > 0) {
            rgw = new RegistrationWindow();
        }
    }
    
    private void deleteRegistrationAction() {
        Registration registration = registrations.getSelectionModel().getSelectedItem();
        if (registration != null) {
            registrations.getItems().remove(registration);
            Service.deleteRegistration(registration);
        }
        else {
            
        }
    }
    
    private void updateRegistrationAction() {
        rgw = new RegistrationWindow();
        Registration r = registrationListenerAction();
        if (r == null) {
            
        }
        else {
            rgw.updateFields(r);
            rgw.showAndWait();
        }
    }
    
    public void setAllExcursions() {
        excursions.getItems().setAll(chooseExcursionWindow.OKAction());
    }
    
    private void createConferenceAction() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
        formatter = formatter.withLocale(Locale.ENGLISH);
        
        String conferenceNamename = name.getText().trim();
        double price = 0.0;
        if (!participantPrice.getText().trim().isEmpty()) {
            price = Double.valueOf(participantPrice.getText().trim());
        }
        LocalDate startDate = LocalDate.parse(this.startDate.getText().trim());
        LocalDate endDate = LocalDate.parse(this.endDate.getText().trim());
        String conferenceDescription = description.getText().trim();
        
        if (conferenceNamename.length() > 0 && startDate.isBefore(endDate)
            && conferenceDescription.length() > 0) {
            Service.createConference(conferenceNamename, price, startDate, endDate,
                new ArrayList<Excursion>(),
                conferenceDescription);
            
            clearTextFields();
            hide();
        }
    }
    
    private void deleteExcursionAction() {
        Service.deleteExcursion(excursions.getSelectionModel().getSelectedItem());
        this.excursions.getItems().setAll(Storage.getExcursion());
    }
    
    public void updateConferenceWindow(Conference conference) {
        this.conference = conference;
        if (conference != null) {
            updateTextFields();
        }
        else {
            createConference();
        }
    }
    
    public void updateTextFields() {
        setTitle("Update conference");
        btnConferenceChange.setText("Update");
        btnConferenceChange.setOnAction(event -> updateAction());
        name.setText(conference.getName());
        description.setText(conference.getDescription());
        participantPrice.setText(Double.toString(conference.getParticipationPrice()));
        startDate.setText(conference.getStartDate().toString());
        endDate.setText(conference.getEndDate().toString());
        registrations.getItems().setAll(conference.getRegistrations());
        excursions.getItems().setAll(conference.getExcursions());
    }
    
    public void updateAction() {
        hide();
    }
    
    public void createConference() {
        setTitle("Create conference");
        btnConferenceChange.setText("Create");
        btnConferenceChange.setOnAction(event -> createConferenceAction());
        
        btnCancel = new Button("Cancel");
        btnCancel.setOnAction(event -> cancelAction());
        pane.add(btnCancel, 2, 8);
    }
    
    private void clearTextFields() {
        conference = null;
        name.clear();
        description.clear();
        participantPrice.clear();
        startDate.clear();
        endDate.clear();
        registrations.getItems().clear();
        excursions.getItems().clear();
    }
    
    public void updateConference() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
        formatter = formatter.withLocale(Locale.ENGLISH);
        ArrayList<Excursion> chosenExcursions = new ArrayList<>(excursions.getItems());
        
        double price = 0.0;
        if (participantPrice.getText().trim().isEmpty()) {
            participantPrice.setText(price + "");
        }
        
        price = Double.valueOf(participantPrice.getText().trim());
        
        Service.UpdateConference(conference, name.getText(), price,
            LocalDate.parse(startDate.getText().trim()),
            LocalDate.parse(endDate.getText().trim()), chosenExcursions,
            description.getText().trim());
        
        clearTextFields();
    }
}
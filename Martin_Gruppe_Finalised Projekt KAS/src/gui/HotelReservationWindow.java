package gui;

import java.util.ArrayList;

import application.model.Hotel;
import application.model.HotelReservation;
import application.model.HotelService;
import application.model.Registration;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import storage.Storage;

public class HotelReservationWindow extends Stage {
	private HotelReservation reservation;
	private ListView<Hotel> hotels;
	private ListView<HotelService> hotelServices;
	private ListView<HotelService> chosenHotelServices;
	private ToggleGroup roomGroup;
	private boolean singleRoom;
	private Registration registration;
	private GridPane pane;

	public HotelReservationWindow() {
		pane = new GridPane();
		this.initContent();

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private void initContent() {
		hotels = new ListView<>();
		hotels.getItems().setAll(Storage.getHotels());

		ChangeListener<Hotel> hotelListener = (ov, oldHotel, newHotel) -> hotelChangeAction();
		hotels.getSelectionModel().selectedItemProperty().addListener(hotelListener);
		pane.add(hotels, 0, 1, 1, 2);

		Label lblHotels = new Label("Hotels");
		pane.add(lblHotels, 0, 0);

		hotelServices = new ListView<>();
		// ChangeListener<HotelService> hotelServiceAddingListener = (ov,
		// oldHotelService, newHotelService) -> hotelServiceAddingListener();
		// hotelServices.getSelectionModel().selectedItemProperty().addListener(hotelServiceAddingListener);
		pane.add(hotelServices, 1, 1, 1, 2);

		Label lblHotelServices = new Label("Hotel services");
		pane.add(lblHotelServices, 1, 0);

		chosenHotelServices = new ListView<>();
		ChangeListener<HotelService> hotelServiceRemobingListener = (ov, oldHotelService,
				newHotelService) -> hotelServiceRemoverListener();
		chosenHotelServices.getSelectionModel().selectedItemProperty().addListener(hotelServiceRemobingListener);
		pane.add(chosenHotelServices, 2, 1, 1, 2);

		Label lblServicesChosen = new Label("Hotel services chosen");
		pane.add(lblServicesChosen, 2, 0);

		Button btnAddHotelService = new Button("Chose hotel service");
		btnAddHotelService.setOnAction(event -> choseHotelSErviceAction());
		pane.add(btnAddHotelService, 1, 2);

		RadioButton singleRoom = new RadioButton("Single room");
		singleRoom.setOnAction(event -> singleRoomAction());
		RadioButton doubleRoom = new RadioButton("Double room");
		doubleRoom.setOnAction(event -> doubleRoomAction());

		roomGroup = new ToggleGroup();
		singleRoom.setToggleGroup(roomGroup);
		pane.add(singleRoom, 3, 0);
		doubleRoom.setToggleGroup(roomGroup);
		pane.add(doubleRoom, 4, 0);

		Button createHotelReservation = new Button("Create hotel reservation");
		createHotelReservation.setOnAction(event -> createHotelReservationAcetion());
		pane.add(createHotelReservation, 3, 1, 2, 1);
	}

	private void doubleRoomAction() {
		singleRoom = false;
	}

	private void singleRoomAction() {
		singleRoom = true;
	}

	private void createHotelReservationAcetion() {
		ArrayList<HotelService> hsChosen = new ArrayList<>();
		for (HotelService hs : hotelServices.getItems()) {
			hsChosen.add(hs);
		}

		reservation = Service.createHotelReservation(hotels.getSelectionModel().getSelectedItem(), hsChosen,
				registration, singleRoom);
	}

	private void choseHotelSErviceAction() {
		HotelService hsc = hotelServices.getSelectionModel().getSelectedItem();
		if (hsc != null) {
			chosenHotelServices.getItems().add(hsc);
		}
	}

	private void hotelChangeAction() {
		Hotel h = hotels.getSelectionModel().getSelectedItem();
		if (h != null) {
			hotelServices.getItems().setAll(h.getServices());
		}
	}

	// private void hotelServiceAddingListener() {
	// }

	private HotelService hotelServiceRemoverListener() {
		return chosenHotelServices.getSelectionModel().getSelectedItem();
	}

	private void updateFields(HotelReservation reservation) {
		this.reservation = reservation;

		hotels.getSelectionModel().select(reservation.getHotel());
		hotelServices.getItems().setAll(reservation.getHotel().getServices());
		chosenHotelServices.getItems().setAll(reservation.getServicesChosen());
	}

	public HotelReservation getReservation() {
		return reservation;
	}

	public void addOrUpdateHotelReservation(String title, HotelReservation reservation, Registration registration) {
		this.registration = registration;
		this.reservation = reservation;
		setTitle(title);

		if (reservation != null && registration != null) {
			updateFields(reservation);
		}
	}
}
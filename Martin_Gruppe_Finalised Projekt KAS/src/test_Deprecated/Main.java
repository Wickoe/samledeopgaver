package test_Deprecated;

import java.time.LocalDate;
import java.util.ArrayList;

import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.HotelReservation;
import application.model.HotelService;
import application.model.Participant;
import application.model.Person;
import application.model.Registration;
import application.model.Speaker;
import application.service.Service;

public class Main {
	public static void main(String[] args) {
		ArrayList<Excursion> havOgHimmelExcusrions = new ArrayList<>();
		havOgHimmelExcusrions.add(new Excursion("Byrundtur, Odense, incl. frokost", 125));
		havOgHimmelExcusrions.add(new Excursion("Egeskov", 75));
		havOgHimmelExcusrions.add(new Excursion("Trapholt Museum, Kolding, incl. frokost", 200));

		Conference havOgHimmel = new Conference("Hav og Himmel", 1500, LocalDate.of(2017, 4, 18),
				LocalDate.of(2017, 4, 20), havOgHimmelExcusrions, "Hav og himmel");

		Hotel hvideSvane = new Hotel("Den Hvide Svane", "incl. bath", "5", "", "", 1050, 1250, "", "");
		Hotel hotelPhoenix = new Hotel("Hotel Phoenix", "Hotel Phoenix", "3", "", "", 700, 800, "", "");
		Hotel pensionTusindfryd = new Hotel("Pension Tusindfryd", "Hotel Tusindfryd", "2", "", "", 500, 600, "", "");

		hvideSvane.createService("WiFi", 50);
		hotelPhoenix.createService("Bath", 200);
		hotelPhoenix.createService("WiFi", 75);
		pensionTusindfryd.createService("Breakfast", 100);

		Person madsen = new Person("Finn Madsen", "", "", "", "");
		Person petersen = new Person("Biels Petersen", "", "", "", "");
		Person sommer = new Person("Peter Sommer", "", "", "", "");
		Person jensen = new Person("Lone Jesnen", "", "", "", "");

		Registration r1 = new Registration(madsen, havOgHimmel, LocalDate.of(2017, 4, 18), LocalDate.of(2017, 4, 20),
				new Participant());
		Registration r2 = new Registration(petersen, havOgHimmel, LocalDate.of(2017, 4, 18), LocalDate.of(2017, 4, 20),
				new Participant());
		Registration r3 = new Registration(sommer, havOgHimmel, LocalDate.of(2017, 4, 18), LocalDate.of(2017, 4, 20),
				new Participant());
		Registration r4 = new Registration(jensen, havOgHimmel, LocalDate.of(2017, 4, 18), LocalDate.of(2017, 4, 20),
				new Speaker());

		r2.setHotelReservation(new HotelReservation(hvideSvane, new ArrayList<HotelService>(), r2, true));

		ArrayList<HotelService> sommerChosenHotelService = new ArrayList<>();
		sommerChosenHotelService.add(hvideSvane.getServices().get(0));
		r3.setHotelReservation(new HotelReservation(hvideSvane, sommerChosenHotelService, r3, false));
		r3.createCompanion("Mie Sommer");
		r3.addExcursion(havOgHimmelExcusrions.get(1));
		r3.addExcursion(havOgHimmelExcusrions.get(2));

		ArrayList<HotelService> jensenChosenHotelService = new ArrayList<>();
		jensenChosenHotelService.add(hvideSvane.getServices().get(0));
		r4.setHotelReservation(new HotelReservation(hvideSvane, jensenChosenHotelService, r4, false));
		r4.createCompanion("Jan Madsen");
		r4.addExcursion(havOgHimmelExcusrions.get(0));
		r4.addExcursion(havOgHimmelExcusrions.get(1));

		System.out.println(r1.getPerson().getName() + " has to pay " + Service.calculateTotalConferencePrice(r1));
		System.out.println(r2.getPerson().getName() + " has to pay " + Service.calculateTotalConferencePrice(r2));
		System.out.println(r3.getPerson().getName() + " has to pay " + Service.calculateTotalConferencePrice(r3));
		System.out.println(r4.getPerson().getName() + " has to pay " + Service.calculateTotalConferencePrice(r4));

	}

}
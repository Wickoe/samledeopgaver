package opgaver.Rentals;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a car with a linces registration, a price per day and the year of
 * purchase.
 *
 * @author Martin
 *
 */
public class Car {
	private String license;
	private double pricePrDay;
	private int purchaseYear;
	private List<Rental> rentals;

	/**
	 * Initialize a car with a given license and year brought
	 *
	 * @param license
	 *            the license of the car
	 * @param year
	 *            the year the car was brought
	 */
	public Car(String license, int year) {
		this.license = license;
		purchaseYear = year;
		rentals = new ArrayList<Rental>();
	}

	/**
	 * Sets the price for this car per day.
	 *
	 * @param price
	 *            the price of the car per day
	 */
	public void setDayPrice(double price) {
		pricePrDay = price;
	}

	/**
	 * Returns the daily price of the car.
	 *
	 * @return pricePrDay the daily price of renting the car
	 */
	public double getDayPrice() {
		return pricePrDay;
	}

	/**
	 * Returns the license of the car.
	 *
	 * @return license the license of the car
	 */
	public String getLicense() {
		return license;
	}

	/**
	 * Returns the year the car was brought.
	 *
	 * @return purchaseYear the year the car was brought
	 */
	public int getPurchaseYear() {
		return purchaseYear;
	}

	/**
	 * Add a rental to the car.
	 *
	 * @param rental
	 *            the rental to be added
	 */
	public void addRental(Rental rental) {
		rentals.add(rental);
		if (!rental.getCars().contains(this)) {
			rental.addCar(this);
		}
	}

	/**
	 * Remove a rental from the car.
	 *
	 * @param rental
	 *            the rental to be removed
	 */
	public void removeRental(Rental rental) {
		rentals.remove(rental);

		if (rental.getCars().contains(this)) {
			rental.removeCar(this);
		}
	}

	/**
	 * Returns a list of rentals for the car.
	 *
	 * @return rentals the rentals for the car
	 */
	public List<Rental> getRentals() {
		return new ArrayList<>(rentals);
	}

	/**
	 * Returns the longest period of time the car has been rented.
	 *
	 * @return longestRentalPeriod the longest rental period for the car
	 */
	public int longestRental() {
		int rentalDays = 0;

		for (Rental r : rentals) {
			if (r.getDays() > rentalDays) {
				rentalDays = r.getDays();
			}
		}
		return rentalDays;
	}
}

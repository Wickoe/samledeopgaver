package opgaver.Series;

import java.util.ArrayList;
import java.util.List;

/**
 * Models an enispde with a number, an episode length in minutes and a list of
 * guest actors.
 *
 * @author Martin
 *
 */
public class Episode {
	private int number, episodeLengthMinutes;
	private List<String> guestActors;

	/**
	 * Initializes an episode.
	 */
	public Episode(int aNumber, int anEpisodeLength) {
		number = aNumber;
		episodeLengthMinutes = anEpisodeLength;
		guestActors = new ArrayList<String>();
	}

	/**
	 * Returns the episodes number in the serie.
	 *
	 * @return number the number of the episode in the serie
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Returns the length of the episode.
	 *
	 * @return length the length of the episode
	 */
	public int getLength() {
		return episodeLengthMinutes;
	}

	/**
	 * Returns a list of the guest actors for the episode.
	 *
	 * @return guestActors the guest actors for the episode
	 */
	public List<String> getGuestActors() {
		return new ArrayList<>(guestActors);
	}

	/**
	 * Adds a guest actor to the list of guest actors.
	 *
	 * @param name
	 *            the name of the guest actor
	 */
	public void addGuestActor(String name) {
		guestActors.add(name);
	}

	/**
	 * Removes the guest actor from the list of guest actors.
	 *
	 * @param name
	 *            the name of the guest actor
	 */
	public void removeGuestActor(String name) {
		guestActors.remove(name);
	}
}

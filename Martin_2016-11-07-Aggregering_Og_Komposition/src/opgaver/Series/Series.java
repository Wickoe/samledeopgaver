package opgaver.Series;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a serie with a title, a cast and episodes.
 *
 * @author Martin
 *
 */
public class Series {
	private String title;
	private List<String> cast;
	private List<Episode> episodes;

	/**
	 * Initialize a serie with a title and a cast
	 *
	 * @param title
	 *            the title of the serie
	 * @param cast
	 *            the list of the serie's cast-members
	 */
	public Series(String title, List<String> cast) {
		this.title = title;
		this.cast = cast;
		episodes = new ArrayList<>();
	}

	/**
	 * Return the title of the serie.
	 *
	 * @return title the title of the serie
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the cast of the serie.
	 *
	 * @return cast the cast of the serie
	 */
	public List<String> getCast() {
		return new ArrayList<>(cast);
	}

	/**
	 * Returns a list of all episodes related to the serie.
	 *
	 * @return episodes the associated episodes for serie
	 */
	public List<Episode> getEpisodes() {
		return new ArrayList<>(episodes);
	}

	/**
	 * Create and returns a new episode in the serie.
	 *
	 * @param aNumber
	 *            the number of the episode
	 * @param anEpisodeLength
	 *            the length of the episode
	 * @return episode the episode just created / released
	 */
	public Episode createEpisode(int aNumber, int anEpisodeLength) {
		Episode episode = new Episode(aNumber, anEpisodeLength);

		episodes.add(episode);

		return episode;
	}

	/**
	 * Deletes an episode from the serie.
	 *
	 * @param episode
	 *            the episode to be deleted
	 */
	public void deleteEpisode(Episode episode) {
		episodes.remove(episode);
	}

	/**
	 * Return the total length (in minutes) of all the episodes in the series.
	 * 
	 * @return totalLength the total length in minutes of all the episodes in
	 *         the serie
	 */
	public int totalLength() {
		int totalLength = 0;

		for (Episode e : episodes) {
			totalLength += e.getLength();
		}

		return totalLength;
	}
}

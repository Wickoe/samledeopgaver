package opgaver.Gift;

/**
 * Models a gift with a description, a price, the person its from and possible
 * the person its to
 *
 * @author Martin
 *
 */
public class Gift {
	private String description;
	private double price;
	private Person giver, reciever;

	/**
	 * Initialize a gift with a description.
	 *
	 * @param description
	 *            the description of the gift
	 */
	public Gift(String description) {
		this.description = description;
	}

	/**
	 * Returns the price of the gift.
	 *
	 * @return price the price of the gift
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Set the price for the gift.
	 *
	 * @param price
	 *            the price for the gift
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Returns the description for the gift.
	 *
	 * @return description the description of the gift
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the giver of the gift.
	 *
	 * @param person
	 *            the giver of the gift
	 */
	public void setGiver(Person person) {
		giver = person;
		if (person != null && person.getGiftsGiven().contains(this)) {
			person.addGivenGift(this);
		}
	}

	/**
	 * Returns the giver of the gift.
	 *
	 * @return giver the person who gives the gift
	 */
	public Person getGiver() {
		return giver;
	}

	/**
	 * Set the receiver of the gift.
	 *
	 * @param person
	 *            the person who receives the gift
	 */
	public void setReciever(Person person) {
		reciever = person;
		if (person != null && !person.getReceivedGifts().contains(this)) {
			person.addRecievedGift(this);
		}
	}

	/**
	 * Returns the receiver of the gift
	 *
	 * @return receiver the receiver of the gift
	 */
	public Person getReciever() {
		return reciever;
	}
}

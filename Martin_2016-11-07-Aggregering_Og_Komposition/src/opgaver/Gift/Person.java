package opgaver.Gift;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a person with a name and an age.
 *
 * @author Martin
 *
 */
public class Person {
	private String name;
	private int age;
	private List<Gift> giftsGiven, giftsRecieved;

	/**
	 * Initialize a person with a name and an age
	 *
	 * @param name
	 *            the name of the person
	 * @param age
	 *            the age of the person
	 */
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
		giftsGiven = new ArrayList<>();
		giftsRecieved = new ArrayList<>();
	}

	/**
	 * Set the age of the person.
	 *
	 * @param age
	 *            the person new age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return name + " " + age;
	}

	/**
	 * Adds a gift to the list of gifts given.
	 *
	 * @param gift
	 *            the gift to be added
	 */
	public void addGivenGift(Gift gift) {
		giftsGiven.add(gift);
		if (!gift.getGiver().equals(this)) {
			gift.setGiver(this);
		}
	}

	/**
	 * Removes a gift from the list of given gifts.
	 *
	 * @param gift
	 *            the gift to be removed
	 */
	public void removeGivenGift(Gift gift) {
		giftsGiven.remove(gift);
		if (gift.getGiver().equals(this)) {
			gift.setGiver(null);
		}
	}

	/**
	 * Returns the list of all gifts given.
	 *
	 * @return gifts the gifts the person has given
	 */
	public List<Gift> getGiftsGiven() {
		return new ArrayList<>(giftsGiven);
	}

	/**
	 * Adds a gift to the list of gifts received.
	 *
	 * @param gift
	 *            the received gift
	 */
	public void addRecievedGift(Gift gift) {
		giftsRecieved.add(gift);
		if (!gift.getReciever().equals(this)) {
			gift.setReciever(this);
		}
	}

	/**
	 * Removes a gift from the list of received gifts.
	 *
	 * @param gift
	 *            the gift to be removed
	 */
	public void removeRecievedGift(Gift gift) {
		giftsRecieved.remove(gift);
		if (gift.getReciever().equals(this)) {
			gift.setReciever(null);
		}
	}

	/**
	 * Returns a list of received gifts.
	 *
	 * @return receivedGifts the list of received gifts
	 */
	public List<Gift> getReceivedGifts() {
		return new ArrayList<>(giftsRecieved);
	}

	/**
	 * Returns the total value of all received gifts.
	 *
	 * @return sum the total sum of received gifts
	 */
	public double sumOfReceivedGifts() {
		double sum = 0;

		for (Gift g : giftsRecieved) {
			sum += g.getPrice();
		}

		return sum;
	}
}

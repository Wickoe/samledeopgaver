package opgaver.Swimming;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a training plan for a Swimmer
 */
public class TrainingPlan {
	private char level;
	private int weeklyWaterHours;
	private int weeklyStrengthHours;
	private List<Swimmer> swimmers;

	/**
	 * Initialize a TrainingPlan with a level, an weekly water- and strength-
	 * hour workout
	 *
	 * @param level
	 *            the level of the training plan
	 * @param weeklyWaterHours
	 *            the weekly water hours
	 * @param weeklyStrengthHours
	 *            the weekly strength hours
	 */
	public TrainingPlan(char level, int weeklyWaterHours, int weeklyStrengthHours) {
		this.level = level;
		this.weeklyWaterHours = weeklyWaterHours;
		this.weeklyStrengthHours = weeklyStrengthHours;
		swimmers = new ArrayList<Swimmer>();
	}

	/**
	 * Returns the level of the training plan.
	 *
	 * @return level the level of the training plan
	 */
	public char getLevel() {
		return level;
	}

	/**
	 * Set the level of the training plan.
	 *
	 * @param niveau
	 *            the level of the training plan
	 */
	public void setLevel(char niveau) {
		this.level = niveau;
	}

	/**
	 * Returns the weekly strength hours work-out.
	 *
	 * @return weeklyStrengthHours the weekly strength hours
	 */
	public int getWeeklyStrengthHours() {
		return weeklyStrengthHours;
	}

	/**
	 * Set the weekly strength hours.
	 *
	 * @Pre: weeklyStrengthHours cannot be negative
	 * @param weeklyStrengthHours
	 *            the weekly strength hours
	 */
	public void setWeeklyStrengthHours(int weeklyStrengthHours) {
		this.weeklyStrengthHours = weeklyStrengthHours;
	}

	/**
	 * Returns the weekly water hours.
	 *
	 * @return weeklyWaterHours the weekly water hours.
	 */
	public int getWeeklyWaterHours() {
		return weeklyWaterHours;
	}

	/**
	 * Set the weekly water hours.
	 *
	 * @Pre: weeklyWaterHours cannot be negative
	 * @param weeklyWaterHours
	 *            the weekly water hours
	 */
	public void setWeeklyWaterHours(int weeklyWaterHours) {
		this.weeklyWaterHours = weeklyWaterHours;
	}

	public List<Swimmer> getSwimmers() {
		return new ArrayList<>(swimmers);
	}

	/**
	 * Creates and adds a swimmer to the training plan.
	 *
	 * @param name
	 *            the name of the swimmer
	 * @param yearGroup
	 *            the birth year of the swimmer
	 * @param lapTimes
	 *            the swimmer lap times
	 * @param club
	 *            the club the swimmer is associated with
	 * @return swimmer a new swimmer who has joined the training plan
	 */
	public Swimmer createSwimmer(String name, int yearGroup, ArrayList<Double> lapTimes, String club) {
		Swimmer swimmer = new Swimmer(name, yearGroup, lapTimes, club);

		swimmers.add(swimmer);

		return swimmer;
	}
}

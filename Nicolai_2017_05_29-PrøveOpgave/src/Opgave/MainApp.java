package Opgave;

public class MainApp {

	public static void main(String[] args) {
		BinaryTree<String> tree1 = new BinaryTree<String>("2");
		BinaryTree<String> tree2 = new BinaryTree<String>("4");
		BinaryTree<String> tree3 = new BinaryTree<String>("+", tree1, tree2);
		BinaryTree<String> tree4 = new BinaryTree<String>("7");
		BinaryTree<String> tree5 = new BinaryTree<String>("*", tree3, tree4);
		BinaryTree<String> tree6 = new BinaryTree<String>("3");
		BinaryTree<String> tree7 = new BinaryTree<String>("8");
		BinaryTree<String> tree8 = new BinaryTree<String>("+", tree6, tree7);
		BinaryTree<String> tree9 = new BinaryTree<String>("+", tree5, tree8);

		System.out.println(tree9.countElements("+"));

	}

}

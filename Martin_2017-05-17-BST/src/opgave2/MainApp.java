package opgave2;

import BST.BinarySearchTree;

public class MainApp {
	public static void main(String[] args) {
		BinarySearchTree<Integer> tal = new BinarySearchTree<>();

		tal.add(45);
		tal.add(77);
		tal.add(90);
		tal.add(88);
		tal.add(22);
		tal.add(30);
		tal.add(25);
		tal.add(11);
		tal.add(15);

		tal.print();
		
		System.out.println(tal.findMin());
		System.out.println(tal.findMax());

		tal.print();

		tal.removeMin();
		tal.removeMax();

		tal.print();
	}
}

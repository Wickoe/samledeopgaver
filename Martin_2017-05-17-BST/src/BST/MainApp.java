package BST;

public class MainApp {
	public static void main(String[] args) {
		DictionaryBST<Integer, String> intNames = new DictionaryBST<>();
		
		intNames.put(45, "Martin");
		intNames.put(77, "Leo");
		intNames.put(90, "Blakie");
		intNames.put(88, "Nicolai");
		intNames.put(22, "Kristian");
		intNames.put(30, "Michael");
		intNames.put(25, "Maxx");
		intNames.put(11, "Chriss");
		intNames.put(15, "Margrethe");
		
		System.out.println(intNames.floor(22));
		System.out.println(intNames.ceil(77));
	}
}

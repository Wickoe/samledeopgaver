package BST;

public class DictionaryBST<K extends Comparable<K>, V> implements Dictionary<K, V> {
	private Node root;
	
	public DictionaryBST() {
		root = null;
	}
	
	@Override
	public V get(K key) {
		Node node = find(key);
		V value = null;

		if (node != null) {
			value = node.value;
		}
		
		return value;
	}
	
	private Node find(K key) {
		Node current = root;
		boolean found = false;
		while (!found && current != null) {
			int d = current.key.compareTo(key);
			if (d == 0) {
				found = true;
			}
			else if (d > 0) {
				current = current.left;
			}
			else {
				current = current.right;
			}
		}

		if (found) {
			return current;
		}
		else {
			return null;
		}
	}
	
	@Override
	public boolean isEmpty() {
		return root == null;
	}
	
	@Override
	public V put(K key, V value) {
		Node node = new Node(key, value);
		V returnValue = value;

		if (isEmpty()) {
			root = node;
		}
		else {
			node = find(key);

			if (node != null) {
				returnValue = node.value;
				node.value = value;
			}
			else {
				node = new Node(key, value);
				root.addNode(node);
			}
		}
		
		return returnValue;
	}
	
	@Override
	public V remove(K key) {
		Node toBeRemoved = root;
		Node parent = null;
		V toReturn = null;
		boolean found = false;

		while (!found && toBeRemoved != null) {
			int d = toBeRemoved.key.compareTo(key);
			if (d == 0) {
				found = true;
				toReturn = toBeRemoved.value;
			}
			else {
				parent = toBeRemoved;
				if (d > 0) {
					toBeRemoved = toBeRemoved.left;
				}
				else {
					toBeRemoved = toBeRemoved.right;
				}
			}
		}
		
		if (found) {
			// toBeRemoved contains obj
			// If one of the children is empty, use the other
			if (toBeRemoved.left == null || toBeRemoved.right == null) {
				Node newChild;
				if (toBeRemoved.left == null) {
					newChild = toBeRemoved.right;
				}
				else {
					newChild = toBeRemoved.left;
				}
				
				if (parent == null) // Found in root
				{
					root = newChild;
				}
				else if (parent.left == toBeRemoved) {
					parent.left = newChild;
				}
				else {
					parent.right = newChild;
				}
				
			}
			else {
				// Neither subtree is empty
				// Find smallest element of the right subtree
				Node smallestParent = toBeRemoved;
				Node smallest = toBeRemoved.right;
				while (smallest.left != null) {
					smallestParent = smallest;
					smallest = smallest.left;
				}
				
				// smallest contains smallest child in right subtree
				// Move contents, unlink child
				toBeRemoved.key = smallest.key;
				toBeRemoved.value = smallest.value;
				if (smallestParent == toBeRemoved) {
					smallestParent.right = smallest.right;
				}
				else {
					smallestParent.left = smallest.right;
				}
			}
		}
		return toReturn;
	}
	
	@Override
	public int size() {
		return size(root);
	}
	
	private int size(Node nodeAt) {
		int size = 0;
		
		if (nodeAt != null) {
			size += size(nodeAt.left);
			size += size(nodeAt.right);
			size++;
		}

		return size;
	}
	
	public V floor(K key) {
		Node current = root;
		int compareValue = current.key.compareTo(key);

		while (current != null && compareValue > 0) {
			current = current.left;
			compareValue = key.compareTo(current.key);
		}
		
		V result = null;
		
		if (current != null) {
			while (current.right != null) {
				current = current.right;
			}
			
			result = current.value;
		}
		
		return result;
	}
	
	public V ceil(K key) {
		Node current = root;
		int compareValue = current.key.compareTo(key);

		while (current != null && compareValue < 0) {
			current = current.right;
			compareValue = key.compareTo(current.key);
		}
		
		V result = null;
		
		if (current != null) {
			while (current.left != null) {
				current = current.left;
			}
			
			result = current.value;
		}
		
		return result;
	}
	
	private class Node {
		private K key;
		private V value;
		private Node left;
		private Node right;
		
		public Node(K key, V value) {
			this.key = key;
			this.value = value;
			this.left = null;
			this.right = null;
		}
		
		/**
		 * Inserts a new node as a descendant of this node.
		 * @param newNode the node to insert
		 */
		private void addNode(Node newNode) {
			int compareValue = newNode.key.compareTo(key);
			
			if (compareValue < 0) {
				if (left != null) {
					left.addNode(newNode);
				}
				else {
					left = newNode;
				}
			}
			else if (compareValue > 0) {
				if (right != null) {
					right.addNode(newNode);
				}
				else {
					right = newNode;
				}
			}
		}
	}
}

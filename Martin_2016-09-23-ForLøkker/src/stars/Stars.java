package stars;

public class Stars {
	final int MAX_ROWS = 10;

	public void starPicture() {
		for (int row = 1; row <= MAX_ROWS; row++) {
			for (int star = 1; star <= row; star++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	public void starPictureA() {
		for (int i = MAX_ROWS; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	public void starPictureB() {
		for (int i = MAX_ROWS - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				System.out.print(" ");
			}
			for (int j = MAX_ROWS; j > i; j--) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	public void starPictureC() {
		for (int i = MAX_ROWS; i > 0; i--) {
			for (int j = MAX_ROWS; j > i; j--) {
				System.out.print(" ");
			}
			for (int j = i; j > 0; j--) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	public void starPictureD() {
		// TODO
	}
}

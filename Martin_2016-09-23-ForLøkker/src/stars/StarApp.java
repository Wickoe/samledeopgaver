package stars;

public class StarApp {

	public static void main(String[] args) {
		Stars star = new Stars();
		System.out.println("Test picture");
		star.starPicture();
		System.out.println();
		System.out.println("Picture A");
		star.starPictureA();
		System.out.println();
		System.out.println("Picture B");
		star.starPictureB();
		System.out.println();
		System.out.println("Picture C");
		star.starPictureC();
		System.out.println();
		System.out.println("Picture D");
		star.starPictureD();
	}

}

package opgaver.TalUdskrivning;

public class TalUdskrivning {
	public static void main(String[] args) {
		for (int i = 1; i < 100; i++) {
			System.out.println(i);
		}

		System.out.println();

		for (int i = 300; i >= 3; i--) {
			if (i % 3 == 0) {
				System.out.println(i);
			}
		}
	}
}

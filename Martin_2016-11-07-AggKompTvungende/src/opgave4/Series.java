package opgave4;

import java.util.ArrayList;

public class Series {
    private String title;
    private ArrayList<String> cast;
    private ArrayList<Episode> episodes = new ArrayList<>();
    
    public Series(String title, ArrayList<String> cast) {
        this.title = title;
        this.cast = cast;
    }
    
    public String getTitle() {
        return title;
    }
    
    public ArrayList<String> getCast() {
        return new ArrayList<>(cast);
    }
    
    public Episode createEpisode(int number, ArrayList<String> guestActors, int episodeLength) {
        Episode aNewEpisode = new Episode(number, guestActors, episodeLength);
        episodes.add(aNewEpisode);
        return aNewEpisode;
    }
    
    public void deledeEpisode(Episode episode) {
        episodes.remove(episode);
    }
    
    public ArrayList<Episode> getEpisodes() {
        return new ArrayList<>(episodes);
    }
    
    public int totalLength() {
        double length = 0;
        for (Episode e : episodes) {
            length += e.getLength();
        }
        
        return (int) length;
    }
    
    public ArrayList<String> getAllGuestActors() {
        ArrayList<String> allGuestActors = new ArrayList<>();
        
        for (Episode e : episodes) {
            for (String s : e.getGuestCast()) {
                if (!allGuestActors.contains(s)) {
                    allGuestActors.add(s);
                }
            }
        }
        return allGuestActors;
    }
}

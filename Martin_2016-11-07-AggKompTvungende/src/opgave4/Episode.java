package opgave4;

import java.util.ArrayList;

public class Episode {
    private int number;
    private ArrayList<String> guestActors;
    private int episodesLengthMinutes;

    Episode(int number, ArrayList<String> guestActors, int episodesLengthMinutes) {
        this.number = number;
        this.guestActors = guestActors;
        this.episodesLengthMinutes = episodesLengthMinutes;
    }
    
    public int getNumber() {
        return number;
    }
    
    public int getLength() {
        return episodesLengthMinutes;
    }

    public ArrayList<String> getGuestCast() {
        return new ArrayList<>(guestActors);
    }
}
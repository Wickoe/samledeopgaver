package opgave1;

import java.util.ArrayList;

public class Car {
    private String license;
    private double pricePerDay;
    private int purchaseYear;
    private ArrayList<Rental> rentals = new ArrayList<>();

    public Car(String license, int year) {
        this.purchaseYear = year;
        this.license = license;
    }
    
    public void setDayPrice(double price) {
        this.pricePerDay = price;
    }

    public double getDayPrice() {
        return this.pricePerDay;
    }
    
    public String getLicense() {
        return this.license;
    }
    
    public int getPurchaseYear() {
        return this.purchaseYear;
    }
    
    public void addRental(Rental rental) {
        rentals.add(rental);
        if (!rental.getCars().contains(this)) {
            rental.addCar(this);
        }
    }

    public void removeRental(Rental rental) {
        rentals.remove(rental);
        if (rental.getCars().contains(this)) {
            rental.removeCar(this);
        }
    }
    
    public ArrayList<Rental> getRentals() {
        return new ArrayList<>(rentals);
    }
    
    public int getLongestRentalPeriod() {
        int longestPeriodOfDaysRented = 0;
        for (Rental rental : rentals) {
            if (rental.getDays() > longestPeriodOfDaysRented) {
                longestPeriodOfDaysRented = rental.getDays();
            }
        }
        
        return longestPeriodOfDaysRented;
    }
}

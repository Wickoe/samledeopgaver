package opgave1;

public class App {
    
    public static void main(String[] args) {
        Car carOne = new Car("Stort", 2004);
        carOne.setDayPrice(1200.50);
        Car carTwo = new Car("Bus", 2010);
        carTwo.setDayPrice(1500);
        Car carThree = new Car("Normalt", 2003);
        carThree.setDayPrice(750.95);
        Car carFour = new Car("Kørekort med stort anhængertræk", 2016);
        carFour.setDayPrice(1000.75);
        Car carFive = new Car("Normalt", 2008);
        carFive.setDayPrice(500);
        
        Rental rentalOne = new Rental(1, "2016-11-05", 2);
        Rental rentalTwo = new Rental(2, "2016-12-1", 1);
        
        rentalOne.addCar(carOne);
        rentalOne.addCar(carTwo);
        rentalOne.addCar(carThree);
        rentalOne.addCar(carFive);
        rentalOne.addCar(carOne);

        rentalTwo.addCar(carTwo);
        rentalTwo.addCar(carThree);
        rentalTwo.addCar(carFour);
        rentalTwo.addCar(carFive);
        
        System.out.println("Total price for the fist rental is: " + rentalOne.getPrice());
        System.out.println("Total price for the fist rental is: " + rentalTwo.getPrice());
        
        System.out.println("The longest car one has ever been rented out at one time is "
            + carOne.getLongestRentalPeriod());
        System.out.println("The longest car two has ever been rented out at one time is "
            + carTwo.getLongestRentalPeriod());
        System.out.println("The longest car four has ever been rented out at one time is "
            + carFour.getLongestRentalPeriod());
    }
    
}

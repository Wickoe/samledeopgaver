package opgave1;

import java.util.ArrayList;

public class Rental {
    private int number;
    private int days;
    private String date;
    private ArrayList<Car> cars = new ArrayList<>();

    public Rental(int number, String date, int days) {
        this.number = number;
        this.date = date;
        this.days = days;
    }

    public double getPrice() {
        double totalPrice = 0;

        for (Car c : cars) {
            totalPrice += c.getDayPrice() * days;
        }

        return totalPrice;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getDays() {
        return this.days;
    }

    public String getDate() {
        return this.date;
    }

    public int getRentalNumber() {
        return this.number;
    }

    public void addCar(Car car) {
        cars.add(car);
        if (!car.getRentals().contains(this)) {
            car.addRental(this);
        }
    }

    public void removeCar(Car car) {
        cars.remove(car);
        if (car.getRentals().contains(this)) {
            car.removeRental(this);
        }
    }

    public ArrayList<Car> getCars() {
        return new ArrayList<>(cars);
    }
}
package opgave3;

import java.util.ArrayList;

public class Person {
    private String name;
    private int age;
    private ArrayList<Gift> presentsRecieved = new ArrayList<>();

    public Person(String aName, int anAge) {
        this.age = anAge;
        this.name = aName;
    }
    
    public ArrayList<Gift> getGifts() {
        return new ArrayList<>(presentsRecieved);
    }
    
    public void addGift(Gift aGift) {
        presentsRecieved.add(aGift);
    }
    
    public void removeGift(Gift aGift) {
        presentsRecieved.remove(aGift);
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        String toPrint =
            name + ", age " + age;
        return toPrint;
    }
    
    public double calcTotalPriceOfGifts() {
        double totalPrice = 0;
        for (Gift recieved : presentsRecieved) {
            totalPrice += recieved.getPrice();
        }
        
        return totalPrice;
    }
    
    public ArrayList<String> getPersonsOfGiftsRecieved() {
        ArrayList<String> persons = new ArrayList<>();
        for (Gift gift : presentsRecieved) {
            persons.add(gift.getGiver().toString());
        }

        return persons;
    }
}
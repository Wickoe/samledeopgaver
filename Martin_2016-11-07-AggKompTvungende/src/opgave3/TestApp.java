package opgave3;

public class TestApp {
    public static void main(String[] args) {
        Person theodor = new Person("Theodor", 2);
        Person martin = new Person("Martin", 23);
        Person mads = new Person("Mads", 0);
        
        Gift presentOne = new Gift("A nice round present", martin);
        presentOne.setPrice(50);
        Gift presentTwo = new Gift("A squared present", mads);
        presentTwo.setPrice(375);
        Gift presentThree = new Gift("A wiered soft present", theodor);
        presentThree.setPrice(100);
        
        theodor.addGift(presentOne);
        theodor.addGift(presentTwo);
        martin.addGift(presentThree);
        
        System.out.println(
            "Theodor recieves presents for " + theodor.calcTotalPriceOfGifts());
        System.out.println(
            "Martin recieves presents for " + martin.calcTotalPriceOfGifts());
        System.out.println();
        System.out.println(theodor + " has revieved gifts from: ");
        for (String s : theodor.getPersonsOfGiftsRecieved()) {
            System.out.println(s);
        }
    }
}
package opgave3;

public class Gift {
    private String description;
    private double price;
    private Person giver;
    
    public Gift(String description, Person aGiver) {
        this.description = description;
        this.giver = aGiver;
    }

    public void setPrice(double aPrice) {
        this.price = aPrice;
    }

    public double getPrice() {
        return price;
    }
    
    public String getDescription() {
        return description;
    }

    public Person getGiver() {
        return giver;
    }
}
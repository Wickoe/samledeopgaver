package opgave2;

import java.util.ArrayList;

/**
 * Modeling a Swimmer
 */
public class Swimmer {
    private String name;
    private String club;
    private int yearGroup;
    private ArrayList<Double> lapTimes;

    /**
     * Initialize a new swimmer with name, club, yearGroup, and lap times.
     */
    Swimmer(String name, int yearGroup, ArrayList<Double> lapTimes, String club) {
        this.name = name;
        this.yearGroup = yearGroup;
        this.lapTimes = lapTimes;
        this.club = club;
    }
    
    /**
     * Return the name of the swimmer
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Return the yearGroup of the swimmer
     */
    public int getYearGroup() {
        return this.yearGroup;
    }
    
    /**
     * Return the club of the swimmer
     */
    public String getClub() {
        return this.club;
    }
    
    /**
     * Register the club of the swimmer
     * @param club
     */
    public void setClub(String club) {
        this.club = club;
    }

    /**
     * Return the fastest lap time
     */
    public double bestLapTime() {
        double bestTime = lapTimes.get(0);
        for (int i = 1; i < lapTimes.size(); i++) {
            if (lapTimes.get(i) < bestTime) {
                bestTime = lapTimes.get(i);
            }
        }
        return bestTime;
    }
    
    @Override
    public String toString() {
        String information = "Name of swimmer is: " + name + "\n";
        information += "Club of swimmer is: " + club + "\n";
        information += "The year-group of the swimmer is: " + yearGroup + "\n";
        information += "Best lap time is: " + Double.toString(bestLapTime()) + "\n";

        return information;
    }
}
